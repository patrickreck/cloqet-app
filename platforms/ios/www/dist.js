var CloqetApp = angular.module('CloqetApp', [
	'ionic', 
	'ngAnimate', 
	'ItemModule', 
	'CollectionModule',
	'BrandModule',
	'AuthenticationModule',
	'ListModule',
	'ConversationModule',
	'ShopModule', 
	'truncate',
	'http-auth-interceptor',
	'ngCordova',
	'angularMoment',
	'ngMap',
	'angular-data.DSCacheFactory',
])

.run(function($ionicPlatform, $rootScope, $ionicSideMenuDelegate, AuthenticationService, $cordovaStatusbar, $ionicLoading, $cordovaSplashscreen, DSCacheFactory, $http, $interval) {
	
//		$rootScope.clientRoot = 'http://192.168.1.47/cloqet_app/www/';
		$rootScope.root = 'http://cloqet-server.herokuapp.com/public';
		$rootScope.api = 'http://cloqet-server.herokuapp.com/public';

		$rootScope.root = 'http://192.168.1.61/cloqet-server/public';
		$rootScope.api = 'http://192.168.1.61/cloqet-server/public';

		// Cache
		DSCacheFactory('defaultCache', {
			maxAge: 300000,
			cacheFlushInterval: 6000000,
			deleteOnExpire: 'aggressive'
		});

		$http.defaults.cache = DSCacheFactory.get('defaultCache');


		// Login specifics
		$rootScope.isLoggedIn = function() {
			return AuthenticationService.isLoggedIn();
		}

		$rootScope.getBrandId = function() {
			return AuthenticationService.getBrandId();
		}

		$rootScope.getShopId = function() {
			return AuthenticationService.getBrandId();
		}

		$rootScope.getUserId = function() {
			return AuthenticationService.getUserId();
		}

		$rootScope.getUserPicture = function() {
			return AuthenticationService.getUserPicture();
		}


		$rootScope.$on('$stateChangeStart', function() {
			$ionicLoading.show({
				template: '<i class="icon ion-loading-c loadinga"></i>',
				animation: 'fade-in',
				showBackdrop: true,
				showDelay: 300
			});
		})

		$rootScope.$on('$stateChangeSuccess', function() {
			$ionicLoading.hide()
		})





	$ionicPlatform.ready(function() {

		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if(window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

	        $interval(function() {
		        $cordovaSplashscreen.hide();
	        }, 2000, 1)

		}
		if(window.StatusBar) {
			StatusBar.styleDefault();

			$cordovaStatusbar.style(2);
		}


	});




})



.config(function($httpProvider) {
	$httpProvider.interceptors.push(function($rootScope) {
		return {
			request: function(config) {
				//$rootScope.$broadcast('loading:show')
				return config
			},
			response: function(response) {
				//$rootScope.$broadcast('loading:hide')
				return response
			}
		}
	})
})














CloqetApp.service('LocationService', ['$rootScope', '$http', '$cordovaGeolocation',

	function($rootScope, $http, $cordovaGeolocation) {

		var myPosition = null;

		this.updateMyPosition = function() {

			return $cordovaGeolocation.getCurrentPosition()
				.then(function (position) {
					myPosition = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};

					$rootScope.$broadcast('updatedPosition', myPosition);

					return myPosition;

				}, function(error) {
				});
		}

		this.getMyPosition = function() {
			return myPosition;
		}


		this.distance = function(lon1, lat1) {
    
	        lon2 = myPosition.lat;
	        lat2 = myPosition.lng;

			var R = 6371;
			var dLat = (lat2-lat1)*Math.PI / 180;
			var dLon = (lon2-lon1)*Math.PI / 180; 
			var a = Math.sin(dLat/2)*Math.sin(dLat/2) +
			    Math.cos(lat1*Math.PI / 180) * Math.cos(lat2*Math.PI / 180) * 
			    Math.sin(dLon/2) * Math.sin(dLon/2); 
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
			var d = R * c;

			return d;
			
		}

	}

]);
CloqetApp.run(function($rootScope, $cordovaGeolocation, $q) {


});

CloqetApp.config(['$stateProvider', '$urlRouterProvider', 
	function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('app/items');

	$stateProvider

			.state('app', {
				url: "/app",
				abstract: true,
				templateUrl: "templates/menu.html",
			})

			/* Items */
			.state('app.items', {
				url: '/items',
				
				views: {
					'menuContent' :{
						templateUrl: 'js/modules/items/partials/items.html',
						controller: 'ItemsController',

						resolve: {
							items : ['ItemService', '$q', function(ItemService, $q) {
								
								var defer = $q.defer();
								ItemService.getMoreItems().then(function(response) {
									defer.resolve(response.data);

								}, function(response) {
									console.log("Oops, no items");
									console.log(response);
								});

								return defer.promise;
							}]
						}
					},
				},

			})

			.state('app.item', {
				url: '/items/:itemId',
				
				views: {
					'menuContent' :{
						templateUrl: 'js/modules/items/partials/item.html',
						controller: 'ItemController',

						resolve: {

							item : ['ItemService', '$stateParams', '$q', function(ItemService, $stateParams, $q) {
								var defer = $q.defer();

								ItemService.getItem($stateParams.itemId).success(function(data) {
									defer.resolve(data);
								});

								return defer.promise;
							}]
						}
					},
				},

			})

			.state('app.shops', {
				url: '/stores',

				views: {
					'menuContent' :{
						templateUrl: 'js/modules/shops/partials/shops.html',
						controller: 'ShopsController',

						resolve: {
							shops : ['ShopService', '$q', function(ShopService, $q) {
								
								var defer = $q.defer();
								ShopService.getShops().then(function(response) {

									defer.resolve(response.data);

								}, function() {
									console.log("Oops");
								});

								return defer.promise;
							}]
						}						
					}
				}
			})

			.state('app.shop', {
				url: '/stores/:shopId',
				
				views: {
					'menuContent' :{
						templateUrl: 'js/modules/shops/partials/shop.html',
						controller: 'ShopController',

						resolve: {

							shop : ['ShopService', '$stateParams', '$q', function(ShopService, $stateParams, $q) {
								var defer = $q.defer();

								ShopService.getShop($stateParams.shopId).success(function(data) {
									defer.resolve(data);
								});

								return defer.promise;
							}]
						}
					},
				},
			})

			.state('app.shop.featured', {
				url: '/featured',
				templateUrl: 'js/modules/shops/partials/shop.featured.html',
				controller: 'ItemsController',

				resolve: {

					items: ['ItemService', '$stateParams', '$q', function(ItemService, $stateParams, $q) {

						var defer = $q.defer();

						ItemService.getItemsByShopRandom($stateParams.shopId).then(function(response) {
							defer.resolve(response.data);
						}, function(response) {
							console.log('Oh no.......');
						});

						return defer.promise;

					}]
				}
			})

			.state('app.shop.search', {
				url: '/search',
				templateUrl: 'js/modules/shops/partials/shop.search.html',
				controller: 'ShopSearchController',

				resolve: {

					items: ['ItemService', '$stateParams', '$q', function(ItemService, $stateParams, $q) {

						var defer = $q.defer();

						ItemService.getItemsByShop($stateParams.shopId).then(function(response) {
							defer.resolve(response.data);
						}, function(response) {
							console.log('Oh no.......');
						});

						return defer.promise;

					}]
				}
			})




			.state('app.brands', {
				url: '/brands',

				views: {
					'menuContent' :{
						templateUrl: 'js/modules/brands/partials/brands.html',
						controller: 'BrandsController',

						resolve: {
							brands : ['BrandService', '$q', function(BrandService, $q) {
								
								var defer = $q.defer();
								BrandService.getBrands().then(function(response) {

									defer.resolve(response.data);

								}, function() {
									console.log("Oops");
								});

								return defer.promise;
							}]
						}						
					}
				}
			})


			.state('app.brand', {
				url: '/brands/:brandId',
				
				views: {
					'menuContent' :{
						templateUrl: 'js/modules/brands/partials/brand.html',
						controller: 'BrandController',

						resolve: {

							brand : ['BrandService', '$stateParams', '$q', function(BrandService, $stateParams, $q) {
								var defer = $q.defer();

								BrandService.getBrand($stateParams.brandId).success(function(data) {
									defer.resolve(data);
								});

								return defer.promise;
							}]
						}
					},
				},

			})


			.state('app.brand.featured', {
				url: '/featured',
				templateUrl: 'js/modules/brands/partials/brand.featured.html',
				controller: 'ItemsController',

				resolve: {

					items: ['ItemService', '$stateParams', '$q', function(ItemService, $stateParams, $q) {

						var defer = $q.defer();

						ItemService.getItemsByBrandRandom($stateParams.brandId).then(function(response) {

							defer.resolve(response.data);

						}, function(response) {
							console.log('Oh no.......');
						});

						return defer.promise;

					}]
				}
			})

			.state('app.brand.collections', {
				url: '/collections',
				templateUrl: 'js/modules/brands/partials/brand.collections.html',
				controller: 'CollectionsController',

				resolve: {

					collections: ['CollectionService', '$stateParams', '$q', function(CollectionService, $stateParams, $q) {

						var defer = $q.defer();

						CollectionService.getCollectionsByBrand($stateParams.brandId).then(function(response) {

							defer.resolve(response.data);

						}, function(response) {
							console.log('Oh no.......');
						});

						return defer.promise;

					}]
				}

			})

			.state('app.brand.newest', {
				url: '/newest',
				templateUrl: 'js/modules/brands/partials/brand.newest.html',
				controller: 'ItemsController',

				resolve: {

					items: ['ItemService', '$stateParams', '$q', function(ItemService, $stateParams, $q) {

						var defer = $q.defer();

						ItemService.getItemsByBrand($stateParams.brandId).then(function(response) {

							defer.resolve(response.data);

						}, function(response) {
							console.log('Oh no.......');
						});

						return defer.promise;

					}]
				}
			})








			.state('app.lists', {
				url: '/lists-by-user/:userId',

				views: {
					'menuContent' :{
						templateUrl: 'js/modules/lists/partials/lists.html',
						controller: 'ListsController',

						resolve: {

							lists: ['ListService', '$stateParams', '$q', function(ListService, $stateParams, $q) {
								var defer = $q.defer();

								ListService.getListsByUser($stateParams.userId).success(function(data) {
									console.log(data);
									defer.resolve(data);
								});

								return defer.promise;
							}]
						}

					}
				}
			})


			.state('app.list', {
				url: '/list/:listId',

				views: {
					'menuContent' :{
						templateUrl: 'js/modules/lists/partials/list.html',
						controller: 'ListController',

						resolve: {

							list: ['ListService', '$stateParams', '$q', function(ListService, $stateParams, $q) {
								var defer = $q.defer();

								ListService.getList($stateParams.listId).success(function(data) {
									defer.resolve(data);
								});

								return defer.promise;
							}]
						}

					}
				}
			})

			.state('app.list-map', {
				url: '/list/:title/map/:item_ids/:max_distance',

				views: {
					'menuContent': {
						templateUrl: 'js/modules/lists/partials/list-map.html',
						controller: 'ListMapController',

						resolve: {
							shops : ['ShopService', '$q', '$stateParams', function(ShopService, $q, $stateParams) {
								
								var defer = $q.defer();

								ShopService.getShopsWithItemsWithin($stateParams.item_ids, $stateParams.max_distance).then(function(response) {
									defer.resolve(response.data);
								}, function() {
									console.log("Oops");
								});

								return defer.promise;
							}]
						}

					}
				}
			})



			.state('app.messages', {
				url: '/messages',
				
				views: {
					'menuContent' :{
						templateUrl: 'js/modules/conversations/partials/conversations.html',
						controller: 'ConversationsController',

						resolve: {

							conversations : ['ConversationService', '$q', function(ConversationService, $q) {
								var defer = $q.defer();

								ConversationService.getConversations().success(function(data) {
									defer.resolve(data);
								});

								return defer.promise;
							}]
						}
					},
				},
			})








			.state('app.empty', {
				url: '/empty',
				views: {
					'menuContent' :{
						template: 'Emptiness',
					}
				}			
			})

			.state('app.test', {
				url: '/test',

				views: {
					'menuContent' :{
						templateUrl: 'test.html',
						controller: 'TestController',

						resolve: {
							shops : ['ShopService', '$q', function(ShopService, $q) {
								
								var defer = $q.defer();
								ShopService.getShops().then(function(response) {

									defer.resolve(response.data);

								}, function() {
									console.log("Oops");
								});

								return defer.promise;
							}],

							items : ['ItemService', '$q', function(ItemService, $q) {
								
								var defer = $q.defer();
								ItemService.getItems().then(function(response) {

									defer.resolve(response.data);

								}, function() {
									console.log("Oops, no items");
								});

								return defer.promise;
							}]


						}						
					}
				}
			})

	}
]);
var AuthenticationModule = angular.module('AuthenticationModule', []);
var BrandModule = angular.module('BrandModule', []);
var CollectionModule = angular.module('CollectionModule', []);
var ItemModule = angular.module('ItemModule', []);
var ShopModule = angular.module('ShopModule', []);
var UserModule = angular.module('UserModule', []);
var ConversationModule = angular.module('ConversationModule', []);
var ListModule = angular.module('ListModule', []);

CloqetApp.controller('TestController', ['$scope', '$cordovaCamera', '$cordovaGeolocation', 'items', 'shops',

	function($scope, $cordovaCamera, $cordovaGeolocation, items, shops) {



		$scope.shops = shops;
		console.log(shops);
		$scope.items = items;






  $scope.options = {
    map: {
      center: new google.maps.LatLng(55.6809086, 12.5792932),
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    },
  };
  
  $scope.triggerOpenInfoWindow = function(shop) {
    $scope.markerEvents = [
      {
        event: 'openinfowindow',
        ids: [shop.id]
      },
    ];
  }

























		$scope.getPhoto = function() {
			
			console.log('Getting camera');

			var options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : true,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 100,
				targetHeight: 100,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false
			};

			$cordovaCamera.getPicture(options).then(function(imageData) {
				console.log("Success");
			}, function(err) {
				console.log("Err");
			});

		}

		$scope.directions = function() {
			window.location = "maps:daddr=" + $scope.toLat + "," + $scope.toLng + "&saddr=" + $scope.lat + "," + $scope.lng;
		}



/*
		if (typeof(Number.prototype.toRad) === "undefined") {
		  Number.prototype.toRad = function() {
		    return this * Math.PI / 180;
		  }
		}
		var distance = function(lon1, lat1, lon2, lat2) {
			var R = 6371; // Radius of the earth in km
			var dLat = (lat2-lat1).toRad();  // Javascript functions in radians
			var dLon = (lon2-lon1).toRad(); 
			var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
					Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
					Math.sin(dLon/2) * Math.sin(dLon/2); 
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
			var d = R * c; // Distance in km

			if (d < 1) {
				d *= 1000;
				d = Math.round(d) + " meters";
			}
			else {
				d = Math.round( d * 10 ) / 10;
				d = d + " km";
			}
			return d;
		}*/


/*
		$scope.toLat = 55.681347;
		$scope.toLng = 12.57573;

		$scope.toLat = 55.6721639;
		$scope.toLng = 12.5619598;
		$scope.lat = 0;
		$scope.lng = 0;

		$scope.distance = 0;

		$cordovaGeolocation.getCurrentPosition()
			.then(function (position) {
			
				$scope.lat = position.coords.latitude;
				$scope.lng = position.coords.longitude;

				$scope.distance = distance($scope.lng, $scope.lat, $scope.toLng, $scope.toLat);

			}, function(err) {
			});

*/




	}

]);
AuthenticationModule.controller('AuthenticationController', ['$scope', '$ionicModal', 'AuthenticationService', 'authService', '$http',
	function ($scope, $ionicModal, AuthenticationService, authService) {

		$scope.login = function() {
//			AuthenticationService.login($scope.email, $scope.password);
		}

		$scope.logout = function() {
			AuthenticationService.logout();
		}

		$scope.isLoggedIn = function() {
			return AuthenticationService.isLoggedIn();
		}

        $scope.getUserId = function() {
            return AuthenticationService.getUserId();
        }

		$scope.getBrandId = function() {
			return AuthenticationService.getBrandId();
		}

        $scope.getUsername = function() {
            return AuthenticationService.getUsername();
        }

		$ionicModal.fromTemplateUrl('js/modules/authentication/partials/modal-login.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
		});

		$scope.openModal = function() {
			AuthenticationService.logout();
			$scope.modal.show();
		};
		
		$scope.closeModal = function() {
			$scope.modal.hide();
		};
		
		//Cleanup the modal when we're done with it!
		$scope.$on('$destroy', function() {
			$scope.modal.remove();
		});

        $scope.user = {
            email: "privat@mail.dk",
            password: "hej123"
        }

        $scope.ok = function() {

            AuthenticationService.login($scope.user.email, $scope.user.password).then(function() {
                $scope.closeModal();
            }, function(response) {
                console.log('authcon');
            	console.log(response.data);
            });
            
        };


        $scope.error = function() {
        	$http.get($rootScope.root + '/auth/unauthorized').then(function(response) {
        		console.log(response.data);
        	}, function(response) {
        		console.log(response.data);
        	});
        }

        $scope.register = function() {
            AuthenticationService.openRegisterModal();
        }


        $scope.$on('event:auth-loginRequired', function() {
			$scope.openModal();
        });


	}
]);
AuthenticationModule.service('AuthenticationService', ['$rootScope', '$http',
	function($rootScope, $http) {

		this.login = function(email, password) {

			console.log('Logging in');


			var postData = {
				email: email,
				password: password
			};

			console.log(postData);

			return $http.post($rootScope.root + '/auth/login', postData).then(function(response) {

				console.log(response.data);

				localStorage.setItem('user_id', response.data.user_id);
				localStorage.setItem('picture', response.data.picture);

				if (typeof(response.data.brand_id) != 'undefined') {
					localStorage.setItem('brand_id', response.data.brand_id);
				}

				else if (typeof(response.data.shop_id) != 'undefined') {
					localStorage.setItem('shop_id', response.data.shop_id);
				}

				localStorage.setItem('username', response.data.username);

			}, function(response) {
				console.log(response.data);
				//$rootScope.toaster.error('Error', response.data);
			});
		};

		this.logout = function() {

			$http.post($rootScope.root + '/auth/logout').then(function(response) {
				
			});

			localStorage.removeItem('user_id');
			localStorage.removeItem('username');
			localStorage.removeItem('shop_id');
			localStorage.removeItem('brand_id');
			localStorage.removeItem('picture');

			$rootScope.$broadcast('loggedOut');
		};

		this.getUserId = function() {
			return localStorage.getItem('user_id');
		};


		this.getBrandId = function() {
			return localStorage.getItem('brand_id');
		};

		this.getUsername = function() {
			return localStorage.getItem('username');
		}

		this.getUserPicture = function() {
			return localStorage.getItem('picture');
		}

		this.isLoggedIn = function() {
			if (localStorage.getItem('user_id') != null) {
				return true;
			}
			return false;
		};

		this.openRegisterModal = function() {
           var modalInstance = $modal.open({
                templateUrl: 'js/modules/authentication/partials/modal-register.html',
                controller: 'RegisterController'
            });

            modalInstance.result.then(function() {
                
            }, function() {
            });            			
		}

		this.registerPerson = function(person) {
			var data = {
				first_name: person.first_name,
				last_name: person.last_name,
				nickname: person.nickname,
				email: person.email,
				gender: person.gender,
				day: person.birthday.day,
				month: person.birthday.month.id,
				year: person.birthday.year,
				password: person.password
			};

			return $http.post($rootScope.root + '/auth/register-person', data);
		}

		this.registerStoreOrBrand = function(data) {
			return $http.post($rootScope.root + '/auth/register-store-or-brand', data);
		}

}]);
AuthenticationModule.controller('LoginController', ['$scope', '$modalInstance', 'AuthenticationService',
    function($scope, $modalInstance, AuthenticationService) {

        $scope.email = "reckpatrick@gmail.com";
        $scope.password = "hej123";

        $scope.ok = function() {

            AuthenticationService.login($scope.email, $scope.password).then(function() {
                $modalInstance.close();
            });
            
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.register = function() {
            $modalInstance.dismiss('cancel');
            AuthenticationService.openRegisterModal();
        };

    }
]);
AuthenticationModule.controller('RegisterController', ['$scope', 'AuthenticationService', '$modalInstance',
	function ($scope, AuthenticationService, $modalInstance) {

        $scope.type = null;
        $scope.step = 1;

        $scope.cancel = function() {
            $modalInstance.dismiss();
        }

        $scope.setStep = function(step) {
            $scope.step = step;
        }

        $scope.nextStep = function() {
            $scope.step += 1;
        }

        $scope.reset = function() {
            $scope.step = 1;
        }

        $scope.setType = function(type) {
            $scope.type = type;
        }

        $scope.isSelected = function(type) {
            if (type == $scope.type) {
                return true;
            }
            return false;
        }

        $scope.days = [];
        $scope.day = null;

        var populateDays = function() {
            for (var i = 1; i <= 31; i++) {
                $scope.days.push(i);
            };
        }
        populateDays();

        $scope.months = [
            {id: 1, name: 'January'},
            {id: 2, name: 'February'},
            {id: 3, name: 'March'},
            {id: 4, name: 'April'},
            {id: 5, name: 'May'},
            {id: 6, name: 'June'},
            {id: 7, name: 'July'},
            {id: 8, name: 'August'},
            {id: 9, name: 'September'},
            {id: 10, name: 'October'},
            {id: 11, name: 'November'},
            {id: 12, name: 'December'},
        ];

        $scope.years = [];
        $scope.year = null;

        var populateYears = function() {
            for (var i = 2010; i >= 1910; i--) {
                $scope.years.push(i);
            };
        }
        populateYears();

        
        /* Registering */


        $scope.person = {
            first_name: null,
            last_name: null,
            email: null,
            gender: 1,
            birthday: {
                day: null,
                month: null,
                year: null,
            },
        }
        
        $scope.data = {
            name: null,
            company_name: null,
            phone: null,
            email: null,
            cvr: null,
            website: null,
            contactperson: {
                title: 2,
                first_name: null,
                last_name: null,
                phone: null,
                email: null,
            },
            address: {
                street: null,
                postal: null,
                city: null,
                country: null,
            }
        }

        $scope.register = function() {

            if ($scope.type == 1) {
                AuthenticationService.registerPerson($scope.person).then(function(response) {
                    $scope.toaster.success('Congratulations', 'You have successfully registered a user. Please activate your account using the link we e-mailed you.')
                    $modalInstance.close();
                }, function(response) {
                    console.log("An errr occured");
                });
            }

            else {
                $scope.data.type = $scope.type;
                AuthenticationService.registerStoreOrBrand($scope.data).then(function(response) {
                    $scope.toaster.success('Congratulations', 'You have successfully registered. A confirmation have been sent to the attached e-mail address.')
                    $modalInstance.close();
                });
            }

        }

	}
]);
BrandModule.controller('BrandController', ['$scope', 'brand', 'BrandService',
	
	function($scope, brand, BrandService) {

		$scope.$emit('changeBackground', brand.cover);
	
		$scope.brand = brand;

		$scope.info = false;
		$scope.toggleInfo = function() {
			$scope.info = !$scope.info;
		}

        console.log(brand);


        $scope.sendMessage = function() {

            var modalInstance = $modal.open({
                templateUrl: 'js/modules/conversations/partials/modal-send-message.html',
                controller: 'ModalStartConversationController',
                size: 'lg',
                resolve: {
                    userdata: function() {
                        return { id: brand.user_id, name: brand.brand_name };
                    }
                }
            });

            modalInstance.result.then(function() {
            }, function() {
            });
        };


        $scope.likes = brand.likes;
        $scope.i_like = brand.i_like;

        $scope.like = function() {

            var old_i_like = $scope.i_like;
            var old_likes = $scope.likes;

            if ($scope.isLoggedIn()) {
                if ($scope.i_like == true) {
                    $scope.likes--;
                }            
                else {
                    $scope.likes++;
                }
                
                $scope.i_like = !$scope.i_like;
            }

            BrandService.like(brand.id).then(function(data) {
                console.log(data);
            }, function() {
                $scope.i_like = old_i_like;
                $scope.likes = old_likes;
            });

        }

	}

]);
BrandModule.service('BrandService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getBrands = function() {
			return $http.get($rootScope.root + '/brands');
		}

		this.getBrand = function(brandId) {
			return $http.get($rootScope.root + '/brands/brand/' + brandId);
		}

		this.like = function(brandId) {
			return $http.post($rootScope.root + '/brands/like', {brand_id: brandId});
		}

	}

]);
BrandModule.controller('BrandsController', ['$scope', 'brands',

	function($scope, brands) {

		$scope.$emit('changeBackground', 'default');

		$scope.brands = brands;


	}

]);
CollectionModule.controller('CollectionController', ['$scope', 'collection', '$interval', '$sce',
	
	function($scope, collection, $interval, $sce) {

		$scope.collection = collection;  

		$scope.collection.description = $sce.trustAsHtml($scope.collection.description); 

		$scope.images = [
			'http://cdn.shopify.com/s/files/1/0226/9103/files/ss14-3_grande.jpg?1822',
			'http://localhost/cloqet_server/public/cover-images/savannahwild.jpg',
			'http://localhost/cloqet_server/public/cover-images/hugoboss.jpg',
			'http://localhost/cloqet_server/public/cover-images/gstar.jpg',
		];

		$scope.slideIndex = 0;

		$scope.nextIndex = function() {
			$scope.slideIndex++;
		}

		$scope.prevIndex = function() {
			$scope.slideIndex--;
		}

		var paused = false;

		$scope.changeSlide = function(){
			if (paused) return;
			if ($scope.slideIndex == $scope.images.length - 1) {
				$scope.slideIndex = 0;
			}
			else {
				$scope.slideIndex++;
			}
		};

		$interval($scope.changeSlide, 6000);

		$scope.pause = function() {
			paused = true;
			console.log("Pause");
		}



	}

]);
CollectionModule.service('CollectionService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getCollectionsByBrand = function(brandId) {
			return $http.get($rootScope.api + '/collections/collections-by-brand/' + brandId);
		}

		this.getCollection = function(collectionId) {
			return $http.get($rootScope.api + '/collections/collection/' + collectionId);
		}

		this.saveCollection = function(name, description, items) {
			var data = {
				name: name,
				description: description,
				items: items
			};

			return $http.post($rootScope.api + '/collections/save-collection', data);
		}
	}

]);
CollectionModule.controller('CollectionsController', ['$scope', 'collections',
	
	function($scope, collections) {

		$scope.collections = collections;
		console.log('Collections');
		console.log($scope.collections);

	
	}

]);

CollectionModule.controller('CreateCollectionController', [
	'$scope', '$modal', '$q', 'CollectionService', 'ItemService', 'items', '$state',

	function ($scope, $modal, $q, CollectionService, ItemService, items, $state) {

		$scope.items = items;

        $scope.updateBox = {};

        $scope.submitted = false;

        $scope.publish = 1;

        $scope.openAddItems = function() {

            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: 'SelectItemsController',
                size: 'lg',
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function(tickedItems) {
            }, function() {
                console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.saveCollection = function() {

            if ($scope.form.$invalid) {
                $scope.submitted = true;
                $scope.$emit('scrollToTop');
                return;
            }

            var addedItems = [];

            angular.forEach($scope.items, function(item, key) {
                if (item.ticked == 1) {
                    addedItems.push(item.id);
                }
            });

            CollectionService.saveCollection($scope.name, $scope.description, addedItems).then(function(response) {
                console.log(response.data);
                var data = response.data;
                $state.go('brand.collection', {'brandId': data.brand_id, 'brandName': $scope.getSlugified(data.brand_name), 'collectionId': data.id, 'collectionName': $scope.getSlugified(data.name)});
            });
        }

	}
]);
CollectionModule.controller('SelectItemsController', ['$scope', '$modalInstance', 'items',
    function($scope, $modalInstance, items) {

    	$scope.items = items;

		$scope.ok = function () {
			$modalInstance.close();
		};

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);
ConversationModule.controller('ConversationController', ['$scope', 'ConversationService', 'conversation', '$modal',

	function($scope, ConversationService, conversation, $modal) {

		$scope.$emit('changeBackground', 'default');

		$scope.conversation = conversation;

		$scope.refreshing = false;

		$scope.reply = function() {

            var modalInstance = $modal.open({
                templateUrl: 'js/modules/conversations/partials/reply-message.html',
                controller: 'ModalReplyController',
                size: 'lg',
                resolve: {
                	conversation: function() {
                		return $scope.conversation;
                	}
                }
            });

            modalInstance.result.then(function() {
            	$scope.refresh();
            }, function() {
            	console.log("Cancelled");
            });

		}

		$scope.refresh = function() {
			$scope.refreshing = true;
			ConversationService.getConversation(conversation.id).then(function(response) {
				$scope.conversation = response.data;
				$scope.refreshing = false;
			});
		}
        
	}

]);
ConversationModule.service('ConversationService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getConversations = function() {
			return $http.get($rootScope.api + '/conversations');
		}

		this.getConversation = function(conversationId) {
			return $http.get($rootScope.api + '/conversations/conversation/' + conversationId);
		}

		this.startConversation = function(to, subject, message) {
			console.log(to);
			return $http.post($rootScope.api + '/conversations/start-conversation', { to: to, subject: subject, message: message });
		}

		this.sendMessage = function(conversation, message) {
			return $http.post($rootScope.api + '/conversations/message', { conversation: conversation, message: message });
		}

		this.deleteConversation = function(conversation_id) {
			return $http.post($rootScope.api + '/conversations/delete', { conversation_id: conversation_id });
		}

	}

]);
ConversationModule.controller('ConversationsController', ['$scope', 'conversations', 'ConversationService', '$state',

	function($scope, conversations, ConversationService, $state) {

		$scope.$emit('changeBackground', 'default');

		$scope.conversations = conversations;

        $scope.$on('loggedOut', function() {
            $scope.goToFrontpage();
        });

		$scope.deleteConversation = function(conversation) {

		}

		$scope.refreshing = false;
		$scope.refresh = function() {
			$scope.refreshing = true;

			ConversationService.getConversations().then(function(response) {
				$scope.conversations = response.data;
				$scope.refreshing = false;
			});
		}

	}

]);
ConversationModule.controller('ModalReplyController', ['$scope', '$modalInstance', 'ConversationService', 'conversation',

    function($scope, $modalInstance, ConversationService, conversation) {

        $scope.message = "";
        $scope.conversation = conversation;

        $scope.ok = function() {
            
            ConversationService.sendMessage(conversation.id, $scope.message).then(function(response) {
                console.log(response.data);
                $scope.toaster.success('Success', 'Your message was sent to ' + conversation.other.name);

                $modalInstance.close();

            }, function(response) {
                
                console.log(response.data);
                $scope.toaster.error('Error', 'Your message was not delivered');

            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);


ConversationModule.controller('ModalStartConversationController', ['$scope', '$modalInstance', 'ConversationService', 'userdata',

    function($scope, $modalInstance, ConversationService, userdata) {

        $scope.id = userdata.id;
        $scope.name = userdata.name;
        $scope.subject = "";
        $scope.message = "";

        $scope.ok = function() {

            ConversationService.startConversation($scope.id, $scope.subject, $scope.message).then(function(response) {
                console.log(response.data);
                $scope.toaster.success('Success', 'Your message was sent to ' + $scope.name);

                $modalInstance.close();

            }, function(response) {
                
                console.log(response.data);
                $scope.toaster.error('Error', 'Your message was not delivered');

            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);


ItemModule.controller('AddToShopController', ['$scope', 'ItemService', 'ShopService', '$modal',
    function($scope, ItemService, ShopService, $modal) {

        $scope.shops = [];
        $scope.success_box = false;

        $scope.updateShops = function() {

            ItemService.getShopAttachments($scope.item.id).then(function(response) {
                $scope.shops = response.data;

                angular.forEach($scope.shops, function(shop, key) {

                    if (shop.attached == null) {
                        shop.checked = false;
                    }
                    else {
                        shop.checked = true;
                    }
                });

            }, function() {
                console.log("Error");
            });

        };
        $scope.updateShops();


        $scope.checkboxChanged = function(shop) {
        	shop.changed = true;
        }


        $scope.attachShops = function() {

        	var changedShops = [];

            angular.forEach($scope.shops, function(shop, key) {
                if (shop.changed == true) {
                	changedShops.push({ shop_id: shop.id, checked: shop.checked });
                }
            });


            if (changedShops.length <= 0) {
            	$scope.toaster.warning('Warning', 'You did not make any changes!');
            	return;
            }

            ShopService.attachItems({item: $scope.item.id, shops: changedShops}).then(function(response) {

	            $scope.toaster.success('Yay!', 'The shop attachments has been updated')
	
	            ItemService.getItem($scope.item.id).then(function(response) {
	            	console.log(response.data);
	            	$scope.$parent.item = response.data;
	            });

	            angular.forEach($scope.shops, function(shop, key) {
	            	if (shop.changed == true) {
	            		shop.changed = false;
	            	}
	            })

            });



        }


        $scope.open = function(size) {

            var modalInstance = $modal.open({
                templateUrl: 'addShop.html',
                controller: 'AddShopShortController'
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.updateShops();
            }, function() {
                console.log('Modal dismissed at: ' + new Date());
            });
        };


    }
]);

ItemModule.controller('CreateItemController', [
    '$rootScope',
    '$http',
    '$scope',
    '$location',
    'ItemService',
    '$upload',
    '$modal',
    '$parse',
    '$state',

    function($rootScope, $http, $scope, $location, ItemService, $upload, $modal, $parse, $state) {



        $scope.step = 1;

        $scope.setStep = function(step) {
            $scope.step = step;
            $scope.errorBox.active = false;
        }

        $scope.next = function() {

            $scope.errorBox.active = false;

            if ($scope.step == 1) {

                $scope.resetErrors();

                if (!$scope.name ||
                    !$scope.gender ||
                    !$scope.description ||
                    $scope.prices.length == 0 ||
                    !$scope.selectedCategory
                    ) {

                    if (!$scope.name) {
                        $scope.name_error = true;
                    }

                    if (!$scope.description) {
                        $scope.description_error = true;
                    }

                    if ($scope.prices.length == 0) {
                        $scope.price_error = true;
                    }

                    $scope.errorBox.active = true;
                    $scope.errorBox.text = "Please fill out the missing data";

                    return;

                }
            }

            if ($scope.step == 2) {

                if (!$scope.havePicture()) {
                    $scope.errorBox.text = "You need to add at least 1 picture";
                    $scope.errorBox.active = true;
                    return false;
                }

            }


            if ($scope.step == 3) {
                $scope.createItem();
                return;
            }

            $scope.errorBox.active = false;

            $scope.step += 1;
        }
        
        $scope.back = function() {
            $scope.errorBox.active = false;

            if ($scope.step == 1)
                return;

            $scope.step -= 1;
        }



        $scope.errorBox = { active: false, text: "Some data is missing from the form" };
        $scope.resetErrors = function() {
            $scope.name_error = false;
            $scope.description_error = false;
            $scope.price_error = false;
        }


        $scope.createItem = function() {
            var postData = {
                name: $scope.name,
                description: $scope.description,
                product_code: $scope.item_number,
                size_fit: $scope.size_fit,
                gender: $scope.gender,
                colors: $scope.chosenColors,
                countries: $scope.prices,
                //construction: $scope.selectedConstruction,
                //webshop_url: $scope.webshop_url,
                categoryId: $scope.selectedCategory.id,
                fabrics: $scope.addedFabrics
            };

            console.log("Submitting to server");
            return ItemService.createItem(postData).then(function(response) {
                
                console.log('Promise recieved');
                console.log(response.data);

                $scope.toaster.success('Congratulations', 'You just created an item!');

                var item = response.data;
                $state.go('item.add-to-shops', {'itemId': item.id, 'itemName': item.slug});


//                $location.path('/items/' + response.data['id'] + '/' + response.data['slug'] + '/show');

            });
        }


        $scope.gender = 1;


        /* Categories */

        $scope.selectedCategory = null;

        $scope.category1_categories = [];
        $scope.category2_categories = [];
        $scope.category3_categories = [];
        $scope.category4_categories = [];

        ItemService.getCategoriesByParent('').then(function(response) {
            $scope.category1_categories = response.data;
            $scope.category1 = response.data[0];
            $scope.categoryChanged($scope.category1);
        });
  
        $scope.categoryChanged = function(category) {

            if (typeof category != 'undefined') {
                ItemService.getCategoriesByParent(category['id']).then(function(response) {

                    if (category === $scope.category1) {
                        $scope.category2_categories = null;
                        $scope.category3_categories = null;
                        $scope.category4_categories = null;                        

                        $scope.category2_categories = response.data;
                        $scope.category2 = response.data[0];

                        if (typeof $scope.category2 != 'undefined')
                            $scope.selectedCategory = $scope.category2;
                        else
                            $scope.selectedCategory = $scope.category1;

                        $scope.categoryChanged($scope.category2);
                    }

                    else if (category === $scope.category2) {
                        $scope.category3_categories = null;
                        $scope.category4_categories = null;

                        $scope.category3_categories = response.data;
                        $scope.category3 = response.data[0];

                        if (typeof $scope.category3 != 'undefined')
                            $scope.selectedCategory = $scope.category3;
                        else
                            $scope.selectedCategory = $scope.category2;

                        $scope.categoryChanged($scope.category3);

                    }                      

                    else if (category === $scope.category3) {
                        $scope.category4_categories = null;

                        $scope.category4_categories = response.data;
                        $scope.category4 = response.data[0];

                        if (typeof $scope.category4 != 'undefined')
                            $scope.selectedCategory = $scope.category4;
                        else
                            $scope.selectedCategory = $scope.category3;

                    }


                });    
            }     
        }


        /* Currencies */

        $scope.countries = [];

        ItemService.getCountries().then(function(response) {
            $scope.countries = response.data;
            $scope.selectedCurrency = $scope.countries[0];
        });

        $scope.prices = [];

        $scope.addPrice = function() {

            if (isNaN($scope.price) || $scope.price == "")
                return;

            $scope.prices.push({"currency" : $scope.selectedCurrency, "price" : $scope.price})
            $scope.countries.splice($scope.countries.indexOf($scope.selectedCurrency), 1);
            $scope.selectedCurrency = $scope.countries[0];

            $scope.price = "";
        }

        $scope.removePrice = function(price) {
            $scope.countries.push(price.currency);
            $scope.prices.splice($scope.prices.indexOf(price), 1);
            $scope.selectedCurrency = $scope.countries[0];
        }




        /* Materials */

        $scope.addNewMaterialTab = {
            isOpen: true,
        }

        $scope.materials = [];
        $scope.addedFabrics = [];


        var getNewTemporaryFabric = function() {
            return {
                name: '',
                contents: [],
                temporaryMaterial: {
                    material: $scope.materials[0],
                    percentage: 0
                }
            };
        }

        $scope.temporaryFabric = getNewTemporaryFabric();

        $scope.addFabric = function() {

            if (typeof($scope.temporaryFabric.name) == 'undefined') {
                $scope.errorBox.text = "You need to enter the fabrics name."
                $scope.errorBox.active = true;
                return;
            }

            if (!$scope.addsToHundred()) {
                $scope.errorBox.text = "The material contents needs to add up to 100%."
                $scope.errorBox.active = true;
                return;
            }

            delete $scope.temporaryFabric.temporaryMaterial;

            $scope.addedFabrics.push($scope.temporaryFabric);


            angular.forEach($scope.materials, function(material, key) {
                material.used = false;
            });

            $scope.temporaryFabric = getNewTemporaryFabric();

            $scope.errorBox.active = false;
        }

        $scope.addMaterial = function() {

            console.log($scope.temporaryFabric.temporaryMaterial.percentage);

            if ($scope.temporaryFabric.temporaryMaterial.percentage <= 0) {
                return;
            }

            $scope.temporaryFabric.contents.push($scope.temporaryFabric.temporaryMaterial);
            
            $scope.temporaryFabric.temporaryMaterial.material.used = true;

            // Set the new selected material to the next NOT used
            var nextMaterial = function() {
                var newMaterial = "";
                angular.forEach($scope.materials.reverse(), function(value, key) {

                    if (value.used != true) {
                        newMaterial = value;
                    }

                });

                $scope.materials.reverse();
                return newMaterial;
            };

            delete $scope.temporaryFabric.temporaryMaterial;

            $scope.temporaryFabric.temporaryMaterial = {
                material: nextMaterial(),
                percentage: 0
            }
        }

        $scope.removeMaterial = function(material) {
            material.used = false;
            $scope.temporaryFabric.contents.splice($scope.temporaryFabric.contents.indexOf(material), 1);
        }

        $scope.addsToHundred = function() {
            var percentage = 0;
            angular.forEach($scope.temporaryFabric.contents, function(material, key) {
                percentage += material.percentage;
            });

            if (percentage != 100)
                return false;

            return true;
        }

        ItemService.getMaterials().then(function(response) {
            $scope.materials = response.data;
            $scope.temporaryFabric.temporaryMaterial.material = $scope.materials[0];
        });

    




        /* Template */

        $scope.colors = {};
        ItemService.getColors().then(function(response) {
            $scope.colors = response.data;
        });

        $scope.chosenColors = [];

        $scope.addColor = function(color) {

            var duplicate = false;

            angular.forEach($scope.chosenColors, function(value, key) {

                if (value.color == color.color) {
                    duplicate = true;
                }
            })

            if (!duplicate) {
                $scope.chosenColors.push(color);
                $scope.chosenColors[$scope.chosenColors.length - 1].images = [];                
            }
        }


        $scope.removeColor = function(color) {
            $scope.chosenColors.splice($scope.chosenColors.indexOf(color), 1);
            $scope.selectedColor = [];
        }


        $scope.selectedColor = [];

        $scope.selectColor = function(color) {
            $scope.selectedColor = color;
        }

        $scope.previewImage = {};
        $scope.updatePreviewImage = function(image) {
            
            $scope.previewImage = image;
        }

        $scope.clearPreviewImage = function() {
            $scope.previewImage = {};
        }


        $scope.havePicture = function() {
            var havePicture = false;
            angular.forEach($scope.chosenColors, function(value, key) {

                if (value.images.length != 0) {
                    havePicture = true;
                }
            });

            return havePicture;
        }



        $scope.openAddPicture = function(thisColor) {

            var modalInstance = $modal.open({
                templateUrl: 'addPictureModal.html',
                controller: 'addPictureModal',
                size: 'lg',
                resolve: {
                    color: function() {
                        return thisColor;
                    }
                }
            });

            modalInstance.result.then(function(pictures) {

            }, function() {
                console.log('Modal dismissed at: ' + new Date());
            });
        };





    }
]);

ItemModule.controller('addPictureModal', ['$scope', '$modalInstance', 'color', '$upload',
    function($scope, $modalInstance, color, $upload) {

        $scope.color = color;
        console.log(color);


        /* File upload */

        $scope.onFileSelect = function($files) {

            for (var i = 0; i < $files.length; i++) {

                var file = $files[i];

                $scope.upload = $upload.upload({

                    url: $scope.root +'/products/image',
                    data: {
                        color: $scope.selectedColor
                    },
                    file: file,

                }).progress(function(evt) {
                    console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                }).success(function(data, status, headers, config) {

                    $scope.color.images.push(data);

                });
            }
        };


        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);


ItemModule.controller('ItemController', [
    '$scope',
    '$rootScope',
    'ItemService',
    'item',
    '$sce',
    '$ionicSlideBoxDelegate',
    '$filter',
    '$ionicModal',
    '$cordovaGeolocation',
    'LocationService',

    function(
        $scope,
        $rootScope,
        ItemService,
        item,
        $sce,
        $ionicSlideBoxDelegate,
        $filter,
        $ionicModal,
        $cordovaGeolocation,
        LocationService
    ) {


        $scope.item = item;

        console.log(item);

        $scope.item.description = $sce.trustAsHtml($scope.item.description);

        $scope.likes = item.likes;
        $scope.i_like = item.i_like;

        $scope.haves = item.haves;
        $scope.i_have = item.i_have;




        /* APP SPECIFICS */
        
        $scope.item.allimages = [];
        angular.forEach($scope.item.colors, function(color) {
            
            angular.forEach(color.images, function(image) {
                $scope.item.allimages.push(image);
            });
        });

        $scope.extra_info = false;
        $scope.toggleInfo = function() {
            console.log("toggling");
            $scope.extra_info = !$scope.extra_info;
            $ionicSlideBoxDelegate.enableSlide(!$scope.extra_info);
        }


        $scope.directions = function(toLng, toLat) {
            window.location = "maps:daddr=" + toLat + "," + toLng + "&saddr=" + $scope.lat + "," + $scope.lng;
        }


        LocationService.updateMyPosition().then(function() {
            angular.forEach($scope.item.shops, function(shop) {
                shop.distance = LocationService.distance(shop.lat, shop.lng);
            });
        });

/*
        $scope.lat = 0;
        $scope.lng = 0;

        $cordovaGeolocation.getCurrentPosition()
            .then(function (position) {

                $scope.lat = position.coords.latitude;
                $scope.lng = position.coords.longitude;

                console.log('lat' + $scope.lat + ' lng' + $scope.lng);

                angular.forEach($scope.item.shops, function(shop) {
                    shop.raw_distance = $scope.distance($scope.lng, $scope.lat, parseFloat(shop.lng), parseFloat(shop.lat));

                    shop.distance = $filter('metricdistance')(shop.raw_distance);
                });

            }, function(err) {

        });

*/



        /* All stores */

        $scope.distance_range = 250;

        $ionicModal.fromTemplateUrl('all-stores.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function() {
            $scope.modal.show();
        
        };
        
        $scope.closeModal = function() {
            $scope.modal.hide();
        };
        
        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.modal.remove();
        });

        $scope.sortBy = null;


























        /* IMAGE SWITCH */

        $scope.currentImage = null;

        $scope.changeCurrentImage = function(image) {
            $scope.currentImage = image;
        }


        /* LIKE */
        $scope.like = function() {

            var old_i_like = $scope.i_like;
            var old_likes = $scope.likes;

            if ($rootScope.isLoggedIn()) {
                if ($scope.i_like == true) {
                    $scope.likes--;
                }            
                else {
                    $scope.likes++;
                }
                
                $scope.i_like = !$scope.i_like;
            }

            ItemService.like(item.id).then(function(data) {
            }, function() {
                $scope.i_like = old_i_like;
                $scope.likes = old_likes;
            });

        }

        $scope.have = function() {

            var old_i_have = $scope.i_have;
            var old_haves = $scope.haves;

            if ($rootScope.isLoggedIn()) {
                if ($scope.i_have == true) {
                    $scope.haves--;
                }            
                else {
                    $scope.haves++;
                }
                
                $scope.i_have = !$scope.i_have;
            }

            ItemService.have(item.id).then(function(data) {
            }, function() {
                $scope.i_have = old_i_have;
                $scope.haves = old_haves;
            });

        }



        /* COLORS */

        $scope.currentColor = null;

        $scope.changeColor = function(color) {

            $scope.currentColor = color;

            $scope.changeCurrentImage($scope.currentColor.images[0]);
        }

        // Set the current color on load
        $scope.changeColor($scope.item.colors[0]);

        /* END COLORS */



        /* Show all shops */
        $scope.showAllShops = function() {

            var modalInstance = $modal.open({
                templateUrl: 'all-shops.html',
                controller: 'ShowAllShopsController',
                size: 'lg',
                resolve: {
                    item: function() {
                        return $scope.item;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.updateShops();
            }, function() {
                console.log('Modal dismissed at: ' + new Date());
            });
        };




    }
]);

ItemModule.controller('ShowAllShopsController', ['$scope', '$modalInstance', 'item',
    function($scope, $modalInstance, item) {

        $scope.item = item;

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);


ItemModule.controller('ItemMediaController', ['$scope', 'ItemService',
    function($scope, ItemService) {

        $scope.title = false;
        $scope.images = [];
        $scope.currentImage = false;
        $scope.loading = false;



        $scope.nextImage = function() {
            if ($scope.currentImage != $scope.images.length - 1) {
                $scope.currentImage++;
            }
        }
        $scope.previousImage = function() {
            if ($scope.currentImage != 1) {
                $scope.currentImage--;
            }
        }

        $scope.updatedUrl = function() {

            var urlRegex = new RegExp('^(https?:\\/\\/)?((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|((\\d{1,3}\\.){3}\\d{1,3}))(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*(\\?[;&a-z\\d%_.~+=-]*)?(\\#[-a-z\\d_]*)?$','i');

            $scope.title = "";
            $scope.images = [];
            $scope.currentImage = false;
            $scope.loading = false;

            if (urlRegex.test($scope.url)) {
                $scope.loading = true;
                console.log('isUrl');

                ItemService.getMediaData($scope.url).then(function(response) {

                        console.log('Url data recieved')
                        console.log(response.data);


                    angular.forEach(response.data.images, function(image) {
                        $scope.images.push({url: image});
                        $scope.currentImage = 1;

                    });
                    $scope.title = response.data.title;
                    $scope.loading = false;

                }, function(response) {
                    $scope.loading = false;
                });

            }
            else {
                console.log('isNotUrl');
            }
            
        }


        $scope.saveMediaTag = function() {
            console.log("Saving....");
            $scope.loading = true;

            ItemService.saveMediaTag($scope.item.id, $scope.url, $scope.title, $scope.images[$scope.currentImage].url).then(function(response) {
                console.log(response.data);
                $scope.toaster.success('Success', 'You have tagged a media link!');

                ItemService.getItem($scope.item.id).then(function(response) {
                    $scope.$parent.item = response.data;

                    $scope.title = "";
                    $scope.url = "";
                    $scope.images = [];
                    $scope.currentImage = false;
                    $scope.loading = false;

                });
                
            }, function() {
                $scope.toaster.error('Error', '');
            });
        }

    }

]);


ItemModule.service('ItemService', ['$rootScope', '$http', 
	function($rootScope, $http) {
		
		this.getItems = function() {
			return $http.get($rootScope.root + '/products', {
				cache: true
			});
		}

		this.getMoreItems = function(skip) {
			return $http.get($rootScope.root + '/products/more/' + skip);
		}

		this.getItem = function(itemId) {
			return $http.get($rootScope.root + '/products/show/' + itemId);
		}

		this.getItemsByBrand = function(brandId) {
			return $http.get($rootScope.root + '/products/items-by-brand/' + brandId);
		}

		this.getItemsByBrandRandom = function(brandId) {
			return $http.get($rootScope.root + '/products/items-by-brand/' + brandId + '/' + true);
		}

		this.getItemsByShop = function(shopId) {
			return $http.get($rootScope.root + '/products/items-by-shop/' + shopId);
		}

		this.getItemsByShopRandom = function(shopId) {
			return $http.get($rootScope.root + '/products/items-by-shop/' + shopId + '/' + true);
		}

		this.getMyItems = function() {
			return $http.get($rootScope.root + '/products/my-items');
		}

		this.searchItems = function(parameters) {
			return $http.get($rootScope.root + '/products/search/' + parameters);
		}

		this.getCategories = function() {
			return $http.get($rootScope.root + '/categories');
		}

		this.getCategoriesByParent = function(parent) {
			return $http.get($rootScope.root + '/categories/subcategories/' + parent);
		}

		this.getCountries = function() {
			return $http.get($rootScope.root + '/countries');
		}

		this.getColors = function() {
			return $http.get($rootScope.root + '/colors');
		}

		this.getConstructions = function() {
			return $http.get($rootScope.root + '/constructions');
		}

		this.getMaterials = function() {
			return $http.get($rootScope.root + '/materials');
		}

		this.createItem = function(postData) {
			return $http.post($rootScope.root + '/products/store', postData);
		}

		this.getShopAttachments = function(item_id) {
			return $http.get($rootScope.root + '/products/shop-attachments/' + item_id);
		}

		this.like = function(item_id) {
			return $http.post($rootScope.root + '/products/like', {item_id: item_id});
		}

		this.have = function(item_id) {
			return $http.post($rootScope.root + '/products/have', {item_id: item_id});
		}

		this.getMediaTags = function(item_id) {
			return $http.get($rootScope.root + '/products/media-tags/' + item_id);
		}

		this.getMediaData = function(userUrl) {
			return $http.post($rootScope.root + '/media/url', {url: userUrl});
		}

		this.saveMediaTag = function(item_id, url, title, image) {
			return $http.post($rootScope.root + '/media/tag', {item_id: item_id, url: url, title: title, imagepath: image});
		}

	}
]);
ItemModule.controller('ItemSettingsController', ['$scope', 'ItemService', 'ShopService', '$state', '$sce',
    function($scope, ItemService, ShopService, $state, $sce) {

        $scope.tabs = [
            { title: 'General', route: 'general' },
            { title: 'Publishing' },
            { title: 'Shop links <span class="badge">3</span>', route: 'add-to-shops' },
            { title: 'Materials' },
            { title: 'Media links', route: 'media' },
            { title: 'Pricing', route: 'pricing' },
        ];

        angular.forEach($scope.tabs, function(tab) {
            tab.title = $sce.trustAsHtml(tab.title);
        });

        $scope.test = "<b>hej</b>";
        $scope.testa = $sce.trustAsHtml($scope.test);

    }
]);

ItemModule.controller('ItemsController', ['$scope', 'items', '$cordovaSplashscreen', 'ItemService',
	function ($scope, items, $cordovaSplashscreen, ItemService) {


		$scope.items = items;
/*
		angular.forEach($scope.items, function(item, index) {
			var foundIt = false;
			angular.forEach(item.images, function(image) {
				console.log('oops');
				if (!foundIt) {
					console.log('Gotcha');
					item.images[0].path = 'thumb_' + image.path;
					foundIt = true;
				}
			});
		});
*/

		$scope.noMoreItems = false;

		$scope.loadMoreData = function() {
			console.log('Loading more');

			ItemService.getMoreItems($scope.items.length).then(function(response) {
				if (response.data.length > 0) {

					angular.forEach(response.data, function(item) {
						$scope.items.push(item);
					});

					angular.forEach($scope.items, function(item, index) {
						var foundIt = false;
						angular.forEach(item.images, function(image) {
							if (!foundIt) {
								item.images[0].path = 'thumb_' + image.path;
								foundIt = true;
							}
						});
					});
				}
				else {
					$scope.noMoreItems = true;
				}

				$scope.$broadcast('scroll.infiniteScrollComplete');

			});

		}


		
	}
]);

ListModule.controller('EditListController', ['$scope',
	
	function($scope) {

	
	}

]);

ListModule.controller('ListController', ['$scope', 'list', '$sce', '$interval',
	
	function($scope, list, $sce, $interval) {

		$scope.$parent.breadcrumb = "Lists / " + list.name;

		$scope.list = list;
	
		$scope.list.description = $sce.trustAsHtml(list.description);

		$scope.item_ids = "";

		var index = 0;
		angular.forEach($scope.list.items, function(item) {
			if (index + 1 == $scope.list.items.length) {
				$scope.item_ids = $scope.item_ids + item.id;
			}
			else {
				$scope.item_ids = $scope.item_ids + item.id + '-';
			}

			index++;
		});

		$scope.testy = function() {

			$scope.class = 'bouncy';
			$interval(function() {
				console.log("woop");
				$scope.class = null;
			}, 1000, 1);
		}

		
	}

]);
ListModule.controller('ListMapController', ['$scope', 'shops', 'LocationService', '$stateParams', '$ionicModal',
	function ($scope, shops, LocationService, $stateParams, $ionicModal) {

		$scope.showShop = function(evt, id) {
			$scope.shop = $scope.shops[id];
			console.log($scope.shop);
			console.debug($scope.shop);

			angular.element(document.getElementById('foo')).scope().shop = $scope.shop;
			angular.element(document.getElementById('foo')).scope().showInfoWindow(evt, 'foo', this);
		};

		$scope.title = $stateParams.title;

    	$scope.map;
	    $scope.$on('mapInitialized', function(evt, evtMap) {
			$scope.map = evtMap;
	    });

	    $scope.shops = [];
	    $scope.items = [];

	    // Format and populate items
	    angular.forEach(shops, function(shop) {
	    	shop.position = [shop.lat, shop.lng];

	    	angular.forEach(shop.items, function(item) {
	    		item.images = [{path: item.image}];

	    		var alreadyExists = false;
	    		angular.forEach($scope.items, function(uniqueItem) {
	    			if (uniqueItem.id == item.id)
	    				alreadyExists = true;
	    		})
	    		if (!alreadyExists)
		    		$scope.items.push(item);
	    	});

	    	$scope.shops.push(shop);
	    });

	    $scope.tickedItems = function() {
	    	var tickedItems = false;

	    	angular.forEach($scope.items, function(item) {
	    		if (item.ticked)
	    			tickedItems = true;
	    	})

	    	return tickedItems;
	    }

	    $scope.tickItem = function(item) {
	    	item.ticked = !item.ticked;
	    	console.log($scope.items);
	    	console.log(item);
//	    	$scope.sortMarkers();
	    }

	    $scope.removeTicks = function() {
	    	angular.forEach($scope.items, function(item) {
	    		item.ticked = false;
	    	});
	    	$scope.sortMarkers();
	    }

	    $scope.showFilters = true;
	    $scope.toggleFilters = function() {
	    	$scope.showFilters = !$scope.showFilters;
	    }

	    $scope.sortMarkers = function() {

	    	var shopFound = false;
	    	angular.forEach($scope.shops, function(shop) {

	    		var itemFound = false;

	    		angular.forEach(shop.items, function(shopItem) {

	    			angular.forEach($scope.items, function(item) {
	    				if (shopItem.id == item.id && item.ticked) {
	    					itemFound = true;
	    					shopFound = true;
	    				}
	    			})
	    		});

	    		if (itemFound)
	    			$scope.map.markers[shop.id].setMap($scope.map);
	    		else
	    			$scope.map.markers[shop.id].setMap(null);

	    	});

	    	if (!shopFound) {
	    		angular.forEach($scope.shops, function(shop) {
	    			$scope.map.markers[shop.id].setMap($scope.map);
	    		});
	    	}

	    }

		$scope.haveItemsFilter = function(shop) {
		    //console.log(shop);
		    
		    var isOk = false;
		    angular.forEach($scope.items, function(item) {
		        angular.forEach(shop.items, function(shopItem) {
		            if (shopItem.id == item.id && item.ticked) {
		                //console.log('Oh, got ya!');
		                //console.log(shopItem.id + ' is the same as ' + item.id + ' and ticked is ' + item.ticked);
		                isOk = true;
		            }
		        });
		    });
		    return isOk;
		};


		$scope.a = false;
		$scope.test = function() {
			$scope.a = !$scope.a;
			if ($scope.a)
				$scope.map.markers[2].setMap(null);
			else 
				$scope.map.markers[2].setMap($scope.map);				
		}


	    // Update position
		LocationService.updateMyPosition().then(function(position) {
			console.log(position);
			$scope.myPosition = [position.lat,position.lng];
			$scope.map.setCenter(new google.maps.LatLng(position.lat, position.lng));
		});


	}
]);

ListModule.service('ListService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getLists = function() {
			return $http.get($rootScope.api + '/lists');
		}

		this.getListsByItem = function(item_id) {
			return $http.get($rootScope.api + '/lists/lists-by-item/' + item_id);
		}

		this.getListsByUser = function(user_id) {
			return $http.get($rootScope.api + '/lists/lists-by-user/' + user_id);
		}

		this.getList = function(list_id) {
			return $http.get($rootScope.api + '/lists/list/' + list_id);
		}

		this.createList = function(name, description) {
			return $http.post($rootScope.api + '/lists/create', {name: name, description: description});
		}

		this.toggleItem = function(item_id, list_id) {
			return $http.post($rootScope.api + '/lists/toggle-item', {item_id: item_id, list_id: list_id});
		}

	}

]);
ListModule.controller('ListsController', ['$scope', 'lists',
	
	function($scope, lists) {

		$scope.lists = lists;
	
	}

]);

ListModule.controller('ModalCreateListController', ['$scope', '$modalInstance', 'ListService', 'item_id',

    function($scope, $modalInstance, ListService, item_id) {

        $scope.ok = function() {

            ListService.createList($scope.name, $scope.description).then(function() {
                $scope.toaster.success('Success', 'You have create a new list!');

                ListService.getListsByItem(item_id).then(function(response) {
                    $modalInstance.close(response.data);
                });

            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);


ItemModule.controller('AddShopShortController', ['$scope', '$modalInstance', 'ShopService',
    function($scope, $modalInstance, ShopService) {


        $scope.ok = function() {
        	console.log($scope.name);

        	ShopService.createShop({ shop_name: $scope.name, adress: $scope.address }).then(function(response) {
	           console.log(response.data);
                $modalInstance.close();
        	}, function(response) {
        		console.log("Error");
                console.log(response.data);
        	});

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);
ShopModule.controller('CreateShopController', [
	'$rootScope', 
	'$scope', 
	'$location',
	'ShopService',

	function ($rootScope, $scope, $location, ShopService) {

/*
			$scope.email = "ppp@gmail.com";
			$scope.password = "hejhej123";
			$scope.shop_name = "Da Shop";
			$scope.cvr = "22995848";
			$scope.website = "http://www.shop.dk/";
			$scope.contact_person = "Patrick Reck";
			$scope.phone = "29828409";
			$scope.adress = "Vejledalen 22 2635";
			$scope.description = "Da one and only shop";
*/

		$scope.createShop = function() {
			
			var postData = {
				email: $scope.email,
				password: $scope.password,
				shop_name: $scope.shop_name,
				cvr: $scope.cvr,
				website: $scope.website,
				contact_person: $scope.contact_person,
				phone: $scope.phone,
				adress: $scope.adress,
				description: $scope.description
			}

			ShopService.createShop(postData).success(function(data) {
				console.log("Success");
				console.log(data);
				$scope.errors = null;
				
			}).error(function(errors) {
				console.log("Errors");
				console.log(errors);
				$scope.errors = errors;
			});

		}




	}
]);
ShopModule.controller('ShopController', ['$rootScope', '$scope', 'shop', '$state', '$ionicModal', '$timeout',
	
	function($rootScope, $scope, shop, $state, $ionicModal, $timeout) {

		$scope.$emit('changeBackground', shop.cover);

		$scope.shop = shop;

		$scope.info = false;
		$scope.toggleInfo = function() {
			$scope.info = !$scope.info;
		}

		$scope.currentState = $state.current.name;
		$rootScope.$on('$stateChangeSuccess', function() {
			$scope.currentState = $state.current.name;
			
			$scope.info = false;
			if ($scope.currentState == 'app.shop.search') {
				$scope.info = true;
			}

		});



		$scope.expanded = true;
		$scope.expand = function() {

			if ($scope.expanded) {
				$scope.info = true;
	
				$timeout(function() {

					$('.profile .top').css({
						height: window.innerHeight - 115,
						marginTop: -window.innerHeight + 115
					});
					$('.profile .buttons').css({
						marginTop: window.innerHeight - 115
					});

				});

			}
			else {
				$scope.info = false;

				$('.profile .top').css({
					height: "300px",
					marginTop: "0px"
				});
				$('.profile .buttons').css({
					marginTop: "0px"
				});
			}

			$timeout(function() {

				// Fire resize event
			}, 10);

			
			$scope.expanded = !$scope.expanded;

		}


		/* Maps */

		$scope.maps = false;

		$scope.toggleMaps = function() {
			$scope.maps = !$scope.maps;
		}




    	$scope.map;
	    $scope.$on('mapInitialized', function(evt, evtMap) {
	      $scope.map = evtMap;
			$scope.map.setCenter(new google.maps.LatLng(shop.lat, shop.lng));

	    });


	}

])

.controller('ShopSearchController', ['$scope', 'items',
	function($scope, items) {
		$scope.items = items;

	}
]);
ShopModule.service('ShopService', ['$rootScope', '$http', 'LocationService',

	function($rootScope, $http, LocationService) {

		this.getShops = function() {
			return $http.get($rootScope.api + '/shops');
		}

		this.getShop = function(shopId) {
			return $http.get($rootScope.api + '/shops/shop/' + shopId);
		}

		this.createShop = function(postData) {
			return $http.post($rootScope.root + '/shops/create-shop', postData);
		}

		this.attachItems = function(postData) {
			return $http.post($rootScope.root + '/shops/attach-items', postData);
		}

		this.getShopsWithItemsWithin = function(item_ids, max_distance) {
			return LocationService.updateMyPosition().then(function(position) {
				
				var url = $rootScope.api + '/shops/shops-with-items-within/' + item_ids + '/' + position.lat + '-' + position.lng + '/' + max_distance;
								
				return $http.get(url);

			});

	
		}


	}

]);
ShopModule.controller('ShopsController', ['$scope', 'shops', 'LocationService',

	function($scope, shops, LocationService) {

		$scope.$emit('changeBackground', 'default');
		
		$scope.shops = shops;

		console.log(shops);

		LocationService.updateMyPosition().then(function() {
			angular.forEach($scope.shops, function(shop) {
				shop.distance = LocationService.distance(shop.lat, shop.lng);
			});
		});

	}

]);
ShopModule.filter('metricdistance', function() {
	return function(input) {

		if (input < 1) {
			input *= 1000;
			input = Math.round(input) + " meters";
		}
		else {
			input = Math.round(input * 10) / 10;
			input = input + " km";
		}

		return input;
	};
})

CloqetApp.filter('format_price', function() {
	return function(input) {

		if (input % 1 == 0)
			return parseInt(input);

		return input;

	};
});
UserModule.controller('UserController', ['$scope', 'user', '$modal',
	
	function($scope, user, $modal) {

		$scope.$emit('changeBackground', 'default');

        $scope.breadcrumb = "";

        $scope.$on('$stateChangeStart', function() {
            $scope.breadcrumb = "";
        });


		$scope.user = user.user;
		$scope.likes = user.likes;



        $scope.sendMessage = function() {

            var modalInstance = $modal.open({
                templateUrl: 'js/modules/conversations/partials/modal-send-message.html',
                controller: 'ModalStartConversationController',
                size: 'lg',
                resolve: {
                	userdata: function() {
                		return { id: $scope.user.id, name: $scope.user.nickname };
                	}
                }
            });

            modalInstance.result.then(function() {
            }, function() {
            });
        };

		
	}
]);
UserModule.service('UserService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getUsers = function() {
			return $http.get($rootScope.api + '/users');
		}

		this.getUser = function(userId) {
			return $http.get($rootScope.api + '/users/user/' + userId);
		}
	}

]);
BrandModule.directive('brandprofile', ['$rootScope', function ($rootScope) {
	return {
		restrict: 'E',
		scope: {
			id: '=',
			brandname: '='
		},

		transclude: true,

		template: '<a ui-sref="brand.home({ brandId: id, brandName: slugifiedBrandname })"><span ng-if="!hasOtherContent"><b>{{ brandname }}</b></span><span ng-transclude></span></a>',
		
		link: function (scope, iElement, iAttrs) {
			scope.slugifiedBrandname = $rootScope.getSlugified(scope.brandname);

			scope.hasOtherContent = iElement.find('span').contents().length > 0;

		}
	};
}])
ItemModule.directive('smallcloqetitem', ['$rootScope', function ($rootScope) {
	return {
		restrict: 'E',
		scope: {
			item: '=',
			disabled: '=',
		},

		transclude: true,

		templateUrl: 'js/modules/items/directives/templates/small-item.html',
		
		link: function ($scope, iElement, iAttrs) {
			$scope.api = $rootScope.api;

			$scope.item.active = false;
			$scope.toggle = function() {
				if ($scope.disabled)
					return;

				$scope.item.active = !$scope.item.active;
			}

			$scope.goToItem = function() {
				$state.go('app.item', {itemId: item.id});
			}
		}
	};
}])
ItemModule.directive('cloqetitem', ['$rootScope', '$state', function($rootScope, $state) {

	return {
		scope: {
			item: '=',
			disabled: '=',
		},
		restrict: 'E',
		templateUrl: 'js/modules/items/directives/templates/item.html',

		link: function($scope, iElm, iAttrs, controller) {
			$scope.api = $rootScope.api;

			$scope.item.active = false;
			$scope.toggle = function() {
				if ($scope.disabled)
					return;

				$scope.item.active = !$scope.item.active;
			}

			$scope.goToItem = function() {
				$state.go('app.item', {itemId: item.id});
			}
		}
	};
}]);
ItemModule.directive('tickablea', ['$rootScope', function ($rootScope) {
	return {
		restrict: 'A',

		template: 'js/modules/items<div ng-transclude></div>/directives/templates/item.html',
		
		link: function (iElement, iAttrs) {
		}
	};
}])
ShopModule.directive('shop', ['$rootScope', function ($rootScope) {
	return {
		restrict: 'E',
		scope: {
			shop: '='
		},

		transclude: true,

		templateUrl: 'js/modules/shops/directives/templates/shop.html',
		
		link: function (scope, iElement, iAttrs) {
		}
	};
}])
ShopModule.directive('shopprofile', ['$rootScope', function ($rootScope) {
	return {
		restrict: 'E',
		scope: {
			id: '=',
			shopname: '='
		},

		transclude: true,

		template: '<a ui-sref="store({ storeId: id, storeName: slugifiedStorename })"><b>{{ shopname }}</b><span ng-transclude></span></a>',
		
		link: function (scope, iElement, iAttrs) {

		}
	};
}])
UserModule.directive('userprofile', ['$rootScope', function ($rootScope) {
	return {
		restrict: 'E',
		scope: {
			id: '=',
			username: '='
		},

		transclude: true,

		template: '<a ui-sref="user-profile({ userId: id, userName: slugifiedUsername })"><span ng-if="!hasOtherContent"><b>{{ username }}</b></span><span ng-transclude></span></a>',
		
		link: function (scope, iElement, iAttrs) {
			scope.slugifiedUsername = $rootScope.getSlugified(scope.username);

			scope.hasOtherContent = iElement.find('span').contents().length > 0;

		}
	};
}])