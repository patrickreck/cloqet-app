ConversationModule.controller('ConversationController', ['$scope', 'ConversationService', 'conversation', '$modal',

	function($scope, ConversationService, conversation, $modal) {

		$scope.$emit('changeBackground', 'default');

		$scope.conversation = conversation;

		$scope.refreshing = false;

		$scope.reply = function() {

            var modalInstance = $modal.open({
                templateUrl: 'js/modules/conversations/partials/reply-message.html',
                controller: 'ModalReplyController',
                size: 'lg',
                resolve: {
                	conversation: function() {
                		return $scope.conversation;
                	}
                }
            });

            modalInstance.result.then(function() {
            	$scope.refresh();
            }, function() {
            	console.log("Cancelled");
            });

		}

		$scope.refresh = function() {
			$scope.refreshing = true;
			ConversationService.getConversation(conversation.id).then(function(response) {
				$scope.conversation = response.data;
				$scope.refreshing = false;
			});
		}
        
	}

]);