ConversationModule.controller('ConversationsController', ['$scope', 'conversations', 'ConversationService', '$state',

	function($scope, conversations, ConversationService, $state) {

		$scope.$emit('changeBackground', 'default');

		$scope.conversations = conversations;

        $scope.$on('loggedOut', function() {
            $scope.goToFrontpage();
        });

		$scope.deleteConversation = function(conversation) {

		}

		$scope.refreshing = false;
		$scope.refresh = function() {
			$scope.refreshing = true;

			ConversationService.getConversations().then(function(response) {
				$scope.conversations = response.data;
				$scope.refreshing = false;
			});
		}

	}

]);