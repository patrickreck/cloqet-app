ShopModule.controller('ShopsController', ['$scope', 'shops', 'LocationService',

	function($scope, shops, LocationService) {

		$scope.$emit('changeBackground', 'default');
		
		$scope.shops = shops;

		console.log(shops);

		LocationService.updateMyPosition().then(function() {
			angular.forEach($scope.shops, function(shop) {
				shop.distance = LocationService.distance(shop.lat, shop.lng);
			});
		});

	}

]);