ShopModule.service('ShopService', ['$rootScope', '$http', 'LocationService',

	function($rootScope, $http, LocationService) {

		this.getShops = function() {
			return $http.get($rootScope.api + '/shops');
		}

		this.getShop = function(shopId) {
			return $http.get($rootScope.api + '/shops/shop/' + shopId);
		}

		this.createShop = function(postData) {
			return $http.post($rootScope.root + '/shops/create-shop', postData);
		}

		this.attachItems = function(postData) {
			return $http.post($rootScope.root + '/shops/attach-items', postData);
		}

		this.getShopsWithItemsWithin = function(item_ids, max_distance) {
			return LocationService.updateMyPosition().then(function(position) {
				
				var url = $rootScope.api + '/shops/shops-with-items-within/' + item_ids + '/' + position.lat + '-' + position.lng + '/' + max_distance;
								
				return $http.get(url);

			});

	
		}


	}

]);