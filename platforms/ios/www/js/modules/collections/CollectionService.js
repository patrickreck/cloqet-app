CollectionModule.service('CollectionService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getCollectionsByBrand = function(brandId) {
			return $http.get($rootScope.api + '/collections/collections-by-brand/' + brandId);
		}

		this.getCollection = function(collectionId) {
			return $http.get($rootScope.api + '/collections/collection/' + collectionId);
		}

		this.saveCollection = function(name, description, items) {
			var data = {
				name: name,
				description: description,
				items: items
			};

			return $http.post($rootScope.api + '/collections/save-collection', data);
		}
	}

]);