ItemModule.directive('cloqetitem', ['$rootScope', '$state', function($rootScope, $state) {

	return {
		scope: {
			item: '=',
			disabled: '=',
		},
		restrict: 'E',
		templateUrl: 'js/modules/items/directives/templates/item.html',

		link: function($scope, iElm, iAttrs, controller) {
			$scope.api = $rootScope.api;

			$scope.item.active = false;
			$scope.toggle = function() {
				if ($scope.disabled)
					return;

				$scope.item.active = !$scope.item.active;
			}

			$scope.goToItem = function() {
				$state.go('app.item', {itemId: item.id});
			}
		}
	};
}]);