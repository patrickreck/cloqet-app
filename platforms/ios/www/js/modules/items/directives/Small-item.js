ItemModule.directive('smallcloqetitem', ['$rootScope', function ($rootScope) {
	return {
		restrict: 'E',
		scope: {
			item: '=',
			disabled: '=',
		},

		transclude: true,

		templateUrl: 'js/modules/items/directives/templates/small-item.html',
		
		link: function ($scope, iElement, iAttrs) {
			$scope.api = $rootScope.api;

			$scope.item.active = false;
			$scope.toggle = function() {
				if ($scope.disabled)
					return;

				$scope.item.active = !$scope.item.active;
			}

			$scope.goToItem = function() {
				$state.go('app.item', {itemId: item.id});
			}
		}
	};
}])