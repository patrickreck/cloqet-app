ItemModule.controller('CreateItemController', [
    '$rootScope',
    '$http',
    '$scope',
    '$location',
    'ItemService',
    '$upload',
    '$modal',
    '$parse',
    '$state',

    function($rootScope, $http, $scope, $location, ItemService, $upload, $modal, $parse, $state) {



        $scope.step = 1;

        $scope.setStep = function(step) {
            $scope.step = step;
            $scope.errorBox.active = false;
        }

        $scope.next = function() {

            $scope.errorBox.active = false;

            if ($scope.step == 1) {

                $scope.resetErrors();

                if (!$scope.name ||
                    !$scope.gender ||
                    !$scope.description ||
                    $scope.prices.length == 0 ||
                    !$scope.selectedCategory
                    ) {

                    if (!$scope.name) {
                        $scope.name_error = true;
                    }

                    if (!$scope.description) {
                        $scope.description_error = true;
                    }

                    if ($scope.prices.length == 0) {
                        $scope.price_error = true;
                    }

                    $scope.errorBox.active = true;
                    $scope.errorBox.text = "Please fill out the missing data";

                    return;

                }
            }

            if ($scope.step == 2) {

                if (!$scope.havePicture()) {
                    $scope.errorBox.text = "You need to add at least 1 picture";
                    $scope.errorBox.active = true;
                    return false;
                }

            }


            if ($scope.step == 3) {
                $scope.createItem();
                return;
            }

            $scope.errorBox.active = false;

            $scope.step += 1;
        }
        
        $scope.back = function() {
            $scope.errorBox.active = false;

            if ($scope.step == 1)
                return;

            $scope.step -= 1;
        }



        $scope.errorBox = { active: false, text: "Some data is missing from the form" };
        $scope.resetErrors = function() {
            $scope.name_error = false;
            $scope.description_error = false;
            $scope.price_error = false;
        }


        $scope.createItem = function() {
            var postData = {
                name: $scope.name,
                description: $scope.description,
                product_code: $scope.item_number,
                size_fit: $scope.size_fit,
                gender: $scope.gender,
                colors: $scope.chosenColors,
                countries: $scope.prices,
                //construction: $scope.selectedConstruction,
                //webshop_url: $scope.webshop_url,
                categoryId: $scope.selectedCategory.id,
                fabrics: $scope.addedFabrics
            };

            console.log("Submitting to server");
            return ItemService.createItem(postData).then(function(response) {
                
                console.log('Promise recieved');
                console.log(response.data);

                $scope.toaster.success('Congratulations', 'You just created an item!');

                var item = response.data;
                $state.go('item.add-to-shops', {'itemId': item.id, 'itemName': item.slug});


//                $location.path('/items/' + response.data['id'] + '/' + response.data['slug'] + '/show');

            });
        }


        $scope.gender = 1;


        /* Categories */

        $scope.selectedCategory = null;

        $scope.category1_categories = [];
        $scope.category2_categories = [];
        $scope.category3_categories = [];
        $scope.category4_categories = [];

        ItemService.getCategoriesByParent('').then(function(response) {
            $scope.category1_categories = response.data;
            $scope.category1 = response.data[0];
            $scope.categoryChanged($scope.category1);
        });
  
        $scope.categoryChanged = function(category) {

            if (typeof category != 'undefined') {
                ItemService.getCategoriesByParent(category['id']).then(function(response) {

                    if (category === $scope.category1) {
                        $scope.category2_categories = null;
                        $scope.category3_categories = null;
                        $scope.category4_categories = null;                        

                        $scope.category2_categories = response.data;
                        $scope.category2 = response.data[0];

                        if (typeof $scope.category2 != 'undefined')
                            $scope.selectedCategory = $scope.category2;
                        else
                            $scope.selectedCategory = $scope.category1;

                        $scope.categoryChanged($scope.category2);
                    }

                    else if (category === $scope.category2) {
                        $scope.category3_categories = null;
                        $scope.category4_categories = null;

                        $scope.category3_categories = response.data;
                        $scope.category3 = response.data[0];

                        if (typeof $scope.category3 != 'undefined')
                            $scope.selectedCategory = $scope.category3;
                        else
                            $scope.selectedCategory = $scope.category2;

                        $scope.categoryChanged($scope.category3);

                    }                      

                    else if (category === $scope.category3) {
                        $scope.category4_categories = null;

                        $scope.category4_categories = response.data;
                        $scope.category4 = response.data[0];

                        if (typeof $scope.category4 != 'undefined')
                            $scope.selectedCategory = $scope.category4;
                        else
                            $scope.selectedCategory = $scope.category3;

                    }


                });    
            }     
        }


        /* Currencies */

        $scope.countries = [];

        ItemService.getCountries().then(function(response) {
            $scope.countries = response.data;
            $scope.selectedCurrency = $scope.countries[0];
        });

        $scope.prices = [];

        $scope.addPrice = function() {

            if (isNaN($scope.price) || $scope.price == "")
                return;

            $scope.prices.push({"currency" : $scope.selectedCurrency, "price" : $scope.price})
            $scope.countries.splice($scope.countries.indexOf($scope.selectedCurrency), 1);
            $scope.selectedCurrency = $scope.countries[0];

            $scope.price = "";
        }

        $scope.removePrice = function(price) {
            $scope.countries.push(price.currency);
            $scope.prices.splice($scope.prices.indexOf(price), 1);
            $scope.selectedCurrency = $scope.countries[0];
        }




        /* Materials */

        $scope.addNewMaterialTab = {
            isOpen: true,
        }

        $scope.materials = [];
        $scope.addedFabrics = [];


        var getNewTemporaryFabric = function() {
            return {
                name: '',
                contents: [],
                temporaryMaterial: {
                    material: $scope.materials[0],
                    percentage: 0
                }
            };
        }

        $scope.temporaryFabric = getNewTemporaryFabric();

        $scope.addFabric = function() {

            if (typeof($scope.temporaryFabric.name) == 'undefined') {
                $scope.errorBox.text = "You need to enter the fabrics name."
                $scope.errorBox.active = true;
                return;
            }

            if (!$scope.addsToHundred()) {
                $scope.errorBox.text = "The material contents needs to add up to 100%."
                $scope.errorBox.active = true;
                return;
            }

            delete $scope.temporaryFabric.temporaryMaterial;

            $scope.addedFabrics.push($scope.temporaryFabric);


            angular.forEach($scope.materials, function(material, key) {
                material.used = false;
            });

            $scope.temporaryFabric = getNewTemporaryFabric();

            $scope.errorBox.active = false;
        }

        $scope.addMaterial = function() {

            console.log($scope.temporaryFabric.temporaryMaterial.percentage);

            if ($scope.temporaryFabric.temporaryMaterial.percentage <= 0) {
                return;
            }

            $scope.temporaryFabric.contents.push($scope.temporaryFabric.temporaryMaterial);
            
            $scope.temporaryFabric.temporaryMaterial.material.used = true;

            // Set the new selected material to the next NOT used
            var nextMaterial = function() {
                var newMaterial = "";
                angular.forEach($scope.materials.reverse(), function(value, key) {

                    if (value.used != true) {
                        newMaterial = value;
                    }

                });

                $scope.materials.reverse();
                return newMaterial;
            };

            delete $scope.temporaryFabric.temporaryMaterial;

            $scope.temporaryFabric.temporaryMaterial = {
                material: nextMaterial(),
                percentage: 0
            }
        }

        $scope.removeMaterial = function(material) {
            material.used = false;
            $scope.temporaryFabric.contents.splice($scope.temporaryFabric.contents.indexOf(material), 1);
        }

        $scope.addsToHundred = function() {
            var percentage = 0;
            angular.forEach($scope.temporaryFabric.contents, function(material, key) {
                percentage += material.percentage;
            });

            if (percentage != 100)
                return false;

            return true;
        }

        ItemService.getMaterials().then(function(response) {
            $scope.materials = response.data;
            $scope.temporaryFabric.temporaryMaterial.material = $scope.materials[0];
        });

    




        /* Template */

        $scope.colors = {};
        ItemService.getColors().then(function(response) {
            $scope.colors = response.data;
        });

        $scope.chosenColors = [];

        $scope.addColor = function(color) {

            var duplicate = false;

            angular.forEach($scope.chosenColors, function(value, key) {

                if (value.color == color.color) {
                    duplicate = true;
                }
            })

            if (!duplicate) {
                $scope.chosenColors.push(color);
                $scope.chosenColors[$scope.chosenColors.length - 1].images = [];                
            }
        }


        $scope.removeColor = function(color) {
            $scope.chosenColors.splice($scope.chosenColors.indexOf(color), 1);
            $scope.selectedColor = [];
        }


        $scope.selectedColor = [];

        $scope.selectColor = function(color) {
            $scope.selectedColor = color;
        }

        $scope.previewImage = {};
        $scope.updatePreviewImage = function(image) {
            
            $scope.previewImage = image;
        }

        $scope.clearPreviewImage = function() {
            $scope.previewImage = {};
        }


        $scope.havePicture = function() {
            var havePicture = false;
            angular.forEach($scope.chosenColors, function(value, key) {

                if (value.images.length != 0) {
                    havePicture = true;
                }
            });

            return havePicture;
        }



        $scope.openAddPicture = function(thisColor) {

            var modalInstance = $modal.open({
                templateUrl: 'addPictureModal.html',
                controller: 'addPictureModal',
                size: 'lg',
                resolve: {
                    color: function() {
                        return thisColor;
                    }
                }
            });

            modalInstance.result.then(function(pictures) {

            }, function() {
                console.log('Modal dismissed at: ' + new Date());
            });
        };





    }
]);

ItemModule.controller('addPictureModal', ['$scope', '$modalInstance', 'color', '$upload',
    function($scope, $modalInstance, color, $upload) {

        $scope.color = color;
        console.log(color);


        /* File upload */

        $scope.onFileSelect = function($files) {

            for (var i = 0; i < $files.length; i++) {

                var file = $files[i];

                $scope.upload = $upload.upload({

                    url: $scope.root +'/products/image',
                    data: {
                        color: $scope.selectedColor
                    },
                    file: file,

                }).progress(function(evt) {
                    console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                }).success(function(data, status, headers, config) {

                    $scope.color.images.push(data);

                });
            }
        };


        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);

