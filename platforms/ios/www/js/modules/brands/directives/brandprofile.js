BrandModule.directive('brandprofile', ['$rootScope', function ($rootScope) {
	return {
		restrict: 'E',
		scope: {
			id: '=',
			brandname: '='
		},

		transclude: true,

		template: '<a ui-sref="brand.home({ brandId: id, brandName: slugifiedBrandname })"><span ng-if="!hasOtherContent"><b>{{ brandname }}</b></span><span ng-transclude></span></a>',
		
		link: function (scope, iElement, iAttrs) {
			scope.slugifiedBrandname = $rootScope.getSlugified(scope.brandname);

			scope.hasOtherContent = iElement.find('span').contents().length > 0;

		}
	};
}])