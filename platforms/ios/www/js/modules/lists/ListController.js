ListModule.controller('ListController', ['$scope', 'list', '$sce', '$interval',
	
	function($scope, list, $sce, $interval) {

		$scope.$parent.breadcrumb = "Lists / " + list.name;

		$scope.list = list;
	
		$scope.list.description = $sce.trustAsHtml(list.description);

		$scope.item_ids = "";

		var index = 0;
		angular.forEach($scope.list.items, function(item) {
			if (index + 1 == $scope.list.items.length) {
				$scope.item_ids = $scope.item_ids + item.id;
			}
			else {
				$scope.item_ids = $scope.item_ids + item.id + '-';
			}

			index++;
		});

		$scope.testy = function() {

			$scope.class = 'bouncy';
			$interval(function() {
				console.log("woop");
				$scope.class = null;
			}, 1000, 1);
		}

		
	}

]);