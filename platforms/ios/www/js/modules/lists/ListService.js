ListModule.service('ListService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getLists = function() {
			return $http.get($rootScope.api + '/lists');
		}

		this.getListsByItem = function(item_id) {
			return $http.get($rootScope.api + '/lists/lists-by-item/' + item_id);
		}

		this.getListsByUser = function(user_id) {
			return $http.get($rootScope.api + '/lists/lists-by-user/' + user_id);
		}

		this.getList = function(list_id) {
			return $http.get($rootScope.api + '/lists/list/' + list_id);
		}

		this.createList = function(name, description) {
			return $http.post($rootScope.api + '/lists/create', {name: name, description: description});
		}

		this.toggleItem = function(item_id, list_id) {
			return $http.post($rootScope.api + '/lists/toggle-item', {item_id: item_id, list_id: list_id});
		}

	}

]);