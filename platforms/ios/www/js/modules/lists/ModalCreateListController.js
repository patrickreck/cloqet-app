ListModule.controller('ModalCreateListController', ['$scope', '$modalInstance', 'ListService', 'item_id',

    function($scope, $modalInstance, ListService, item_id) {

        $scope.ok = function() {

            ListService.createList($scope.name, $scope.description).then(function() {
                $scope.toaster.success('Success', 'You have create a new list!');

                ListService.getListsByItem(item_id).then(function(response) {
                    $modalInstance.close(response.data);
                });

            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);

