var CloqetApp = angular.module('CloqetApp', [
	'ionic', 
	'ngAnimate', 
	'ItemModule', 
	'CollectionModule',
	'BrandModule',
	'AuthenticationModule',
	'ListModule',
	'ConversationModule',
	'ShopModule', 
	'truncate',
	'http-auth-interceptor',
	'ngCordova',
	'angularMoment',
	'ngMap',
	'angular-data.DSCacheFactory',
])

.run(function($ionicPlatform, $rootScope, $ionicSideMenuDelegate, AuthenticationService, $cordovaStatusbar, $ionicLoading, $cordovaSplashscreen, DSCacheFactory, $http, $interval) {
	
//		$rootScope.clientRoot = 'http://192.168.1.47/cloqet_app/www/';
		$rootScope.root = 'http://cloqet-server.herokuapp.com/public';
		$rootScope.api = 'http://cloqet-server.herokuapp.com/public';

		$rootScope.root = 'http://192.168.1.61/cloqet-server/public';
		$rootScope.api = 'http://192.168.1.61/cloqet-server/public';

		// Cache
		DSCacheFactory('defaultCache', {
			maxAge: 300000,
			cacheFlushInterval: 6000000,
			deleteOnExpire: 'aggressive'
		});

		$http.defaults.cache = DSCacheFactory.get('defaultCache');


		// Login specifics
		$rootScope.isLoggedIn = function() {
			return AuthenticationService.isLoggedIn();
		}

		$rootScope.getBrandId = function() {
			return AuthenticationService.getBrandId();
		}

		$rootScope.getShopId = function() {
			return AuthenticationService.getBrandId();
		}

		$rootScope.getUserId = function() {
			return AuthenticationService.getUserId();
		}

		$rootScope.getUserPicture = function() {
			return AuthenticationService.getUserPicture();
		}


		$rootScope.$on('$stateChangeStart', function() {
			$ionicLoading.show({
				template: '<i class="icon ion-loading-c loadinga"></i>',
				animation: 'fade-in',
				showBackdrop: true,
				showDelay: 300
			});
		})

		$rootScope.$on('$stateChangeSuccess', function() {
			$ionicLoading.hide()
		})





	$ionicPlatform.ready(function() {

		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if(window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

	        $interval(function() {
		        $cordovaSplashscreen.hide();
	        }, 2000, 1)

		}
		if(window.StatusBar) {
			StatusBar.styleDefault();

			$cordovaStatusbar.style(2);
		}


	});




})



.config(function($httpProvider) {
	$httpProvider.interceptors.push(function($rootScope) {
		return {
			request: function(config) {
				//$rootScope.$broadcast('loading:show')
				return config
			},
			response: function(response) {
				//$rootScope.$broadcast('loading:hide')
				return response
			}
		}
	})
})













