ShopModule.controller('CreateShopController', [
	'$rootScope', 
	'$scope', 
	'$location',
	'ShopService',

	function ($rootScope, $scope, $location, ShopService) {

/*
			$scope.email = "ppp@gmail.com";
			$scope.password = "hejhej123";
			$scope.shop_name = "Da Shop";
			$scope.cvr = "22995848";
			$scope.website = "http://www.shop.dk/";
			$scope.contact_person = "Patrick Reck";
			$scope.phone = "29828409";
			$scope.adress = "Vejledalen 22 2635";
			$scope.description = "Da one and only shop";
*/

		$scope.createShop = function() {
			
			var postData = {
				email: $scope.email,
				password: $scope.password,
				shop_name: $scope.shop_name,
				cvr: $scope.cvr,
				website: $scope.website,
				contact_person: $scope.contact_person,
				phone: $scope.phone,
				adress: $scope.adress,
				description: $scope.description
			}

			ShopService.createShop(postData).success(function(data) {
				console.log("Success");
				console.log(data);
				$scope.errors = null;
				
			}).error(function(errors) {
				console.log("Errors");
				console.log(errors);
				$scope.errors = errors;
			});

		}




	}
]);