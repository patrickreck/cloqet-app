ShopModule.filter('metricdistance', function() {
	return function(input) {

		if (input < 1) {
			input *= 1000;
			input = Math.round(input) + " meters";
		}
		else {
			input = Math.round(input * 10) / 10;
			input = input + " km";
		}

		return input;
	};
})

CloqetApp.filter('format_price', function() {
	return function(input) {

		if (input % 1 == 0)
			return parseInt(input);

		return input;

	};
});