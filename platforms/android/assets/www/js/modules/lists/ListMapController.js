ListModule.controller('ListMapController', ['$scope', 'shops', 'LocationService', '$stateParams', '$ionicModal',
	function ($scope, shops, LocationService, $stateParams, $ionicModal) {

		$scope.showShop = function(evt, id) {
			$scope.shop = $scope.shops[id];
			console.log($scope.shop);
			console.debug($scope.shop);

			angular.element(document.getElementById('foo')).scope().shop = $scope.shop;
			angular.element(document.getElementById('foo')).scope().showInfoWindow(evt, 'foo', this);
		};

		$scope.title = $stateParams.title;

    	$scope.map;
	    $scope.$on('mapInitialized', function(evt, evtMap) {
			$scope.map = evtMap;
	    });

	    $scope.shops = [];
	    $scope.items = [];

	    // Format and populate items
	    angular.forEach(shops, function(shop) {
	    	shop.position = [shop.lat, shop.lng];

	    	angular.forEach(shop.items, function(item) {
	    		item.images = [{path: item.image}];

	    		var alreadyExists = false;
	    		angular.forEach($scope.items, function(uniqueItem) {
	    			if (uniqueItem.id == item.id)
	    				alreadyExists = true;
	    		})
	    		if (!alreadyExists)
		    		$scope.items.push(item);
	    	});

	    	$scope.shops.push(shop);
	    });

	    $scope.tickedItems = function() {
	    	var tickedItems = false;

	    	angular.forEach($scope.items, function(item) {
	    		if (item.ticked)
	    			tickedItems = true;
	    	})

	    	return tickedItems;
	    }

	    $scope.tickItem = function(item) {
	    	item.ticked = !item.ticked;
	    	$scope.sortMarkers();
	    }

	    $scope.showFilters = true;
	    $scope.toggleFilters = function() {
	    	$scope.showFilters = !$scope.showFilters;
	    }

	    $scope.sortMarkers = function() {

	    	var shopFound = false;
	    	angular.forEach($scope.shops, function(shop) {

	    		var itemFound = false;

	    		angular.forEach(shop.items, function(shopItem) {

	    			angular.forEach($scope.items, function(item) {
	    				if (shopItem.id == item.id && item.ticked) {
	    					itemFound = true;
	    					shopFound = true;
	    				}
	    			})
	    		});

	    		if (itemFound)
	    			$scope.map.markers[shop.id].setMap($scope.map);
	    		else
	    			$scope.map.markers[shop.id].setMap(null);

	    	});

	    	if (!shopFound) {
	    		angular.forEach($scope.shops, function(shop) {
	    			$scope.map.markers[shop.id].setMap($scope.map);
	    		});
	    	}

	    }

		$scope.haveItemsFilter = function(shop) {
		    //console.log(shop);
		    
		    var isOk = false;
		    angular.forEach($scope.items, function(item) {
		        angular.forEach(shop.items, function(shopItem) {
		            if (shopItem.id == item.id && item.ticked) {
		                //console.log('Oh, got ya!');
		                //console.log(shopItem.id + ' is the same as ' + item.id + ' and ticked is ' + item.ticked);
		                isOk = true;
		            }
		        });
		    });
		    return isOk;
		};


		$scope.a = false;
		$scope.test = function() {
			$scope.a = !$scope.a;
			if ($scope.a)
				$scope.map.markers[2].setMap(null);
			else 
				$scope.map.markers[2].setMap($scope.map);				
		}


	    // Update position
		LocationService.updateMyPosition().then(function(position) {
			console.log(position);
			$scope.myPosition = [position.lat,position.lng];
			$scope.map.setCenter(new google.maps.LatLng(position.lat, position.lng));
		});


	}
]);
