CollectionModule.controller('CollectionController', ['$scope', 'collection', '$interval', '$sce',
	
	function($scope, collection, $interval, $sce) {

		$scope.collection = collection;  

		$scope.collection.description = $sce.trustAsHtml($scope.collection.description); 

		$scope.images = [
			'http://cdn.shopify.com/s/files/1/0226/9103/files/ss14-3_grande.jpg?1822',
			'http://localhost/cloqet_server/public/cover-images/savannahwild.jpg',
			'http://localhost/cloqet_server/public/cover-images/hugoboss.jpg',
			'http://localhost/cloqet_server/public/cover-images/gstar.jpg',
		];

		$scope.slideIndex = 0;

		$scope.nextIndex = function() {
			$scope.slideIndex++;
		}

		$scope.prevIndex = function() {
			$scope.slideIndex--;
		}

		var paused = false;

		$scope.changeSlide = function(){
			if (paused) return;
			if ($scope.slideIndex == $scope.images.length - 1) {
				$scope.slideIndex = 0;
			}
			else {
				$scope.slideIndex++;
			}
		};

		$interval($scope.changeSlide, 6000);

		$scope.pause = function() {
			paused = true;
			console.log("Pause");
		}



	}

]);