ConversationModule.service('ConversationService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getConversations = function() {
			return $http.get($rootScope.api + '/conversations');
		}

		this.getConversation = function(conversationId) {
			return $http.get($rootScope.api + '/conversations/conversation/' + conversationId);
		}

		this.startConversation = function(to, subject, message) {
			console.log(to);
			return $http.post($rootScope.api + '/conversations/start-conversation', { to: to, subject: subject, message: message });
		}

		this.sendMessage = function(conversation, message) {
			return $http.post($rootScope.api + '/conversations/message', { conversation: conversation, message: message });
		}

		this.deleteConversation = function(conversation_id) {
			return $http.post($rootScope.api + '/conversations/delete', { conversation_id: conversation_id });
		}

	}

]);