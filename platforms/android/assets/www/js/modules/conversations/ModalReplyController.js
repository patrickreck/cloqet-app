ConversationModule.controller('ModalReplyController', ['$scope', '$modalInstance', 'ConversationService', 'conversation',

    function($scope, $modalInstance, ConversationService, conversation) {

        $scope.message = "";
        $scope.conversation = conversation;

        $scope.ok = function() {
            
            ConversationService.sendMessage(conversation.id, $scope.message).then(function(response) {
                console.log(response.data);
                $scope.toaster.success('Success', 'Your message was sent to ' + conversation.other.name);

                $modalInstance.close();

            }, function(response) {
                
                console.log(response.data);
                $scope.toaster.error('Error', 'Your message was not delivered');

            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);

