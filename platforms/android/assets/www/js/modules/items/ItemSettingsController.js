ItemModule.controller('ItemSettingsController', ['$scope', 'ItemService', 'ShopService', '$state', '$sce',
    function($scope, ItemService, ShopService, $state, $sce) {

        $scope.tabs = [
            { title: 'General', route: 'general' },
            { title: 'Publishing' },
            { title: 'Shop links <span class="badge">3</span>', route: 'add-to-shops' },
            { title: 'Materials' },
            { title: 'Media links', route: 'media' },
            { title: 'Pricing', route: 'pricing' },
        ];

        angular.forEach($scope.tabs, function(tab) {
            tab.title = $sce.trustAsHtml(tab.title);
        });

        $scope.test = "<b>hej</b>";
        $scope.testa = $sce.trustAsHtml($scope.test);

    }
]);
