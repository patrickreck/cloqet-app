ItemModule.directive('tickablea', ['$rootScope', function ($rootScope) {
	return {
		restrict: 'A',

		template: 'js/modules/items<div ng-transclude></div>/directives/templates/item.html',
		
		link: function (iElement, iAttrs) {
		}
	};
}])