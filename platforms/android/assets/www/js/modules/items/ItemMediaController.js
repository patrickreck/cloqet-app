ItemModule.controller('ItemMediaController', ['$scope', 'ItemService',
    function($scope, ItemService) {

        $scope.title = false;
        $scope.images = [];
        $scope.currentImage = false;
        $scope.loading = false;



        $scope.nextImage = function() {
            if ($scope.currentImage != $scope.images.length - 1) {
                $scope.currentImage++;
            }
        }
        $scope.previousImage = function() {
            if ($scope.currentImage != 1) {
                $scope.currentImage--;
            }
        }

        $scope.updatedUrl = function() {

            var urlRegex = new RegExp('^(https?:\\/\\/)?((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|((\\d{1,3}\\.){3}\\d{1,3}))(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*(\\?[;&a-z\\d%_.~+=-]*)?(\\#[-a-z\\d_]*)?$','i');

            $scope.title = "";
            $scope.images = [];
            $scope.currentImage = false;
            $scope.loading = false;

            if (urlRegex.test($scope.url)) {
                $scope.loading = true;
                console.log('isUrl');

                ItemService.getMediaData($scope.url).then(function(response) {

                        console.log('Url data recieved')
                        console.log(response.data);


                    angular.forEach(response.data.images, function(image) {
                        $scope.images.push({url: image});
                        $scope.currentImage = 1;

                    });
                    $scope.title = response.data.title;
                    $scope.loading = false;

                }, function(response) {
                    $scope.loading = false;
                });

            }
            else {
                console.log('isNotUrl');
            }
            
        }


        $scope.saveMediaTag = function() {
            console.log("Saving....");
            $scope.loading = true;

            ItemService.saveMediaTag($scope.item.id, $scope.url, $scope.title, $scope.images[$scope.currentImage].url).then(function(response) {
                console.log(response.data);
                $scope.toaster.success('Success', 'You have tagged a media link!');

                ItemService.getItem($scope.item.id).then(function(response) {
                    $scope.$parent.item = response.data;

                    $scope.title = "";
                    $scope.url = "";
                    $scope.images = [];
                    $scope.currentImage = false;
                    $scope.loading = false;

                });
                
            }, function() {
                $scope.toaster.error('Error', '');
            });
        }

    }

]);
