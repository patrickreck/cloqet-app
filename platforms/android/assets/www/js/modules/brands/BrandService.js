BrandModule.service('BrandService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getBrands = function() {
			return $http.get($rootScope.root + '/brands');
		}

		this.getBrand = function(brandId) {
			return $http.get($rootScope.root + '/brands/brand/' + brandId);
		}

		this.like = function(brandId) {
			return $http.post($rootScope.root + '/brands/like', {brand_id: brandId});
		}

	}

]);