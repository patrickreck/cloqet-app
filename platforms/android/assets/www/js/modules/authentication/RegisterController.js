AuthenticationModule.controller('RegisterController', ['$scope', 'AuthenticationService', '$modalInstance',
	function ($scope, AuthenticationService, $modalInstance) {

        $scope.type = null;
        $scope.step = 1;

        $scope.cancel = function() {
            $modalInstance.dismiss();
        }

        $scope.setStep = function(step) {
            $scope.step = step;
        }

        $scope.nextStep = function() {
            $scope.step += 1;
        }

        $scope.reset = function() {
            $scope.step = 1;
        }

        $scope.setType = function(type) {
            $scope.type = type;
        }

        $scope.isSelected = function(type) {
            if (type == $scope.type) {
                return true;
            }
            return false;
        }

        $scope.days = [];
        $scope.day = null;

        var populateDays = function() {
            for (var i = 1; i <= 31; i++) {
                $scope.days.push(i);
            };
        }
        populateDays();

        $scope.months = [
            {id: 1, name: 'January'},
            {id: 2, name: 'February'},
            {id: 3, name: 'March'},
            {id: 4, name: 'April'},
            {id: 5, name: 'May'},
            {id: 6, name: 'June'},
            {id: 7, name: 'July'},
            {id: 8, name: 'August'},
            {id: 9, name: 'September'},
            {id: 10, name: 'October'},
            {id: 11, name: 'November'},
            {id: 12, name: 'December'},
        ];

        $scope.years = [];
        $scope.year = null;

        var populateYears = function() {
            for (var i = 2010; i >= 1910; i--) {
                $scope.years.push(i);
            };
        }
        populateYears();

        
        /* Registering */


        $scope.person = {
            first_name: null,
            last_name: null,
            email: null,
            gender: 1,
            birthday: {
                day: null,
                month: null,
                year: null,
            },
        }
        
        $scope.data = {
            name: null,
            company_name: null,
            phone: null,
            email: null,
            cvr: null,
            website: null,
            contactperson: {
                title: 2,
                first_name: null,
                last_name: null,
                phone: null,
                email: null,
            },
            address: {
                street: null,
                postal: null,
                city: null,
                country: null,
            }
        }

        $scope.register = function() {

            if ($scope.type == 1) {
                AuthenticationService.registerPerson($scope.person).then(function(response) {
                    $scope.toaster.success('Congratulations', 'You have successfully registered a user. Please activate your account using the link we e-mailed you.')
                    $modalInstance.close();
                }, function(response) {
                    console.log("An errr occured");
                });
            }

            else {
                $scope.data.type = $scope.type;
                AuthenticationService.registerStoreOrBrand($scope.data).then(function(response) {
                    $scope.toaster.success('Congratulations', 'You have successfully registered. A confirmation have been sent to the attached e-mail address.')
                    $modalInstance.close();
                });
            }

        }

	}
]);