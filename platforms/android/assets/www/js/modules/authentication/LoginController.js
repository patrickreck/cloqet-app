AuthenticationModule.controller('LoginController', ['$scope', '$modalInstance', 'AuthenticationService',
    function($scope, $modalInstance, AuthenticationService) {

        $scope.email = "reckpatrick@gmail.com";
        $scope.password = "hej123";

        $scope.ok = function() {

            AuthenticationService.login($scope.email, $scope.password).then(function() {
                $modalInstance.close();
            });
            
        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

        $scope.register = function() {
            $modalInstance.dismiss('cancel');
            AuthenticationService.openRegisterModal();
        };

    }
]);