AuthenticationModule.controller('AuthenticationController', ['$scope', '$ionicModal', 'AuthenticationService', 'authService', '$http',
	function ($scope, $ionicModal, AuthenticationService, authService) {

		$scope.login = function() {
//			AuthenticationService.login($scope.email, $scope.password);
		}

		$scope.logout = function() {
			AuthenticationService.logout();
		}

		$scope.isLoggedIn = function() {
			return AuthenticationService.isLoggedIn();
		}

        $scope.getUserId = function() {
            return AuthenticationService.getUserId();
        }

		$scope.getBrandId = function() {
			return AuthenticationService.getBrandId();
		}

        $scope.getUsername = function() {
            return AuthenticationService.getUsername();
        }

		$ionicModal.fromTemplateUrl('js/modules/authentication/partials/modal-login.html', {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
		});

		$scope.openModal = function() {
			AuthenticationService.logout();
			$scope.modal.show();
		};
		
		$scope.closeModal = function() {
			$scope.modal.hide();
		};
		
		//Cleanup the modal when we're done with it!
		$scope.$on('$destroy', function() {
			$scope.modal.remove();
		});

        $scope.user = {
            email: "privat@mail.dk",
            password: "hej123"
        }

        $scope.ok = function() {

            AuthenticationService.login($scope.user.email, $scope.user.password).then(function() {
                $scope.closeModal();
            }, function(response) {
                console.log('authcon');
            	console.log(response.data);
            });
            
        };


        $scope.error = function() {
        	$http.get($rootScope.root + '/auth/unauthorized').then(function(response) {
        		console.log(response.data);
        	}, function(response) {
        		console.log(response.data);
        	});
        }

        $scope.register = function() {
            AuthenticationService.openRegisterModal();
        }


        $scope.$on('event:auth-loginRequired', function() {
			$scope.openModal();
        });


	}
]);