CloqetApp.service('LocationService', ['$rootScope', '$http', '$cordovaGeolocation',

	function($rootScope, $http, $cordovaGeolocation) {

		var myPosition = null;

		this.updateMyPosition = function() {

			return $cordovaGeolocation.getCurrentPosition()
				.then(function (position) {
					myPosition = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};

					$rootScope.$broadcast('updatedPosition', myPosition);

					return myPosition;

				}, function(error) {
				});
		}

		this.getMyPosition = function() {
			return myPosition;
		}


		this.distance = function(lon1, lat1) {
    
	        lon2 = myPosition.lat;
	        lat2 = myPosition.lng;

			var R = 6371;
			var dLat = (lat2-lat1)*Math.PI / 180;
			var dLon = (lon2-lon1)*Math.PI / 180; 
			var a = Math.sin(dLat/2)*Math.sin(dLat/2) +
			    Math.cos(lat1*Math.PI / 180) * Math.cos(lat2*Math.PI / 180) * 
			    Math.sin(dLon/2) * Math.sin(dLon/2); 
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
			var d = R * c;

			return d;
			
		}

	}

]);