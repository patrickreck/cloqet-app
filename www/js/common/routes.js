CloqetApp.config(['$stateProvider', '$urlRouterProvider', 
	function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('app/items');

	$stateProvider

			.state('app', {
				url: "/app",
				abstract: true,
				templateUrl: "templates/menu.html",
			})

			/* Items */
			.state('app.items', {
				url: '/items',
				
				views: {
					'menuContent' :{
						templateUrl: 'js/modules/items/partials/items.html',
						controller: 'ItemsController',

						resolve: {
							items : ['ItemService', '$q', function(ItemService, $q) {
								
								var defer = $q.defer();
								ItemService.getMoreItems().then(function(response) {
									defer.resolve(response.data);

								}, function(response) {
									console.log("Oops, no items");
									console.log(response);
								});

								return defer.promise;
							}]
						}
					},
				},

			})

			.state('app.item', {
				url: '/items/:itemId',
				
				views: {
					'menuContent' :{
						templateUrl: 'js/modules/items/partials/item.html',
						controller: 'ItemController',

						resolve: {

							item : ['ItemService', '$stateParams', '$q', function(ItemService, $stateParams, $q) {
								var defer = $q.defer();

								ItemService.getItem($stateParams.itemId).success(function(data) {
									defer.resolve(data);
								});

								return defer.promise;
							}]
						}
					},
				},

			})

			.state('app.shops', {
				url: '/stores',

				views: {
					'menuContent' :{
						templateUrl: 'js/modules/shops/partials/shops.html',
						controller: 'ShopsController',

						resolve: {
							shops : ['ShopService', '$q', function(ShopService, $q) {
								
								var defer = $q.defer();
								ShopService.getShops().then(function(response) {

									defer.resolve(response.data);

								}, function() {
									console.log("Oops");
								});

								return defer.promise;
							}]
						}						
					}
				}
			})

			.state('app.shop', {
				url: '/stores/:shopId',
				
				views: {
					'menuContent' :{
						templateUrl: 'js/modules/shops/partials/shop.html',
						controller: 'ShopController',

						resolve: {

							shop : ['ShopService', '$stateParams', '$q', function(ShopService, $stateParams, $q) {
								var defer = $q.defer();

								ShopService.getShop($stateParams.shopId).success(function(data) {
									defer.resolve(data);
								});

								return defer.promise;
							}]
						}
					},
				},
			})

			.state('app.shop.featured', {
				url: '/featured',
				templateUrl: 'js/modules/shops/partials/shop.featured.html',
				controller: 'ItemsController',

				resolve: {

					items: ['ItemService', '$stateParams', '$q', function(ItemService, $stateParams, $q) {

						var defer = $q.defer();

						ItemService.getItemsByShopRandom($stateParams.shopId).then(function(response) {
							defer.resolve(response.data);
						}, function(response) {
							console.log('Oh no.......');
						});

						return defer.promise;

					}]
				}
			})

			.state('app.shop.search', {
				url: '/search',
				templateUrl: 'js/modules/shops/partials/shop.search.html',
				controller: 'ShopSearchController',

				resolve: {

					items: ['ItemService', '$stateParams', '$q', function(ItemService, $stateParams, $q) {

						var defer = $q.defer();

						ItemService.getItemsByShop($stateParams.shopId).then(function(response) {
							defer.resolve(response.data);
						}, function(response) {
							console.log('Oh no.......');
						});

						return defer.promise;

					}]
				}
			})




			.state('app.brands', {
				url: '/brands',

				views: {
					'menuContent' :{
						templateUrl: 'js/modules/brands/partials/brands.html',
						controller: 'BrandsController',

						resolve: {
							brands : ['BrandService', '$q', function(BrandService, $q) {
								
								var defer = $q.defer();
								BrandService.getBrands().then(function(response) {

									defer.resolve(response.data);

								}, function() {
									console.log("Oops");
								});

								return defer.promise;
							}]
						}						
					}
				}
			})


			.state('app.brand', {
				url: '/brands/:brandId',
				
				views: {
					'menuContent' :{
						templateUrl: 'js/modules/brands/partials/brand.html',
						controller: 'BrandController',

						resolve: {

							brand : ['BrandService', '$stateParams', '$q', function(BrandService, $stateParams, $q) {
								var defer = $q.defer();

								BrandService.getBrand($stateParams.brandId).success(function(data) {
									defer.resolve(data);
								});

								return defer.promise;
							}]
						}
					},
				},

			})


			.state('app.brand.featured', {
				url: '/featured',
				templateUrl: 'js/modules/brands/partials/brand.featured.html',
				controller: 'ItemsController',

				resolve: {

					items: ['ItemService', '$stateParams', '$q', function(ItemService, $stateParams, $q) {

						var defer = $q.defer();

						ItemService.getItemsByBrandRandom($stateParams.brandId).then(function(response) {

							defer.resolve(response.data);

						}, function(response) {
							console.log('Oh no.......');
						});

						return defer.promise;

					}]
				}
			})

			.state('app.brand.collections', {
				url: '/collections',
				templateUrl: 'js/modules/brands/partials/brand.collections.html',
				controller: 'CollectionsController',

				resolve: {

					collections: ['CollectionService', '$stateParams', '$q', function(CollectionService, $stateParams, $q) {

						var defer = $q.defer();

						CollectionService.getCollectionsByBrand($stateParams.brandId).then(function(response) {

							defer.resolve(response.data);

						}, function(response) {
							console.log('Oh no.......');
						});

						return defer.promise;

					}]
				}

			})

			.state('app.brand.newest', {
				url: '/newest',
				templateUrl: 'js/modules/brands/partials/brand.newest.html',
				controller: 'ItemsController',

				resolve: {

					items: ['ItemService', '$stateParams', '$q', function(ItemService, $stateParams, $q) {

						var defer = $q.defer();

						ItemService.getItemsByBrand($stateParams.brandId).then(function(response) {

							defer.resolve(response.data);

						}, function(response) {
							console.log('Oh no.......');
						});

						return defer.promise;

					}]
				}
			})








			.state('app.lists', {
				url: '/lists-by-user/:userId',

				views: {
					'menuContent' :{
						templateUrl: 'js/modules/lists/partials/lists.html',
						controller: 'ListsController',

						resolve: {

							lists: ['ListService', '$stateParams', '$q', function(ListService, $stateParams, $q) {
								var defer = $q.defer();

								ListService.getListsByUser($stateParams.userId).success(function(data) {
									console.log(data);
									defer.resolve(data);
								});

								return defer.promise;
							}]
						}

					}
				}
			})


			.state('app.list', {
				url: '/list/:listId',

				views: {
					'menuContent' :{
						templateUrl: 'js/modules/lists/partials/list.html',
						controller: 'ListController',

						resolve: {

							list: ['ListService', '$stateParams', '$q', function(ListService, $stateParams, $q) {
								var defer = $q.defer();

								ListService.getList($stateParams.listId).success(function(data) {
									defer.resolve(data);
								});

								return defer.promise;
							}]
						}

					}
				}
			})

			.state('app.list-map', {
				url: '/list/:title/map/:item_ids/:max_distance',

				views: {
					'menuContent': {
						templateUrl: 'js/modules/lists/partials/list-map.html',
						controller: 'ListMapController',

						resolve: {
							shops : ['ShopService', '$q', '$stateParams', function(ShopService, $q, $stateParams) {
								
								var defer = $q.defer();

								ShopService.getShopsWithItemsWithin($stateParams.item_ids, $stateParams.max_distance).then(function(response) {
									defer.resolve(response.data);
								}, function() {
									console.log("Oops");
								});

								return defer.promise;
							}]
						}

					}
				}
			})



			.state('app.messages', {
				url: '/messages',
				
				views: {
					'menuContent' :{
						templateUrl: 'js/modules/conversations/partials/conversations.html',
						controller: 'ConversationsController',

						resolve: {

							conversations : ['ConversationService', '$q', function(ConversationService, $q) {
								var defer = $q.defer();

								ConversationService.getConversations().success(function(data) {
									defer.resolve(data);
								});

								return defer.promise;
							}]
						}
					},
				},
			})








			.state('app.empty', {
				url: '/empty',
				views: {
					'menuContent' :{
						template: 'Emptiness',
					}
				}			
			})

			.state('app.test', {
				url: '/test',

				views: {
					'menuContent' :{
						templateUrl: 'test.html',
						controller: 'TestController',

						resolve: {
							shops : ['ShopService', '$q', function(ShopService, $q) {
								
								var defer = $q.defer();
								ShopService.getShops().then(function(response) {

									defer.resolve(response.data);

								}, function() {
									console.log("Oops");
								});

								return defer.promise;
							}],

							items : ['ItemService', '$q', function(ItemService, $q) {
								
								var defer = $q.defer();
								ItemService.getItems().then(function(response) {

									defer.resolve(response.data);

								}, function() {
									console.log("Oops, no items");
								});

								return defer.promise;
							}]


						}						
					}
				}
			})

	}
]);