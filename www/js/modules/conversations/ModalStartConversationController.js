ConversationModule.controller('ModalStartConversationController', ['$scope', '$modalInstance', 'ConversationService', 'userdata',

    function($scope, $modalInstance, ConversationService, userdata) {

        $scope.id = userdata.id;
        $scope.name = userdata.name;
        $scope.subject = "";
        $scope.message = "";

        $scope.ok = function() {

            ConversationService.startConversation($scope.id, $scope.subject, $scope.message).then(function(response) {
                console.log(response.data);
                $scope.toaster.success('Success', 'Your message was sent to ' + $scope.name);

                $modalInstance.close();

            }, function(response) {
                
                console.log(response.data);
                $scope.toaster.error('Error', 'Your message was not delivered');

            });

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);

