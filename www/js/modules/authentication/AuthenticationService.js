AuthenticationModule.service('AuthenticationService', ['$rootScope', '$http',
	function($rootScope, $http) {

		this.login = function(email, password) {

			console.log('Logging in');


			var postData = {
				email: email,
				password: password
			};

			console.log(postData);

			return $http.post($rootScope.root + '/auth/login', postData).then(function(response) {

				console.log(response.data);

				localStorage.setItem('user_id', response.data.user_id);
				localStorage.setItem('picture', response.data.picture);

				if (typeof(response.data.brand_id) != 'undefined') {
					localStorage.setItem('brand_id', response.data.brand_id);
				}

				else if (typeof(response.data.shop_id) != 'undefined') {
					localStorage.setItem('shop_id', response.data.shop_id);
				}

				localStorage.setItem('username', response.data.username);

			}, function(response) {
				console.log(response.data);
				//$rootScope.toaster.error('Error', response.data);
			});
		};

		this.logout = function() {

			$http.post($rootScope.root + '/auth/logout').then(function(response) {
				
			});

			localStorage.removeItem('user_id');
			localStorage.removeItem('username');
			localStorage.removeItem('shop_id');
			localStorage.removeItem('brand_id');
			localStorage.removeItem('picture');

			$rootScope.$broadcast('loggedOut');
		};

		this.getUserId = function() {
			return localStorage.getItem('user_id');
		};


		this.getBrandId = function() {
			return localStorage.getItem('brand_id');
		};

		this.getUsername = function() {
			return localStorage.getItem('username');
		}

		this.getUserPicture = function() {
			return localStorage.getItem('picture');
		}

		this.isLoggedIn = function() {
			if (localStorage.getItem('user_id') != null) {
				return true;
			}
			return false;
		};

		this.openRegisterModal = function() {
           var modalInstance = $modal.open({
                templateUrl: 'js/modules/authentication/partials/modal-register.html',
                controller: 'RegisterController'
            });

            modalInstance.result.then(function() {
                
            }, function() {
            });            			
		}

		this.registerPerson = function(person) {
			var data = {
				first_name: person.first_name,
				last_name: person.last_name,
				nickname: person.nickname,
				email: person.email,
				gender: person.gender,
				day: person.birthday.day,
				month: person.birthday.month.id,
				year: person.birthday.year,
				password: person.password
			};

			return $http.post($rootScope.root + '/auth/register-person', data);
		}

		this.registerStoreOrBrand = function(data) {
			return $http.post($rootScope.root + '/auth/register-store-or-brand', data);
		}

}]);