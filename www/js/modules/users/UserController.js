UserModule.controller('UserController', ['$scope', 'user', '$modal',
	
	function($scope, user, $modal) {

		$scope.$emit('changeBackground', 'default');

        $scope.breadcrumb = "";

        $scope.$on('$stateChangeStart', function() {
            $scope.breadcrumb = "";
        });


		$scope.user = user.user;
		$scope.likes = user.likes;



        $scope.sendMessage = function() {

            var modalInstance = $modal.open({
                templateUrl: 'js/modules/conversations/partials/modal-send-message.html',
                controller: 'ModalStartConversationController',
                size: 'lg',
                resolve: {
                	userdata: function() {
                		return { id: $scope.user.id, name: $scope.user.nickname };
                	}
                }
            });

            modalInstance.result.then(function() {
            }, function() {
            });
        };

		
	}
]);