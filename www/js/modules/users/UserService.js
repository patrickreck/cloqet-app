UserModule.service('UserService', ['$rootScope', '$http',

	function($rootScope, $http) {

		this.getUsers = function() {
			return $http.get($rootScope.api + '/users');
		}

		this.getUser = function(userId) {
			return $http.get($rootScope.api + '/users/user/' + userId);
		}
	}

]);