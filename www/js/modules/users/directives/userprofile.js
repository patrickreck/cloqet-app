UserModule.directive('userprofile', ['$rootScope', function ($rootScope) {
	return {
		restrict: 'E',
		scope: {
			id: '=',
			username: '='
		},

		transclude: true,

		template: '<a ui-sref="user-profile({ userId: id, userName: slugifiedUsername })"><span ng-if="!hasOtherContent"><b>{{ username }}</b></span><span ng-transclude></span></a>',
		
		link: function (scope, iElement, iAttrs) {
			scope.slugifiedUsername = $rootScope.getSlugified(scope.username);

			scope.hasOtherContent = iElement.find('span').contents().length > 0;

		}
	};
}])