
ItemModule.service('ItemService', ['$rootScope', '$http', 
	function($rootScope, $http) {
		
		this.getItems = function() {
			return $http.get($rootScope.root + '/products', {
				cache: true
			});
		}

		this.getMoreItems = function(skip) {
			return $http.get($rootScope.root + '/products/more/' + skip);
		}

		this.getItem = function(itemId) {
			return $http.get($rootScope.root + '/products/show/' + itemId);
		}

		this.getItemsByBrand = function(brandId) {
			return $http.get($rootScope.root + '/products/items-by-brand/' + brandId);
		}

		this.getItemsByBrandRandom = function(brandId) {
			return $http.get($rootScope.root + '/products/items-by-brand/' + brandId + '/' + true);
		}

		this.getItemsByShop = function(shopId) {
			return $http.get($rootScope.root + '/products/items-by-shop/' + shopId);
		}

		this.getItemsByShopRandom = function(shopId) {
			return $http.get($rootScope.root + '/products/items-by-shop/' + shopId + '/' + true);
		}

		this.getMyItems = function() {
			return $http.get($rootScope.root + '/products/my-items');
		}

		this.searchItems = function(parameters) {
			return $http.get($rootScope.root + '/products/search/' + parameters);
		}

		this.getCategories = function() {
			return $http.get($rootScope.root + '/categories');
		}

		this.getCategoriesByParent = function(parent) {
			return $http.get($rootScope.root + '/categories/subcategories/' + parent);
		}

		this.getCountries = function() {
			return $http.get($rootScope.root + '/countries');
		}

		this.getColors = function() {
			return $http.get($rootScope.root + '/colors');
		}

		this.getConstructions = function() {
			return $http.get($rootScope.root + '/constructions');
		}

		this.getMaterials = function() {
			return $http.get($rootScope.root + '/materials');
		}

		this.createItem = function(postData) {
			return $http.post($rootScope.root + '/products/store', postData);
		}

		this.getShopAttachments = function(item_id) {
			return $http.get($rootScope.root + '/products/shop-attachments/' + item_id);
		}

		this.like = function(item_id) {
			return $http.post($rootScope.root + '/products/like', {item_id: item_id});
		}

		this.have = function(item_id) {
			return $http.post($rootScope.root + '/products/have', {item_id: item_id});
		}

		this.getMediaTags = function(item_id) {
			return $http.get($rootScope.root + '/products/media-tags/' + item_id);
		}

		this.getMediaData = function(userUrl) {
			return $http.post($rootScope.root + '/media/url', {url: userUrl});
		}

		this.saveMediaTag = function(item_id, url, title, image) {
			return $http.post($rootScope.root + '/media/tag', {item_id: item_id, url: url, title: title, imagepath: image});
		}

	}
]);