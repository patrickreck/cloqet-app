ItemModule.controller('ItemsController', ['$scope', 'items', '$cordovaSplashscreen', 'ItemService',
	function ($scope, items, $cordovaSplashscreen, ItemService) {


		$scope.items = items;
/*
		angular.forEach($scope.items, function(item, index) {
			var foundIt = false;
			angular.forEach(item.images, function(image) {
				console.log('oops');
				if (!foundIt) {
					console.log('Gotcha');
					item.images[0].path = 'thumb_' + image.path;
					foundIt = true;
				}
			});
		});
*/

		$scope.noMoreItems = false;

		$scope.loadMoreData = function() {
			console.log('Loading more');

			ItemService.getMoreItems($scope.items.length).then(function(response) {
				if (response.data.length > 0) {

					angular.forEach(response.data, function(item) {
						$scope.items.push(item);
					});

					angular.forEach($scope.items, function(item, index) {
						var foundIt = false;
						angular.forEach(item.images, function(image) {
							if (!foundIt) {
								item.images[0].path = 'thumb_' + image.path;
								foundIt = true;
							}
						});
					});
				}
				else {
					$scope.noMoreItems = true;
				}

				$scope.$broadcast('scroll.infiniteScrollComplete');

			});

		}


		
	}
]);
