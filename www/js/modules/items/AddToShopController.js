ItemModule.controller('AddToShopController', ['$scope', 'ItemService', 'ShopService', '$modal',
    function($scope, ItemService, ShopService, $modal) {

        $scope.shops = [];
        $scope.success_box = false;

        $scope.updateShops = function() {

            ItemService.getShopAttachments($scope.item.id).then(function(response) {
                $scope.shops = response.data;

                angular.forEach($scope.shops, function(shop, key) {

                    if (shop.attached == null) {
                        shop.checked = false;
                    }
                    else {
                        shop.checked = true;
                    }
                });

            }, function() {
                console.log("Error");
            });

        };
        $scope.updateShops();


        $scope.checkboxChanged = function(shop) {
        	shop.changed = true;
        }


        $scope.attachShops = function() {

        	var changedShops = [];

            angular.forEach($scope.shops, function(shop, key) {
                if (shop.changed == true) {
                	changedShops.push({ shop_id: shop.id, checked: shop.checked });
                }
            });


            if (changedShops.length <= 0) {
            	$scope.toaster.warning('Warning', 'You did not make any changes!');
            	return;
            }

            ShopService.attachItems({item: $scope.item.id, shops: changedShops}).then(function(response) {

	            $scope.toaster.success('Yay!', 'The shop attachments has been updated')
	
	            ItemService.getItem($scope.item.id).then(function(response) {
	            	console.log(response.data);
	            	$scope.$parent.item = response.data;
	            });

	            angular.forEach($scope.shops, function(shop, key) {
	            	if (shop.changed == true) {
	            		shop.changed = false;
	            	}
	            })

            });



        }


        $scope.open = function(size) {

            var modalInstance = $modal.open({
                templateUrl: 'addShop.html',
                controller: 'AddShopShortController'
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.updateShops();
            }, function() {
                console.log('Modal dismissed at: ' + new Date());
            });
        };


    }
]);
