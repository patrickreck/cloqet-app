ItemModule.controller('ItemController', [
    '$scope',
    '$rootScope',
    'ItemService',
    'item',
    '$sce',
    '$ionicSlideBoxDelegate',
    '$filter',
    '$ionicModal',
    '$cordovaGeolocation',
    'LocationService',

    function(
        $scope,
        $rootScope,
        ItemService,
        item,
        $sce,
        $ionicSlideBoxDelegate,
        $filter,
        $ionicModal,
        $cordovaGeolocation,
        LocationService
    ) {


        $scope.item = item;

        console.log(item);

        $scope.item.description = $sce.trustAsHtml($scope.item.description);

        $scope.likes = item.likes;
        $scope.i_like = item.i_like;

        $scope.haves = item.haves;
        $scope.i_have = item.i_have;




        /* APP SPECIFICS */
        
        $scope.item.allimages = [];
        angular.forEach($scope.item.colors, function(color) {
            
            angular.forEach(color.images, function(image) {
                $scope.item.allimages.push(image);
            });
        });

        $scope.extra_info = false;
        $scope.toggleInfo = function() {
            console.log("toggling");
            $scope.extra_info = !$scope.extra_info;
            $ionicSlideBoxDelegate.enableSlide(!$scope.extra_info);
        }


        $scope.directions = function(toLng, toLat) {
            window.location = "maps:daddr=" + toLat + "," + toLng + "&saddr=" + $scope.lat + "," + $scope.lng;
        }


        LocationService.updateMyPosition().then(function() {
            angular.forEach($scope.item.shops, function(shop) {
                shop.distance = LocationService.distance(shop.lat, shop.lng);
            });
        });

/*
        $scope.lat = 0;
        $scope.lng = 0;

        $cordovaGeolocation.getCurrentPosition()
            .then(function (position) {

                $scope.lat = position.coords.latitude;
                $scope.lng = position.coords.longitude;

                console.log('lat' + $scope.lat + ' lng' + $scope.lng);

                angular.forEach($scope.item.shops, function(shop) {
                    shop.raw_distance = $scope.distance($scope.lng, $scope.lat, parseFloat(shop.lng), parseFloat(shop.lat));

                    shop.distance = $filter('metricdistance')(shop.raw_distance);
                });

            }, function(err) {

        });

*/



        /* All stores */

        $scope.distance_range = 250;

        $ionicModal.fromTemplateUrl('all-stores.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function() {
            $scope.modal.show();
        
        };
        
        $scope.closeModal = function() {
            $scope.modal.hide();
        };
        
        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
            $scope.modal.remove();
        });

        $scope.sortBy = null;


























        /* IMAGE SWITCH */

        $scope.currentImage = null;

        $scope.changeCurrentImage = function(image) {
            $scope.currentImage = image;
        }


        /* LIKE */
        $scope.like = function() {

            var old_i_like = $scope.i_like;
            var old_likes = $scope.likes;

            if ($rootScope.isLoggedIn()) {
                if ($scope.i_like == true) {
                    $scope.likes--;
                }            
                else {
                    $scope.likes++;
                }
                
                $scope.i_like = !$scope.i_like;
            }

            ItemService.like(item.id).then(function(data) {
            }, function() {
                $scope.i_like = old_i_like;
                $scope.likes = old_likes;
            });

        }

        $scope.have = function() {

            var old_i_have = $scope.i_have;
            var old_haves = $scope.haves;

            if ($rootScope.isLoggedIn()) {
                if ($scope.i_have == true) {
                    $scope.haves--;
                }            
                else {
                    $scope.haves++;
                }
                
                $scope.i_have = !$scope.i_have;
            }

            ItemService.have(item.id).then(function(data) {
            }, function() {
                $scope.i_have = old_i_have;
                $scope.haves = old_haves;
            });

        }



        /* COLORS */

        $scope.currentColor = null;

        $scope.changeColor = function(color) {

            $scope.currentColor = color;

            $scope.changeCurrentImage($scope.currentColor.images[0]);
        }

        // Set the current color on load
        $scope.changeColor($scope.item.colors[0]);

        /* END COLORS */



        /* Show all shops */
        $scope.showAllShops = function() {

            var modalInstance = $modal.open({
                templateUrl: 'all-shops.html',
                controller: 'ShowAllShopsController',
                size: 'lg',
                resolve: {
                    item: function() {
                        return $scope.item;
                    }
                }
            });

            modalInstance.result.then(function(selectedItem) {
                $scope.updateShops();
            }, function() {
                console.log('Modal dismissed at: ' + new Date());
            });
        };




    }
]);

ItemModule.controller('ShowAllShopsController', ['$scope', '$modalInstance', 'item',
    function($scope, $modalInstance, item) {

        $scope.item = item;

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);

