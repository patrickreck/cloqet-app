BrandModule.controller('BrandController', ['$scope', 'brand', 'BrandService',
	
	function($scope, brand, BrandService) {

		$scope.$emit('changeBackground', brand.cover);
	
		$scope.brand = brand;

		$scope.info = false;
		$scope.toggleInfo = function() {
			$scope.info = !$scope.info;
		}

        console.log(brand);


        $scope.sendMessage = function() {

            var modalInstance = $modal.open({
                templateUrl: 'js/modules/conversations/partials/modal-send-message.html',
                controller: 'ModalStartConversationController',
                size: 'lg',
                resolve: {
                    userdata: function() {
                        return { id: brand.user_id, name: brand.brand_name };
                    }
                }
            });

            modalInstance.result.then(function() {
            }, function() {
            });
        };


        $scope.likes = brand.likes;
        $scope.i_like = brand.i_like;

        $scope.like = function() {

            var old_i_like = $scope.i_like;
            var old_likes = $scope.likes;

            if ($scope.isLoggedIn()) {
                if ($scope.i_like == true) {
                    $scope.likes--;
                }            
                else {
                    $scope.likes++;
                }
                
                $scope.i_like = !$scope.i_like;
            }

            BrandService.like(brand.id).then(function(data) {
                console.log(data);
            }, function() {
                $scope.i_like = old_i_like;
                $scope.likes = old_likes;
            });

        }

	}

]);