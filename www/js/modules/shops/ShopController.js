ShopModule.controller('ShopController', ['$rootScope', '$scope', 'shop', '$state', '$ionicModal', '$timeout',
	
	function($rootScope, $scope, shop, $state, $ionicModal, $timeout) {

		$scope.$emit('changeBackground', shop.cover);

		$scope.shop = shop;

		$scope.info = false;
		$scope.toggleInfo = function() {
			$scope.info = !$scope.info;
		}

		$scope.currentState = $state.current.name;
		$rootScope.$on('$stateChangeSuccess', function() {
			$scope.currentState = $state.current.name;
			
			$scope.info = false;
			if ($scope.currentState == 'app.shop.search') {
				$scope.info = true;
			}

		});



		$scope.expanded = true;
		$scope.expand = function() {

			if ($scope.expanded) {
				$scope.info = true;
	
				$timeout(function() {

					$('.profile .top').css({
						height: window.innerHeight - 115,
						marginTop: -window.innerHeight + 115
					});
					$('.profile .buttons').css({
						marginTop: window.innerHeight - 115
					});

				});

			}
			else {
				$scope.info = false;

				$('.profile .top').css({
					height: "300px",
					marginTop: "0px"
				});
				$('.profile .buttons').css({
					marginTop: "0px"
				});
			}

			$timeout(function() {

				// Fire resize event
			}, 10);

			
			$scope.expanded = !$scope.expanded;

		}


		/* Maps */

		$scope.maps = false;

		$scope.toggleMaps = function() {
			$scope.maps = !$scope.maps;
		}




    	$scope.map;
	    $scope.$on('mapInitialized', function(evt, evtMap) {
	      $scope.map = evtMap;
			$scope.map.setCenter(new google.maps.LatLng(shop.lat, shop.lng));

	    });


	}

])

.controller('ShopSearchController', ['$scope', 'items',
	function($scope, items) {
		$scope.items = items;

	}
]);