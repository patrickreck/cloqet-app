ItemModule.controller('AddShopShortController', ['$scope', '$modalInstance', 'ShopService',
    function($scope, $modalInstance, ShopService) {


        $scope.ok = function() {
        	console.log($scope.name);

        	ShopService.createShop({ shop_name: $scope.name, adress: $scope.address }).then(function(response) {
	           console.log(response.data);
                $modalInstance.close();
        	}, function(response) {
        		console.log("Error");
                console.log(response.data);
        	});

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');
        };

    }
]);