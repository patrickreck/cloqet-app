ShopModule.directive('shopprofile', ['$rootScope', function ($rootScope) {
	return {
		restrict: 'E',
		scope: {
			id: '=',
			shopname: '='
		},

		transclude: true,

		template: '<a ui-sref="store({ storeId: id, storeName: slugifiedStorename })"><b>{{ shopname }}</b><span ng-transclude></span></a>',
		
		link: function (scope, iElement, iAttrs) {

		}
	};
}])