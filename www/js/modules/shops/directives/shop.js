ShopModule.directive('shop', ['$rootScope', function ($rootScope) {
	return {
		restrict: 'E',
		scope: {
			shop: '='
		},

		transclude: true,

		templateUrl: 'js/modules/shops/directives/templates/shop.html',
		
		link: function (scope, iElement, iAttrs) {
		}
	};
}])