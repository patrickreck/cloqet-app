CollectionModule.controller('CreateCollectionController', [
	'$scope', '$modal', '$q', 'CollectionService', 'ItemService', 'items', '$state',

	function ($scope, $modal, $q, CollectionService, ItemService, items, $state) {

		$scope.items = items;

        $scope.updateBox = {};

        $scope.submitted = false;

        $scope.publish = 1;

        $scope.openAddItems = function() {

            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: 'SelectItemsController',
                size: 'lg',
                resolve: {
                    items: function() {
                        return $scope.items;
                    }
                }
            });

            modalInstance.result.then(function(tickedItems) {
            }, function() {
                console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.saveCollection = function() {

            if ($scope.form.$invalid) {
                $scope.submitted = true;
                $scope.$emit('scrollToTop');
                return;
            }

            var addedItems = [];

            angular.forEach($scope.items, function(item, key) {
                if (item.ticked == 1) {
                    addedItems.push(item.id);
                }
            });

            CollectionService.saveCollection($scope.name, $scope.description, addedItems).then(function(response) {
                console.log(response.data);
                var data = response.data;
                $state.go('brand.collection', {'brandId': data.brand_id, 'brandName': $scope.getSlugified(data.brand_name), 'collectionId': data.id, 'collectionName': $scope.getSlugified(data.name)});
            });
        }

	}
]);