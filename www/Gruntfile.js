module.exports = function(grunt) {
	grunt.initConfig({

		pkg: grunt.file.readJSON("package.json"),
		
		concat: {

			js: {
				src: [

					'js/app.js',

					'js/common/*.js',
					'js/common/*/*.js',


					'js/modules/authentication/AuthenticationModule.js',
					'js/modules/brands/BrandModule.js',
					'js/modules/collections/CollectionModule.js',
					'js/modules/items/ItemModule.js',
					'js/modules/shops/ShopModule.js',
					'js/modules/users/UserModule.js',
					'js/modules/conversations/ConversationModule.js',
					'js/modules/lists/ListModule.js',

					'js/modules/*.js',
					'js/modules/*/*.js',
					'js/modules/*/*/*.js',


				],

				dest: 'dist.js'
			},

			css: {
				src: ['css/**/*.css', 'css/*.css', 'dev/*.css'],
				dest: 'dev/app.css'
			}
		},


		cssmin: {
			compress: {
				files: {
					"dist/app.min.css" : "<%= concat.css.dest %>"
				}
			}
		},

		htmlmin: {
			dist: { 
				options: {
					removeComments: true,
					collapseWhitespace: true
				}
			},

			files: {
				'dist/index.html': 'index.html'
			}	
		},

		uglify: {
			js: {
				files: {
					'dist.min.js':['dist.js']
				}
			}
		},

		less: {
			main: {
				files: {
					'styles.css': 'css/*.less'
				}
			},
			sub: {
				files: {
					'pages.css': 'css/pages/*.less'
				}
			}

		},


		watch: {
			js: {
				files: ['<%= concat.js.src %>'],
				tasks: ['concat:js']
			},

			less: {
				files: ['css/*.less', 'css/pages/*.less'],
				tasks: ['less']
			},

		}


	});

	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');

	grunt.registerTask('default', 'dist');
};