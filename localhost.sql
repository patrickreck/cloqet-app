-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Vært: localhost:8889
-- Genereringstid: 23. 10 2014 kl. 11:37:32
-- Serverversion: 5.5.38
-- PHP-version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cloqet`
--

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `brands`
--

CREATE TABLE `brands` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `cvr` int(11) NOT NULL,
  `website` varchar(255) NOT NULL,
  `phone` int(11) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `description` text,
  `contact_title` int(2) NOT NULL,
  `contact_first_name` varchar(50) NOT NULL,
  `contact_last_name` varchar(50) NOT NULL,
  `contact_phone` varchar(8) NOT NULL,
  `contact_email` varchar(150) NOT NULL,
  `street` varchar(100) NOT NULL,
  `postal` int(4) NOT NULL,
  `city` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Data dump for tabellen `brands`
--

INSERT INTO `brands` (`id`, `user_id`, `brand_name`, `cvr`, `website`, `phone`, `cover`, `description`, `contact_title`, `contact_first_name`, `contact_last_name`, `contact_phone`, `contact_email`, `street`, `postal`, `city`, `country`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 'Savannah Wild', 12345678, 'http://www.samsoe.com/', 29828409, 'savannahwild.jpg', 'Savannah Wild was founded in 2012, and is a tale of combining design process and aesthetics with inspiration from the natural world and the imagination.  Head designer and owner Sarah Sheikh is educated in political science in both London and Copenhagen and has worked in politics and interior design, before ultimately finding her place in the world of fashion.', 0, '', '', '', '', '', 0, '', '', 1, '2013-08-04 15:34:39', '2013-08-07 08:33:34'),
(3, 3, 'G-Star', 12345678, 'http://www.g-star.com/', 427228119, 'gstar.jpg', '', 0, '', '', '', '', '', 0, '', '', 0, '2013-08-04 21:54:51', '2013-08-04 21:54:51'),
(4, 4, 'Diesel', 42817392, 'http://www.diesel.com/', 571628437, 'diesel.jpg', '', 0, '', '', '', '', '', 0, '', '', 1, '2013-08-06 09:50:12', '2013-08-07 20:09:12'),
(5, 5, 'Hugo Boss', 58172782, 'http://www.hugoboss.com/', 28741728, 'hugoboss.jpg', '', 0, '', '', '', '', '', 0, '', '', 1, '2013-08-06 12:00:40', '2013-08-07 12:05:51'),
(6, 6, 'Ecko', 58192847, 'http://www.ecko.com/', 19283758, 'ecko.jpg', '', 0, '', '', '', '', '', 0, '', '', 1, '2013-08-06 12:02:37', '2013-08-28 06:58:01'),
(7, 28, 'Revolution', 12345678, 'www.revolution.com', 29828409, '', '', 2, 'Hans', 'Hansen', '29828409', 'hans@revolution.com', 'Tordenskjoldsgade 25', 1055, 'København K', 'Denmark', 0, '2014-08-28 13:42:43', '2014-08-28 13:42:43');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `brands_likes`
--

CREATE TABLE `brands_likes` (
`id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Data dump for tabellen `brands_likes`
--

INSERT INTO `brands_likes` (`id`, `brand_id`, `user_id`, `updated_at`, `created_at`) VALUES
(26, 4, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `categories`
--

CREATE TABLE `categories` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Data dump for tabellen `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`) VALUES
(1, 'Clothing - Fashion & Streetwear', NULL),
(2, 'Shirts', 1),
(3, 'Sleeve', 2),
(4, 'Short Sleeve', 3),
(5, 'Long Sleeve', 3),
(6, 'Fit', 2),
(7, 'Slim Fit', 6),
(8, 'Regular Fit', 6),
(9, 'Blouse', 2),
(10, 'Jeans', 1),
(11, 'Super Skinny', 10),
(12, 'Slim', 10),
(13, 'Straight Cut', 10),
(14, 'Loose', 10),
(15, 'Boot Cut / Flare', 10),
(16, 'Dresses', 1),
(17, 'Party Dress', 16),
(18, 'Wedding', 16),
(19, 'Bandeau', 16),
(20, 'Bodycon', 16),
(21, 'Long Sleeve', 16),
(22, 'Short Sleeve', 16),
(23, 'Cut out', 16),
(24, 'Oversize', 16),
(25, 'Little Black Dress', 16),
(26, 'Maxi Dresses', 16),
(27, 'Skirts', 1),
(28, 'Mini', 27),
(29, 'Mid-Length', 27),
(30, 'Long', 27),
(33, 'Clothing - Sportswear & Fashion', NULL),
(34, 'Jackets & Coats', 33),
(35, 'Tops', 33);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `cloqin_campaign_users`
--

CREATE TABLE `cloqin_campaign_users` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cloqin_campaign_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Data dump for tabellen `cloqin_campaign_users`
--

INSERT INTO `cloqin_campaign_users` (`id`, `user_id`, `cloqin_campaign_id`, `updated_at`, `created_at`) VALUES
(1, 3, 1, '2013-10-12 08:22:23', '2013-10-12 10:28:23');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `cloqin_campaigns`
--

CREATE TABLE `cloqin_campaigns` (
`id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `text_offer` text NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `expenditure` int(11) NOT NULL,
  `activated` int(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Data dump for tabellen `cloqin_campaigns`
--

INSERT INTO `cloqin_campaigns` (`id`, `shop_id`, `item_id`, `text_offer`, `from`, `to`, `expenditure`, `activated`) VALUES
(1, 1, 17, '', '2013-10-11 09:00:00', '2013-10-14 12:00:00', 0, 1);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `collection_item`
--

CREATE TABLE `collection_item` (
`id` int(11) NOT NULL,
  `collection_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Data dump for tabellen `collection_item`
--

INSERT INTO `collection_item` (`id`, `collection_id`, `item_id`) VALUES
(1, 1, 12),
(2, 1, 16),
(3, 1, 17),
(4, 2, 1),
(5, 2, 7),
(6, 2, 12),
(7, 2, 16);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `collections`
--

CREATE TABLE `collections` (
`id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `collections`
--

INSERT INTO `collections` (`id`, `brand_id`, `name`, `description`, `updated_at`, `created_at`) VALUES
(1, 1, 'Waves', 'My Spring/Summer 2014 collection reflects the soft and shiny texture of water, while maintaining the hardness of the waves. Therefore I am introducing a sleek and shiny calf leather in Salmon, Emerald Green and Deep Ocean Blue to reflect this.<br/><br/> \n\nWater warps reality, and when submerging an object into the water it changes the dimensions of the object by creating a higher pressure on the object. Playing with dimensions has thus been key to this collection, where I have created mini versions of bags while keeping a strong graphic notion throughout.', '2014-07-30 11:52:33', '2014-07-30 11:52:33'),
(2, 1, 'A new collection', 'asdqwesad', '2014-07-30 12:42:06', '2014-07-30 12:42:06');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `colors`
--

CREATE TABLE `colors` (
`id` int(11) NOT NULL,
  `color` varchar(255) NOT NULL,
  `hex` varchar(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Data dump for tabellen `colors`
--

INSERT INTO `colors` (`id`, `color`, `hex`) VALUES
(1, 'Black', '242424'),
(2, 'Blue', '1a80ff'),
(3, 'Green', '39b61c'),
(4, 'Orange', 'ff7e00'),
(5, 'Pink', 'ff6ac9'),
(6, 'Red', 'c20e0e'),
(7, 'White', 'ffffff'),
(8, 'Yellow', 'ffd200'),
(9, 'Brown', '554130'),
(10, 'Grey', 'b3b3b3');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `constructions`
--

CREATE TABLE `constructions` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Data dump for tabellen `constructions`
--

INSERT INTO `constructions` (`id`, `name`) VALUES
(1, 'Knit'),
(2, 'Heavy knit'),
(3, 'Woven'),
(4, 'Woven - Denim');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `conversations`
--

CREATE TABLE `conversations` (
`id` int(11) NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `deleted_from` int(1) NOT NULL DEFAULT '0',
  `deleted_to` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Data dump for tabellen `conversations`
--

INSERT INTO `conversations` (`id`, `user_from`, `user_to`, `subject`, `deleted_from`, `deleted_to`, `created_at`, `updated_at`) VALUES
(1, 3, 2, 'I have something interesting to say', 0, 1, '2014-08-18 12:31:13', '2014-08-20 17:03:51'),
(2, 3, 2, 'One more', 0, 1, '2014-08-18 12:48:04', '2014-08-20 17:06:55'),
(3, 1, 2, 'Yep, I''m a brand', 0, 0, '2014-08-18 14:57:37', '2014-08-25 20:58:47'),
(4, 2, 3, 'Let''s start a new conversation', 0, 0, '2014-08-19 15:26:19', '2014-10-05 14:58:57'),
(5, 7, 2, 'Hey', 0, 0, '2014-08-20 12:27:34', '2014-08-25 18:41:26'),
(6, 2, 1, 'I am your biggest fan', 1, 0, '2014-08-20 16:52:30', '2014-08-20 20:12:25'),
(7, 2, 9, 'I love your store', 0, 0, '2014-08-20 17:02:10', '2014-08-20 17:02:10');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `countries`
--

CREATE TABLE `countries` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `currency` varchar(5) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Data dump for tabellen `countries`
--

INSERT INTO `countries` (`id`, `name`, `currency`) VALUES
(1, 'Danmark', 'DKK'),
(2, 'Sverige', 'SKK'),
(3, 'England', 'GBP');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `customers`
--

CREATE TABLE `customers` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `profilepicture` varchar(255) NOT NULL,
  `bday_day` int(11) NOT NULL,
  `bday_month` int(11) NOT NULL,
  `bday_year` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Data dump for tabellen `customers`
--

INSERT INTO `customers` (`id`, `user_id`, `nickname`, `first_name`, `last_name`, `profilepicture`, `bday_day`, `bday_month`, `bday_year`, `updated_at`, `created_at`) VALUES
(1, 3, 'carolinemarburger', 'Caroline', 'Marburger', 'caroline.jpg', 0, 0, 0, '2014-01-28 09:00:00', '2014-01-01 00:00:00'),
(2, 2, 'patrickreck', 'Patrick', 'Reck', 'patrick.jpg', 0, 0, 0, '2014-01-28 09:00:00', '2014-01-01 00:00:00'),
(15, 24, 'pareck', 'Ethan', 'Cooper', '', 30, 3, 1992, '2014-08-27 14:47:21', '2014-08-27 14:47:21'),
(16, 25, 'EthanCooper', 'Ethan', 'Cooper', '', 3, 1, 1929, '2014-08-27 14:50:22', '2014-08-27 14:50:22'),
(17, 26, 'ethy', 'ethan', 'cooper', '', 1, 3, 1999, '2014-08-27 14:53:37', '2014-08-27 14:53:37'),
(18, 30, 'ditteillum', 'Ditte', 'Illum', '', 20, 9, 1993, '2014-08-28 21:19:40', '2014-08-28 21:19:40'),
(19, 31, 'patrickrecka', 'Patrick', 'Reck', '', 30, 3, 1991, '2014-09-03 15:10:39', '2014-09-03 15:10:39');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `event_item`
--

CREATE TABLE `event_item` (
`id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `discount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `events`
--

CREATE TABLE `events` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `adress` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `type` int(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Data dump for tabellen `events`
--

INSERT INTO `events` (`id`, `user_id`, `name`, `description`, `adress`, `date`, `type`, `updated_at`, `created_at`) VALUES
(1, 6, 'We are opening!', 'Yes, this time we really are opening!', '', '2013-10-01', 1, '2013-09-25 11:45:40', '2013-09-25 11:45:40');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `groups`
--

CREATE TABLE `groups` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Data dump for tabellen `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Users', '{"user":1, "create-news":1}', '2013-08-03 09:58:56', '2013-08-03 09:58:56'),
(2, 'Shops', '{"shop":1}', '2013-08-03 09:58:56', '2013-08-03 09:58:56'),
(3, 'Brands', '{"brand":1}', '2013-08-03 09:58:56', '2013-08-03 09:58:56');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `items`
--

CREATE TABLE `items` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `size_fit` varchar(255) DEFAULT NULL,
  `construction_id` int(11) DEFAULT NULL,
  `gender` int(1) NOT NULL,
  `webshop_url` varchar(255) DEFAULT NULL,
  `published` int(1) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Data dump for tabellen `items`
--

INSERT INTO `items` (`id`, `user_id`, `brand_id`, `name`, `description`, `product_code`, `size_fit`, `construction_id`, `gender`, `webshop_url`, `published`, `category_id`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 'RVLT Heavy', 'Lang og lækker jakke fra RVLT. RVLT jakken har to side lommer med ikke synligt knappe system. På samme måde er knapperne ned langs RVLT jakkens front også skjulte når jakken er lukket. \n\nHar snøre ved hætte. Inderlomme med plads til eks. mobil eller tegnebog.', 'rvlt-heavy', 'Normal size', 1, 1, 'http://www.2trendy.dk/shop/rvlt-heavy-black-16482p.html', 1, 1, '2013-12-06 17:58:23', '2013-12-06 17:58:23'),
(3, 1, 3, 'TOPSTAR HI-SUEDE', 'Suede high-top sneakers by PONY. Lace-up front and logo print at the back. Leather detailing to sides. Padded cuff. \nCircumference 28 cm. Shaft 10 cm. Sole 2,5 cm.\nMade of suede. Inside of textile. Rubber outsole.', NULL, 'Normal', 1, 1, 'http://nelly.com/uk/mens-fashion/shoes/trainers/pony-11046/topstar-hi-suede-106610-162', 1, 6, '2013-12-06 22:08:43', '2013-12-06 22:08:43'),
(4, 1, 6, 'Kevin belt', 'Jack & Jones ''Kevin'' bælte. Bæltet måler ca. 3,8 cm i bredden. Kvaliteten er i 100% koskind.', 'kevin-belt', 'One-size', 1, 1, NULL, 1, 9, '2013-12-06 22:14:25', '2013-12-06 22:14:25'),
(5, 1, 4, 'Dinni Dress', 'Long dress in an oversized fit from SELECTED FEMME. Sequinned embellished and belt loops with self tie strap belt at the waist. Opening and hook and eye to the reverse. Hand washing is recommended.', NULL, 'Small fit', 1, 2, 'http://nelly.com/uk/womens-fashion/clothing/party-dresses/selected-femme-796/dinni-dress-212608-14/', 1, 11, '2013-12-13 08:37:33', '2013-12-13 08:37:33'),
(6, 1, 5, 'Glory New Cardigan', 'This classic cardigan by Vero Moda is a perfect addition to your autumn wardrobe.', NULL, NULL, NULL, 2, 'http://www.boozt.com/dk/da/vero-moda/glory-new-ls-o-neck-cardigan-noos_3203342/3203353?navId=47990&group=listing', 1, 12, '2013-12-13 12:52:12', '2013-12-13 12:52:12'),
(7, 1, 1, 'Report oldschool root', 'Taske fra CONVERSE med stort, kontrastfarvet logo. To lommer foran med lynlås. Tasken indeholder en lomme til laptop med vatteret beskyttelse samt en mindre lomme med lynlås. Justerbar skulderrem med forstærkning over skulderen.', '282276-0046', NULL, NULL, 3, 'http://nelly.com/dk/t%C3%B8j-til-kvinder/tilbeh%C3%B8r/tasker/converse-282/reporter-oldschool-root-282276-46/', 1, 4, '2013-12-19 14:52:21', '2013-12-19 14:52:21'),
(12, 1, 1, 'Proud Top', 'Top fra M BY M. Korte ærmer samt afrundet hals. Lige pasform.', NULL, 'Normal', NULL, 2, NULL, 1, 9, '2014-04-25 10:10:38', '2014-04-25 10:10:38'),
(16, 1, 1, 'NEON FLORAL', 'Mønstret playsuit fra AX PARIS. Kortærmet samt afrundet halsudskæring. Markeret talje med fold. Lukkes med skjult lynlås i ryggen.', NULL, NULL, NULL, 2, NULL, 1, 17, '2014-06-17 12:26:55', '2014-06-17 12:26:54'),
(17, 1, 1, 'HOT ANATOMY', 'Bikini fra HOT ANATOMY. Bikinitop med trekantet pasform med udtagelig vattering i skålene samt bindebånd i nakken og ryggen. Dekorative blonder på skålene. Bikinitrusse med tanga pasform med dekorative blonder ved taljen samt justerbare bindebånd i siderne. Scrunch bagved. Helforet inderside. Maskinevaskes på 30 grader.', NULL, NULL, NULL, 2, NULL, 1, 28, '2014-07-22 11:30:37', '2014-07-22 11:30:37'),
(18, 1, 1, 'ESTRADEUR', 'Top fra ESTRADEUR. Afrundet halsudskæring samt bryderryg.', NULL, NULL, NULL, 2, NULL, 1, 7, '2014-07-30 13:18:55', '2014-07-30 13:18:54'),
(19, 1, 1, 'SCAPE MINI DRESS', 'Kjole af trikot fra AQ/AQ. Afrundet halsudskæring samt åben del ved taljen og ryggen. Lukkes med skjult lynlås bagved.', NULL, NULL, NULL, 2, NULL, 1, 17, '2014-07-31 09:28:36', '2014-07-31 09:28:36'),
(20, 1, 1, 'Super dress', 'Super dress 40.000 with extra machineguns', NULL, NULL, NULL, 2, NULL, 1, 20, '2014-07-31 14:13:15', '2014-07-31 14:13:15'),
(24, 1, 1, 'SL OLIVE BLOUSE', 'Ærmeløs bluse fra RIVER ISLAND. V-ringet halsudskæring samt slå om effekt foran. Længere bagved.', NULL, NULL, NULL, 2, NULL, 1, 4, '2014-07-31 15:04:16', '2014-07-31 15:04:16'),
(25, 1, 1, 'LACE DETAIL SKATER DRESS', 'Kjole med overdel af blonder fra AX PARIS. V-ringet halsudskæring med slå om effekt foran. <br /><br />Markeret talje. Bredere nederdel. Lukkes med skjult lynlås i ryggen. Helforet.', NULL, NULL, NULL, 2, NULL, 1, 17, '2014-08-01 09:03:53', '2014-08-01 09:03:52'),
(26, 1, 1, 'Sweet Bird', 'T-shirt fra SWEET. Afrundet halsudskæring med ribstrik. Print foran. Detalje med logo ved kanten længst nede bagved.', NULL, NULL, NULL, 1, NULL, 1, 4, '2014-08-03 13:32:34', '2014-08-03 13:32:34'),
(27, 1, 1, 'M. PAUL OXFORD SHIRT', 'Skjorte med button down krave fra FILIPPA K. Knaplukning samt brystlomme. Convertible manchet ved ærmerne.', NULL, NULL, NULL, 1, NULL, 1, 4, '2014-08-03 13:36:54', '2014-08-03 13:36:54'),
(28, 1, 1, 'BLACK JACKET', 'Blazer fra RIVER ISLAND. To knapper samt vindslag. To lommer foran samt en brystlomme. Slids bagved samt let vattering i skuldrene. To inderlommer. Dekorative manchetknapper ved ærmerne. Kemisk rensning anbefales.', NULL, NULL, NULL, 1, NULL, 1, 4, '2014-08-03 13:38:35', '2014-08-03 13:38:35'),
(29, 1, 1, 'Plain', 'Stofsko fra EXANI. Snørebånd foran. Afrundet tå samt forstærket hæl.', NULL, NULL, NULL, 1, NULL, 1, 4, '2014-08-03 13:40:30', '2014-08-03 13:40:30');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `items_colors`
--

CREATE TABLE `items_colors` (
`id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Data dump for tabellen `items_colors`
--

INSERT INTO `items_colors` (`id`, `item_id`, `color_id`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 3, 1),
(4, 4, 9),
(5, 5, 1),
(6, 6, 1),
(7, 6, 6),
(8, 6, 10),
(9, 7, 6),
(10, 7, 7),
(18, 12, 1),
(19, 12, 2),
(20, 13, 2),
(21, 14, 2),
(22, 15, 1),
(23, 15, 2),
(24, 15, 4),
(26, 15, 4),
(27, 16, 4),
(28, 17, 2),
(29, 18, 4),
(30, 18, 3),
(31, 19, 1),
(32, 19, 2),
(33, 20, 6),
(34, 20, 2),
(35, 21, 1),
(36, 21, 2),
(37, 22, 2),
(38, 23, 2),
(39, 24, 6),
(40, 25, 8),
(41, 26, 4),
(42, 27, 7),
(43, 28, 1),
(44, 29, 2);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `items_fabrics`
--

CREATE TABLE `items_fabrics` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Data dump for tabellen `items_fabrics`
--

INSERT INTO `items_fabrics` (`id`, `name`, `item_id`) VALUES
(1, 'Inner lining', 1),
(2, 'Outer lining', 1),
(4, 'Whole', 3),
(5, 'Leather', 4),
(6, 'Dress', 5),
(7, 'Whole', 6),
(8, 'Outer', 7),
(9, 'Inner', 7),
(14, 'Blue Wonder', 12),
(15, 'qwe', 13),
(16, 'Inner lining', 14),
(17, 'Body', 15),
(18, 'Magic', 15),
(19, 'Pla', 16),
(20, 'Poly', 17),
(21, 'Body fabric', 18),
(22, 'Leg fabric', 18),
(23, 'Lining', 18),
(24, 'Body', 19),
(25, 'Sleeves', 0),
(26, 'Outer', 24),
(27, 'Inner', 24),
(28, 'Body', 25),
(29, 'Body', 26),
(30, 'Shirt', 27),
(31, '', 28),
(32, 'Shoey', 29);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `items_fabrics_materials`
--

CREATE TABLE `items_fabrics_materials` (
`id` int(11) NOT NULL,
  `fabric_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `percentage` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Data dump for tabellen `items_fabrics_materials`
--

INSERT INTO `items_fabrics_materials` (`id`, `fabric_id`, `material_id`, `percentage`) VALUES
(1, 1, 1, 100),
(2, 2, 4, 50),
(3, 2, 3, 50),
(7, 4, 1, 50),
(8, 4, 3, 25),
(9, 4, 7, 25),
(10, 5, 4, 100),
(11, 6, 4, 100),
(12, 7, 1, 80),
(13, 7, 3, 20),
(14, 8, 3, 100),
(15, 9, 1, 100),
(16, 10, 3, 50),
(17, 10, 7, 20),
(18, 10, 8, 30),
(19, 11, 3, 100),
(20, 12, 3, 50),
(21, 12, 7, 20),
(22, 12, 8, 30),
(23, 13, 3, 100),
(24, 14, 1, 100),
(25, 15, 1, 100),
(26, 16, 1, 100),
(27, 17, 1, 75),
(28, 17, 3, 25),
(29, 18, 4, 100),
(30, 19, 1, 100),
(31, 20, 4, 100),
(32, 21, 2, 50),
(33, 21, 7, 50),
(34, 22, 1, 100),
(35, 23, 3, 80),
(36, 23, 4, 20),
(37, 24, 1, 100),
(38, 26, 1, 100),
(39, 27, 1, 50),
(40, 27, 3, 50),
(41, 28, 1, 100),
(42, 29, 1, 100),
(43, 30, 1, 100),
(44, 31, 4, 100),
(45, 32, 1, 100);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `items_images`
--

CREATE TABLE `items_images` (
`id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `main` int(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=107 ;

--
-- Data dump for tabellen `items_images`
--

INSERT INTO `items_images` (`id`, `item_id`, `color_id`, `path`, `main`) VALUES
(1, 1, 2, '66751636051386352501.jpg', 1),
(2, 1, 2, '78619812521386352508.jpg', 0),
(3, 1, 2, '87130198721386352517.jpg', 0),
(4, 1, 2, '10525184581386352527.jpg', 0),
(5, 1, 2, '3296992001386352533.jpg', 0),
(6, 1, 3, '6981912621386352550.jpg', 0),
(7, 1, 3, '21680231501386352572.jpg', 0),
(8, 1, 3, '9621912761386352578.jpg', 0),
(9, 1, 3, '64101645011386352588.jpg', 0),
(10, 1, 3, '76807684971386352596.jpg', 0),
(11, 3, 1, '9852254311386367677.jpg', 1),
(12, 3, 1, '56794838441386367680.jpg', 0),
(13, 3, 1, '56483271781386367683.jpg', 0),
(14, 4, 9, '6592821621386368022.jpg', 1),
(15, 5, 1, '4512421221386923788.jpg', 1),
(16, 5, 1, '33101758851386923793.jpg', 0),
(17, 5, 1, '4848058991386923796.jpg', 0),
(18, 5, 1, '22527778941386923800.jpg', 0),
(19, 6, 1, '81092827061386939038.jpg', 1),
(20, 6, 1, '26554876301386939041.jpg', 0),
(21, 6, 1, '71779506051386939047.jpg', 0),
(22, 6, 6, '85037124401386939053.jpg', 0),
(23, 6, 6, '93693313951386939056.jpg', 0),
(24, 6, 10, '14805908671386939063.jpg', 0),
(25, 6, 10, '92033335261386939066.jpg', 0),
(26, 6, 10, '78717968391386939070.jpg', 0),
(27, 7, 6, '9763133011387464667.jpg', 1),
(28, 7, 6, '21051278931387464672.jpg', 0),
(29, 7, 7, '32734440721387464680.jpg', 0),
(30, 7, 7, '42616857751387464684.jpg', 0),
(33, 12, 1, '60349857011398420596.jpg', 1),
(34, 12, 1, '40169369971398420596.jpg', 0),
(35, 12, 1, '26387574601398420596.jpg', 0),
(36, 12, 1, '13473675641398420596.jpg', 0),
(37, 12, 2, '38915247831398420617.jpg', 0),
(38, 12, 2, '10665406701398420617.jpg', 0),
(39, 12, 2, '24701410251398420617.jpg', 0),
(40, 12, 2, '28460788671398420617.jpg', 0),
(41, 13, 2, '43682997611398934243.jpg', 1),
(42, 13, 2, '97687553771398934244.jpg', 0),
(43, 13, 2, '68918585541398934244.jpg', 0),
(44, 14, 2, '66609976991398953303.jpg', 1),
(45, 14, 2, '37317430921398953332.jpg', 0),
(46, 14, 2, '9152278901398953332.jpg', 0),
(47, 14, 2, '168325761398953332.jpg', 0),
(55, 16, 4, '1403007986-56702338781403007986.jpg', 1),
(56, 16, 4, '1403007993-3896661411403007993.jpg', 0),
(57, 16, 4, '1403007993-30213128371403007993.jpg', 0),
(58, 17, 2, '1406028618-81094496081406028618.jpg', 1),
(60, 17, 2, '1406028622-45438762521406028622.jpg', 0),
(61, 18, 4, '1406724798-97058187311406724798.jpg', 1),
(62, 18, 4, '1406724807-72831991581406724807.jpg', 0),
(63, 18, 3, '1406724833-5921420591406724833.jpg', 0),
(64, 18, 3, '1406724838-63317997991406724838.jpg', 0),
(65, 19, 1, '1406798869-4013824431406798869.jpg', 1),
(66, 19, 1, '1406798869-1809481081406798869.jpg', 0),
(67, 19, 1, '1406798869-65389317101406798869.jpg', 0),
(68, 19, 1, '1406798869-82917602401406798869.jpg', 0),
(69, 19, 2, '1406798881-61501663921406798881.jpg', 0),
(70, 19, 2, '1406798881-24884444371406798881.jpg', 0),
(71, 19, 2, '1406798881-9980966201406798881.jpg', 0),
(72, 19, 2, '1406798881-4079741171406798881.jpg', 0),
(73, 20, 6, '1406815894-27886884271406815894.jpg', 1),
(74, 20, 6, '1406815894-25636292981406815894.jpg', 0),
(75, 20, 6, '1406815894-62722681851406815894.jpg', 0),
(76, 20, 6, '1406815894-66166222281406815894.jpg', 0),
(77, 20, 2, '1406815915-78614600441406815915.jpg', 0),
(78, 20, 2, '1406815915-90912712361406815915.jpg', 0),
(79, 20, 2, '1406815915-931126551406815915.jpg', 0),
(80, 20, 2, '1406815915-30909225001406815915.jpg', 0),
(81, 21, 1, '1406817037-26913691271406817037.jpg', 1),
(82, 21, 1, '1406817037-55717516881406817037.jpg', 0),
(83, 21, 1, '1406817037-83221137781406817037.jpg', 0),
(84, 21, 2, '1406817093-83677420001406817093.jpg', 0),
(85, 21, 2, '1406817093-28410683421406817093.jpg', 0),
(86, 21, 2, '1406817093-29490762431406817093.jpg', 0),
(87, 22, 2, '1406818565-63803431871406818565.jpg', 1),
(88, 24, 6, '1406819031-61184448031406819031.jpg', 1),
(89, 24, 6, '1406819031-48326979321406819031.jpg', 0),
(90, 24, 6, '1406819031-4088335351406819031.jpg', 0),
(91, 25, 8, '1406883810-12380242861406883810.jpg', 1),
(92, 25, 8, '1406883815-58305940781406883815.jpg', 0),
(93, 25, 8, '1406883815-9460316311406883815.jpg', 0),
(94, 26, 4, '1407072725-19744343461407072725.jpg', 1),
(95, 26, 4, '1407072730-91526157321407072730.jpg', 0),
(96, 26, 4, '1407072730-30873699411407072730.jpg', 0),
(97, 27, 7, '1407072991-55302446701407072991.jpg', 1),
(98, 27, 7, '1407072996-38799706051407072996.jpg', 0),
(99, 27, 7, '1407072996-49076299821407072996.jpg', 0),
(100, 27, 7, '1407073000-5109891281407073000.jpg', 0),
(101, 28, 1, '1407073099-90478171301407073099.jpg', 1),
(102, 28, 1, '1407073103-6141631891407073103.jpg', 0),
(103, 28, 1, '1407073103-29399865651407073103.jpg', 0),
(104, 29, 2, '1407073205-87001159811407073205.jpg', 1),
(105, 29, 2, '1407073210-84075714191407073210.jpg', 0),
(106, 29, 2, '1407073217-85233518051407073217.jpg', 0);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `items_likes`
--

CREATE TABLE `items_likes` (
`id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=371 ;

--
-- Data dump for tabellen `items_likes`
--

INSERT INTO `items_likes` (`id`, `item_id`, `user_id`, `updated_at`, `created_at`) VALUES
(46, 16, 5, '2014-07-22 07:44:37', '2014-07-22 07:44:37'),
(132, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(173, 3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, 18, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(189, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(190, 20, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(302, 16, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(306, 28, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(335, 19, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(344, 16, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(346, 17, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(352, 25, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(356, 29, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(357, 27, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(366, 18, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(369, 25, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(370, 25, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `items_prices`
--

CREATE TABLE `items_prices` (
`id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Data dump for tabellen `items_prices`
--

INSERT INTO `items_prices` (`id`, `item_id`, `country_id`, `price`) VALUES
(1, 1, 1, 1299),
(2, 1, 2, 1599),
(3, 1, 3, 399),
(7, 3, 1, 399),
(8, 4, 1, 199),
(9, 5, 1, 399),
(10, 6, 1, 129),
(11, 7, 1, 899),
(19, 12, 1, 199),
(20, 12, 2, 229),
(21, 13, 1, 199),
(22, 14, 1, 199),
(23, 15, 2, 599),
(24, 15, 3, 79),
(25, 15, 1, 249),
(26, 16, 1, 249),
(27, 17, 1, 199),
(28, 18, 1, 96),
(29, 19, 1, 995),
(30, 20, 1, 5799),
(31, 21, 1, 7999),
(32, 21, 2, 8999),
(33, 21, 3, 999),
(34, 22, 1, 29),
(35, 24, 1, 279),
(36, 25, 1, 429),
(37, 26, 1, 179),
(38, 27, 1, 799),
(39, 28, 1, 649),
(40, 29, 1, 179);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `items_tags`
--

CREATE TABLE `items_tags` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Data dump for tabellen `items_tags`
--

INSERT INTO `items_tags` (`id`, `user_id`, `item_id`, `image`, `title`, `url`, `updated_at`, `created_at`) VALUES
(1, 1, 25, '1.jpg', 'Editor''s Style: Danielle Prescod''s Professional Look', 'http://www.elle.com/news/street-chic-daily/summer-editor-street-style-danielle-prescod-august-5', '2014-08-06 13:46:57', '2014-08-06 12:46:53'),
(2, 1, 25, '2.jpg', 'At the Stay Hotel', 'http://fashionbycaroline.dk/at-the-stay-hotel/', '2014-08-06 13:48:30', '2014-08-06 16:12:21'),
(3, 1, 25, '3.jpg', 'Sleek Hair', 'http://www.modebloggen.dk/2014/07/22/sleek-hair/', '2014-08-06 14:17:41', '2014-08-06 16:32:35'),
(5, 1, 25, '5.jpg', 'Bachelorette Party', 'http://www.modebloggen.dk/2014/06/23/bachelorette-party/', '2014-08-06 17:18:33', '2014-08-06 17:18:27'),
(6, 1, 25, '6.jpg', 'Weekend planer- none', 'http://sarahlouise.dk/2014/08/weekend-planer-none/', '2014-08-06 17:20:47', '2014-08-06 17:20:40'),
(7, 1, 25, '7.jpg', 'Suzy Menkes: Those Fabulous Fifties!', 'http://www.vogue.co.uk/news/2014/08/06/suzy-menkes---the-fabulous-fifties', '2014-08-06 17:22:51', '2014-08-06 17:22:44'),
(8, 1, 25, '8.jpg', 'A Decade Of Doutzen', 'http://www.vogue.co.uk/news/2014/08/06/doutzen-kroes-model-pictures-catwalk-cv', '2014-08-06 17:24:24', '2014-08-06 17:24:21'),
(9, 1, 25, '9.jpg', 'Spis dig til en flad mave med disse 12 madvarer', 'http://www.elle.dk/Skonhed/Sundhed/2014/Juli/flad-mave.aspx', '2014-08-06 17:27:18', '2014-08-06 17:27:18'),
(10, 1, 17, '10.jpg', 'Streetstyle fra modeugen â?? dag 1', 'http://www.elle.dk/Mode/Modeugen/August-2014/Copenhagen-Fashion-Week/Streetstyle-Dag-1.aspx', '2014-08-06 17:33:30', '2014-08-06 17:33:23'),
(11, 1, 17, '11.jpg', 'Chloé Sunnies', 'http://www.modebloggen.dk/2014/06/10/chloe-sunnies/', '2014-08-06 17:34:31', '2014-08-06 17:34:23'),
(12, 1, 17, '12.jpg', 'Kaibosh og Storm & Marie Ã¥bner butikker pÃ¥ Gammel Kongevej', 'http://www.elle.dk/Mode/Nyheder/2014/August/Kaiboosh-Storm-Marie.aspx', '2014-08-06 17:37:43', '2014-08-06 17:37:38'),
(14, 1, 18, '14.jpg', 'Chloé Sunnies', 'http://www.modebloggen.dk/2014/06/10/chloe-sunnies/', '2014-08-06 18:02:20', '2014-08-06 18:02:18'),
(15, 1, 18, '15.jpg', 'Kaibosh og Storm & Marie Ã¥bner butikker pÃ¥ Gammel Kongevej', 'http://www.elle.dk/Mode/Nyheder/2014/August/Kaiboosh-Storm-Marie.aspx', '2014-08-06 18:03:13', '2014-08-06 18:03:10'),
(16, 1, 25, '16.jpg', 'Her er ELLE-redaktionens modeugeoutfits - dag 1', 'http://www.elle.dk/Mode/Modeugen/August-2014/Copenhagen-Fashion-Week/Outfits-dag-1.aspx', '2014-08-08 15:30:09', '2014-08-08 15:30:09');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `lists`
--

CREATE TABLE `lists` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `user_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Data dump for tabellen `lists`
--

INSERT INTO `lists` (`id`, `name`, `description`, `user_id`, `updated_at`, `created_at`) VALUES
(1, 'Girls wear 2014', 'Here''s a bunch of cool clothing for girls in 2014!', 2, '2014-08-22 15:25:49', '2014-08-22 15:25:49'),
(2, 'Ethan''s cool list', 'Yes it very vewry very cool', 2, '2014-08-22 15:28:52', '2014-08-22 15:28:52'),
(3, 'Favorite clothes', 'A collection of my favorite clothes in 2014', 2, '2014-08-23 20:40:23', '2014-08-23 20:40:23'),
(4, 'Perfect clothing', 'Yes, that''s right!', 2, '2014-08-25 21:01:32', '2014-08-25 21:01:32'),
(5, 'Daniels nye tøj', '', 3, '2014-09-01 11:41:11', '2014-09-01 11:41:11'),
(6, 'Birthday', 'I wish for these', 3, '2014-09-01 11:51:33', '2014-09-01 11:51:33'),
(7, 'Party clothes', 'Ready to party!', 3, '2014-09-05 15:21:55', '2014-09-05 15:21:55'),
(8, 'Juleaften', NULL, 3, '2014-09-15 19:16:38', '2014-09-15 19:16:38');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `lists_items`
--

CREATE TABLE `lists_items` (
`id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Data dump for tabellen `lists_items`
--

INSERT INTO `lists_items` (`id`, `list_id`, `item_id`) VALUES
(2, 1, 17),
(5, 2, 27),
(6, 3, 18),
(7, 3, 20),
(9, 3, 16),
(23, 4, 16),
(26, 5, 26),
(27, 3, 26),
(30, 7, 25),
(31, 6, 16),
(32, 6, 20),
(34, 6, 12),
(35, 6, 28),
(36, 8, 25),
(42, 6, 18),
(44, 8, 18);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `materials`
--

CREATE TABLE `materials` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Data dump for tabellen `materials`
--

INSERT INTO `materials` (`id`, `name`) VALUES
(1, 'Cotton'),
(2, 'Cotton (Oragnic)'),
(3, 'Nylon'),
(4, 'Polyester (PET)'),
(5, 'Polyester (Recycled)'),
(6, 'Abacá'),
(7, 'Acetate'),
(8, 'Acrylic');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `messages`
--

CREATE TABLE `messages` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `conversation_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `is_read` int(1) DEFAULT '0',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Data dump for tabellen `messages`
--

INSERT INTO `messages` (`id`, `user_id`, `conversation_id`, `message`, `is_read`, `updated_at`, `created_at`) VALUES
(1, 3, 1, 'Ha ha ha.... Of course not!', 0, '2014-08-18 12:31:13', '2014-08-18 12:31:13'),
(2, 3, 2, 'OK?', 0, '2014-08-18 12:48:04', '2014-08-18 12:48:04'),
(3, 2, 1, 'Jees!', 0, '0000-00-00 00:00:00', '2014-08-19 12:31:13'),
(4, 2, 2, 'What do you want', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 3, 'Ha ha, you are just a regular user', 0, '2014-08-18 14:57:37', '2014-08-18 14:57:37'),
(6, 2, 1, 'What are you up to tonight?', 0, '2014-08-19 15:22:15', '2014-08-19 15:22:15'),
(7, 2, 3, 'OK!', 0, '2014-08-19 15:25:00', '2014-08-19 15:25:00'),
(10, 2, 4, 'Hello again!', 0, '2014-08-19 15:26:19', '2014-08-19 15:26:19'),
(13, 7, 5, 'What''s up Patrick? Come check out my store!', 0, '2014-08-20 12:27:34', '2014-08-20 12:27:34'),
(14, 2, 5, 'Hep hey', 0, '2014-08-20 12:28:15', '2014-08-20 12:28:15'),
(15, 2, 5, 'Are you there?', 0, '2014-08-20 12:48:27', '2014-08-20 12:48:27'),
(17, 2, 6, 'Can I see your new collection please', 0, '2014-08-20 16:52:30', '2014-08-20 16:52:30'),
(18, 2, 7, 'When are you getting the green jacket back home?', 0, '2014-08-20 17:02:10', '2014-08-20 17:02:10'),
(19, 2, 4, 'Hallooooo', 0, '2014-08-20 20:10:30', '2014-08-20 20:10:30'),
(20, 2, 3, 'Hi hi', 0, '2014-08-21 18:57:06', '2014-08-21 18:57:06'),
(21, 2, 5, 'piss off', 0, '2014-08-22 15:34:07', '2014-08-22 15:34:07'),
(22, 2, 5, 'Oh sorry', 0, '2014-08-25 18:41:26', '2014-08-25 18:41:26'),
(23, 1, 3, 'Don''t you dare!', 0, '2014-08-25 20:54:09', '2014-08-25 20:54:09'),
(24, 1, 3, 'Mu ha ha', 0, '2014-08-25 20:54:42', '2014-08-25 20:54:42'),
(25, 1, 3, 'Beautiful', 0, '2014-08-25 20:55:33', '2014-08-25 20:55:33'),
(26, 1, 3, 'Sweet jesus', 0, '2014-08-25 20:55:55', '2014-08-25 20:55:55'),
(27, 1, 3, 'wrooom', 0, '2014-08-25 20:58:11', '2014-08-25 20:58:11'),
(28, 1, 3, 'hm', 0, '2014-08-25 20:58:19', '2014-08-25 20:58:19'),
(29, 1, 3, 'Boom', 0, '2014-08-25 20:58:47', '2014-08-25 20:58:47'),
(30, 3, 4, 'Davs', 0, '2014-09-01 11:39:47', '2014-09-01 11:39:47'),
(31, 3, 4, 'AOSdk', 0, '2014-09-01 11:40:02', '2014-09-01 11:40:02'),
(32, 3, 4, 'helloooo', 0, '2014-10-05 14:58:57', '2014-10-05 14:58:57');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `notifications`
--

CREATE TABLE `notifications` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `read` int(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `saved_lists`
--

CREATE TABLE `saved_lists` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `list_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `sessions`
--

CREATE TABLE `sessions` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `expires` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Data dump for tabellen `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `token`, `expires`) VALUES
(4, 1, '$2y$08$GmHaIcCnrkXbN2Kiyz30/uIbtnf5QCZc1hV5MtL7qmOO2HOK4DMU.', '2014-06-20 14:33:42');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `shop_item`
--

CREATE TABLE `shop_item` (
`id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `accepted_by_shop` int(1) NOT NULL DEFAULT '0',
  `accepted_by_brand` int(1) NOT NULL DEFAULT '0',
  `price` decimal(11,2) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Data dump for tabellen `shop_item`
--

INSERT INTO `shop_item` (`id`, `shop_id`, `item_id`, `accepted_by_shop`, `accepted_by_brand`, `price`) VALUES
(10, 12, 29, 0, 0, 966.00),
(17, 1, 25, 0, 0, 223.00),
(18, 2, 25, 0, 0, 218.00),
(19, 1, 17, 0, 0, 418.00),
(20, 6, 17, 0, 0, 438.00),
(21, 20, 25, 0, 0, 935.00),
(22, 6, 25, 1, 1, 359.00),
(23, 1, 16, 0, 0, 989.00),
(26, 24, 16, 0, 0, 867.00),
(27, 1, 1, 0, 0, 368.00),
(28, 25, 1, 0, 0, 239.00),
(29, 1, 26, 0, 0, 91.00),
(30, 22, 7, 0, 0, 735.00),
(31, 25, 7, 0, 0, 403.00),
(32, 20, 24, 0, 0, 808.00),
(33, 24, 24, 0, 0, 833.00),
(37, 23, 5, 0, 0, 738.00),
(38, 25, 5, 0, 0, 190.00),
(39, 2, 28, 0, 0, 736.00),
(40, 23, 28, 0, 0, 108.00),
(41, 2, 20, 0, 0, 334.00),
(42, 22, 20, 0, 0, 343.00),
(45, 6, 1, 0, 0, 715.00),
(46, 22, 1, 0, 0, 546.00),
(47, 20, 26, 0, 0, 584.00),
(48, 22, 26, 0, 0, 283.00),
(49, 25, 26, 0, 0, 661.00),
(50, 2, 24, 0, 0, 457.00),
(51, 6, 16, 0, 0, 303.00),
(52, 20, 19, 0, 0, 140.00),
(53, 24, 19, 0, 0, 791.00),
(54, 23, 17, 0, 0, 537.00),
(55, 24, 17, 0, 0, 310.00),
(56, 23, 27, 0, 0, 938.00);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `shops`
--

CREATE TABLE `shops` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `shop_name` varchar(255) NOT NULL,
  `cvr` int(50) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `description` text,
  `cover` varchar(255) DEFAULT NULL,
  `lng` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `contact_title` int(2) NOT NULL,
  `contact_first_name` varchar(50) NOT NULL,
  `contact_last_name` varchar(50) NOT NULL,
  `contact_phone` varchar(8) NOT NULL,
  `contact_email` varchar(150) NOT NULL,
  `street` varchar(255) NOT NULL,
  `postal` varchar(4) NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Data dump for tabellen `shops`
--

INSERT INTO `shops` (`id`, `user_id`, `shop_name`, `cvr`, `website`, `phone`, `description`, `cover`, `lng`, `lat`, `contact_title`, `contact_first_name`, `contact_last_name`, `contact_phone`, `contact_email`, `street`, `postal`, `city`, `country`, `updated_at`, `created_at`) VALUES
(1, 7, 'H&M Kronprinsensgade', 123456, '', 0, 'Kings & Queens er en fed jeans og fashion butik. Vi sælger alle de nyeste mærker til både piger og drenge.\n\nVi har i øjeblikket 6 butikker i Danmark - Fields, Slagelse, Næstved, Svendborg, Sønderborg samt Aabenraa.\n', 'hm.jpg', '12.578465', '55.680542', 0, '', '', '', '', 'Hjultorv 8', '4700', 'Næstved', 'Denmark', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 9, 'New Yorker', 12345678, 'http://www.newyorker.de/dk/', 57462817, 'New Yorker is a shop located in Copenhagen selling clothes.', 'shop1.jpg', '12.571241', '55.677394', 0, '', '', '', '', 'Bahulahvej 25', '1114', 'København K', 'Denmark', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 15, 'Mulberry', 22995848, 'http://www.shop.dk/', 29828409, 'Da one and only shop', 'shop2.jpg', '12.583717', '55.680003', 0, '', '', '', '', 'Vejledalen 22', '2635', 'Ishøj', 'Denmark', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 0, 'Louis Vuitton', NULL, NULL, NULL, NULL, 'shop4.jpg', '12.579483', '55.678982', 0, '', '', '', '', 'Kongens Nytorv 13', '1050', 'København K', 'Denmark', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 0, 'Peak Performance', NULL, NULL, NULL, NULL, NULL, '12.582657', '55.679767', 0, '', '', '', '', 'Vejledalen 22', '2635', 'Ishoj', 'Denmark', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 0, 'Urban Outfitters', NULL, NULL, NULL, NULL, NULL, '12.581588', '55.679413', 0, '', '', '', '', 'Slotsgade 11A', '4200', 'Slagelse', 'Denmark', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 0, 'Vila', 12345678, 'www.revolution.com', NULL, NULL, NULL, '12.560457', '55.66189', 2, 'Hans', 'Hansen', '29828409', 'hans@revolution.com', 'Tordenskjoldsgade 25', '1055', 'København K', 'Denmark', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 29, 'Zara', 69809898, 'www.blah.coim', 2147483647, NULL, NULL, '12.579063', '55.630784', 2, 'Ethan', 'Cooper', '25806606', 'ethan@ottesencooper.com', 'kronprins 6', '1115', 'mebshao', 'denmark', '2014-08-28 13:52:26', '2014-08-28 13:52:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `throttle`
--

CREATE TABLE `throttle` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(4) NOT NULL DEFAULT '0',
  `banned` tinyint(4) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

--
-- Data dump for tabellen `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `ip_address`, `attempts`, `suspended`, `banned`, `last_attempt_at`, `suspended_at`, `banned_at`) VALUES
(1, 1, NULL, 0, 0, 0, NULL, NULL, NULL),
(2, 4, '::1', 0, 0, 0, NULL, NULL, NULL),
(3, 1, '::1', 0, 0, 0, NULL, NULL, NULL),
(4, 1, '87.60.45.156', 0, 0, 0, NULL, NULL, NULL),
(5, 1, '87.50.122.53', 0, 0, 0, NULL, NULL, NULL),
(6, 1, '127.0.0.1', 0, 0, 0, NULL, NULL, NULL),
(7, 6, '87.50.122.53', 0, 0, 0, NULL, NULL, NULL),
(8, 6, '::1', 0, 0, 0, NULL, NULL, NULL),
(9, 6, '87.60.45.156', 0, 0, 0, NULL, NULL, NULL),
(10, 1, '83.94.185.154', 0, 0, 0, NULL, NULL, NULL),
(11, 6, '83.94.185.154', 0, 0, 0, NULL, NULL, NULL),
(12, 9, '::1', 0, 0, 0, NULL, NULL, NULL),
(13, 6, '127.0.0.1', 0, 0, 0, NULL, NULL, NULL),
(14, 3, '::1', 0, 0, 0, NULL, NULL, NULL),
(15, 6, '86.52.159.251', 0, 0, 0, NULL, NULL, NULL),
(16, 2, '::1', 0, 0, 0, NULL, NULL, NULL),
(17, 7, '::1', 0, 0, 0, NULL, NULL, NULL),
(18, 19, '::1', 0, 0, 0, NULL, NULL, NULL),
(19, 24, '::1', 0, 0, 0, NULL, NULL, NULL),
(20, 30, '::1', 0, 0, 0, NULL, NULL, NULL),
(21, 3, '192.168.1.11', 0, 0, 0, NULL, NULL, NULL),
(22, 3, '192.168.1.26', 0, 0, 0, NULL, NULL, NULL),
(23, 3, '192.168.1.45', 0, 0, 0, NULL, NULL, NULL),
(24, 3, '192.168.1.85', 0, 0, 0, NULL, NULL, NULL),
(25, 3, '192.168.1.89', 0, 0, 0, NULL, NULL, NULL),
(26, 3, '192.168.1.93', 0, 0, 0, NULL, NULL, NULL),
(27, 3, '192.168.1.97', 0, 0, 0, NULL, NULL, NULL),
(28, 3, '192.168.1.90', 0, 0, 0, NULL, NULL, NULL),
(29, 3, '192.168.0.13', 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `users`
--

CREATE TABLE `users` (
`id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(1) NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(4) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Data dump for tabellen `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `type`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `created_at`, `updated_at`) VALUES
(1, 'reckpatrick@gmail.com', '$2y$10$Ou2T8vaxD.6ZNEAxqzWjg.Q5UXPgsAX6bo/6lZKOXtLhiD4LQ5JI6', 3, NULL, 1, NULL, NULL, '2014-10-09 18:29:20', '$2y$10$W03jN.MOam1DBV4sLFftA.cwa97BY8kGIJa2bkFkrRK51oQX51ktK', NULL, '2013-08-04 13:34:39', '2014-10-09 16:29:20'),
(2, 'patrick@reck.dk', '$2y$10$b6bLUWCXm3N7oCaochQtuOtQW8Bu/gxsq5q09/fo7Au94XeZrZ3zq', 1, NULL, 1, NULL, NULL, '2014-08-28 16:34:44', '$2y$10$r15zK9l1QFts/6iQP.D4s.bZU/5T0JITjkL69vwPqNx7n5A3dW7pS', NULL, '2013-08-04 19:54:51', '2014-08-28 14:34:44'),
(3, 'privat@mail.dk', '$2y$10$MpQLJ7TyAiyvfRrrZ303TuCM4B.oUWElq1Mg82vjU7EZWtGcHVbXO', 1, NULL, 1, NULL, NULL, '2014-10-09 18:31:00', '$2y$10$2xNvd2vOXRbzDWzM1BKagOGcfS01tOwBmmmtG2..AlKp0Qzfcpzni', NULL, '2013-08-04 19:54:51', '2014-10-09 16:31:00'),
(5, 'hugo@boss.com', '$2y$10$ujBrzElc/Repo.ZgsM.rhuaeic2mPBbb/Czg5yfwiw/eDiu5.I9ba', 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '2013-08-06 10:00:40', '2013-08-06 10:00:40'),
(7, 'shop@gmail.com', '$2y$10$EZYTZ4NPI3sSWhrXW.WCCeSOJkajbCslFNmYEKq3wSXo2vH6gXaVe', 2, NULL, 1, NULL, NULL, '2014-08-20 12:27:10', '$2y$10$Qv/GqYx/Ma8iTYC81cMTLuu7rycgMhplP4LNeKe1i5olq/rWsRi7e', NULL, '2014-08-20 10:22:51', '2014-08-20 10:27:10'),
(9, 'newyorker@gmail.com', '$2y$10$EZYTZ4NPI3sSWhrXW.WCCeSOJkajbCslFNmYEKq3wSXo2vH6gXaVe', 2, NULL, 1, NULL, NULL, '2014-08-20 12:27:10', '$2y$10$Qv/GqYx/Ma8iTYC81cMTLuu7rycgMhplP4LNeKe1i5olq/rWsRi7e', NULL, '2014-08-20 10:22:51', '2014-08-20 10:27:10'),
(26, 'ethan@ottesevncooper.com', '$2y$10$qrqKETNcChVmdkdfPkJcNO5Odxq2K9ymqlj4eiay.g0gK69YB6Ci6', 1, NULL, 0, 'JTZ36c7SEwjVYuEFNJChRz9PVFenZwmFFiOVdNPpef', NULL, NULL, NULL, NULL, '2014-08-27 12:53:37', '2014-08-27 12:53:37'),
(27, 'reckpatri1ck@gmail.com', '$2y$10$O77NLadTdxgz00eaIu7/tu/4FdZb8zRwUO8ez5Of0EPrkHjmv0knm', 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, '2014-08-28 11:32:00', '2014-08-28 11:32:00'),
(28, 'reckpaatrick@gmail.com', '$2y$10$bCch09xoSKTpJ.4c40kzweE8n5GjTGOw.7RaJFJ93MtkU/nfUcNOO', 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '2014-08-28 11:42:43', '2014-08-28 11:42:43'),
(29, 'ethan@ottesencooper.com', '$2y$10$A6sTo61plH048wSM/X/LSurZz7KhyTKFJ/Bwcoy3ObxJj6BqYe.MW', 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, '2014-08-28 11:52:26', '2014-08-28 11:52:26'),
(30, 'reckpatr2ick@gmail.com', '$2y$10$NYqKukKcTD8tUGfVSokfvui9APFTXWFrK8zuXjop9FV0ntE9lF3Nu', 1, NULL, 1, NULL, '2014-08-28 21:20:26', '2014-08-28 21:20:38', '$2y$10$WMyf0j9TQI.IDafkFQnS7OW3vZPajjHIUnvglGe0l9H9CapO06G2O', NULL, '2014-08-28 19:19:40', '2014-08-28 19:20:38'),
(31, 'reckpatricqqqk@gmail.com', '$2y$10$l9ussCfYEzCHti/UHrgcie3hEtTyzcjfdI7C7f5v7yIqihVK2lXdm', 1, NULL, 0, 'vDNxsKBvLnGVGqTG655QeI7jY6vn9swXJ0eUpneY9w', NULL, NULL, NULL, NULL, '2014-09-03 13:10:39', '2014-09-03 13:10:39');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `users_groups`
--

CREATE TABLE `users_groups` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Data dump for tabellen `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 3),
(2, 2, 3),
(3, 3, 3),
(4, 4, 3),
(5, 5, 3),
(6, 6, 3),
(9, 7, 3);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `users_have`
--

CREATE TABLE `users_have` (
`id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Data dump for tabellen `users_have`
--

INSERT INTO `users_have` (`id`, `item_id`, `user_id`, `updated_at`, `created_at`) VALUES
(5, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 28, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 29, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 25, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 19, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Begrænsninger for dumpede tabeller
--

--
-- Indeks for tabel `brands`
--
ALTER TABLE `brands`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- Indeks for tabel `brands_likes`
--
ALTER TABLE `brands_likes`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indeks for tabel `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `cloqin_campaign_users`
--
ALTER TABLE `cloqin_campaign_users`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `cloqin_campaigns`
--
ALTER TABLE `cloqin_campaigns`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `collection_item`
--
ALTER TABLE `collection_item`
 ADD PRIMARY KEY (`id`), ADD KEY `collection_id` (`collection_id`), ADD KEY `item_id` (`item_id`);

--
-- Indeks for tabel `collections`
--
ALTER TABLE `collections`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `colors`
--
ALTER TABLE `colors`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `constructions`
--
ALTER TABLE `constructions`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `conversations`
--
ALTER TABLE `conversations`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `countries`
--
ALTER TABLE `countries`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `customers`
--
ALTER TABLE `customers`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `event_item`
--
ALTER TABLE `event_item`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `events`
--
ALTER TABLE `events`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `groups_name_unique` (`name`);

--
-- Indeks for tabel `items`
--
ALTER TABLE `items`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `items_colors`
--
ALTER TABLE `items_colors`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `items_fabrics`
--
ALTER TABLE `items_fabrics`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `items_fabrics_materials`
--
ALTER TABLE `items_fabrics_materials`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `items_images`
--
ALTER TABLE `items_images`
 ADD PRIMARY KEY (`id`), ADD KEY `item_id` (`item_id`), ADD KEY `item_id_2` (`item_id`);

--
-- Indeks for tabel `items_likes`
--
ALTER TABLE `items_likes`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `items_prices`
--
ALTER TABLE `items_prices`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `items_tags`
--
ALTER TABLE `items_tags`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `lists`
--
ALTER TABLE `lists`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `lists_items`
--
ALTER TABLE `lists_items`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `materials`
--
ALTER TABLE `materials`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `notifications`
--
ALTER TABLE `notifications`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `saved_lists`
--
ALTER TABLE `saved_lists`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `sessions`
--
ALTER TABLE `sessions`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `shop_item`
--
ALTER TABLE `shop_item`
 ADD PRIMARY KEY (`id`), ADD KEY `shop_id` (`shop_id`), ADD KEY `item_id` (`item_id`);

--
-- Indeks for tabel `shops`
--
ALTER TABLE `shops`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `user_id_2` (`user_id`);

--
-- Indeks for tabel `throttle`
--
ALTER TABLE `throttle`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`), ADD KEY `users_activation_code_index` (`activation_code`), ADD KEY `users_reset_password_code_index` (`reset_password_code`);

--
-- Indeks for tabel `users_groups`
--
ALTER TABLE `users_groups`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `users_have`
--
ALTER TABLE `users_have`
 ADD PRIMARY KEY (`id`);

--
-- Brug ikke AUTO_INCREMENT for slettede tabeller
--

--
-- Tilføj AUTO_INCREMENT i tabel `brands`
--
ALTER TABLE `brands`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Tilføj AUTO_INCREMENT i tabel `brands_likes`
--
ALTER TABLE `brands_likes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- Tilføj AUTO_INCREMENT i tabel `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- Tilføj AUTO_INCREMENT i tabel `cloqin_campaign_users`
--
ALTER TABLE `cloqin_campaign_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Tilføj AUTO_INCREMENT i tabel `cloqin_campaigns`
--
ALTER TABLE `cloqin_campaigns`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Tilføj AUTO_INCREMENT i tabel `collection_item`
--
ALTER TABLE `collection_item`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Tilføj AUTO_INCREMENT i tabel `collections`
--
ALTER TABLE `collections`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Tilføj AUTO_INCREMENT i tabel `colors`
--
ALTER TABLE `colors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- Tilføj AUTO_INCREMENT i tabel `constructions`
--
ALTER TABLE `constructions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Tilføj AUTO_INCREMENT i tabel `conversations`
--
ALTER TABLE `conversations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Tilføj AUTO_INCREMENT i tabel `countries`
--
ALTER TABLE `countries`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Tilføj AUTO_INCREMENT i tabel `customers`
--
ALTER TABLE `customers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- Tilføj AUTO_INCREMENT i tabel `event_item`
--
ALTER TABLE `event_item`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Tilføj AUTO_INCREMENT i tabel `events`
--
ALTER TABLE `events`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Tilføj AUTO_INCREMENT i tabel `groups`
--
ALTER TABLE `groups`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Tilføj AUTO_INCREMENT i tabel `items`
--
ALTER TABLE `items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- Tilføj AUTO_INCREMENT i tabel `items_colors`
--
ALTER TABLE `items_colors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- Tilføj AUTO_INCREMENT i tabel `items_fabrics`
--
ALTER TABLE `items_fabrics`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- Tilføj AUTO_INCREMENT i tabel `items_fabrics_materials`
--
ALTER TABLE `items_fabrics_materials`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- Tilføj AUTO_INCREMENT i tabel `items_images`
--
ALTER TABLE `items_images`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=107;
--
-- Tilføj AUTO_INCREMENT i tabel `items_likes`
--
ALTER TABLE `items_likes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=371;
--
-- Tilføj AUTO_INCREMENT i tabel `items_prices`
--
ALTER TABLE `items_prices`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- Tilføj AUTO_INCREMENT i tabel `items_tags`
--
ALTER TABLE `items_tags`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- Tilføj AUTO_INCREMENT i tabel `lists`
--
ALTER TABLE `lists`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Tilføj AUTO_INCREMENT i tabel `lists_items`
--
ALTER TABLE `lists_items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- Tilføj AUTO_INCREMENT i tabel `materials`
--
ALTER TABLE `materials`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Tilføj AUTO_INCREMENT i tabel `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- Tilføj AUTO_INCREMENT i tabel `notifications`
--
ALTER TABLE `notifications`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Tilføj AUTO_INCREMENT i tabel `saved_lists`
--
ALTER TABLE `saved_lists`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Tilføj AUTO_INCREMENT i tabel `sessions`
--
ALTER TABLE `sessions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Tilføj AUTO_INCREMENT i tabel `shop_item`
--
ALTER TABLE `shop_item`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- Tilføj AUTO_INCREMENT i tabel `shops`
--
ALTER TABLE `shops`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- Tilføj AUTO_INCREMENT i tabel `throttle`
--
ALTER TABLE `throttle`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- Tilføj AUTO_INCREMENT i tabel `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- Tilføj AUTO_INCREMENT i tabel `users_groups`
--
ALTER TABLE `users_groups`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- Tilføj AUTO_INCREMENT i tabel `users_have`
--
ALTER TABLE `users_have`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;--
-- Database: `madfolket`
--

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `badges`
--

CREATE TABLE `badges` (
`id` int(10) unsigned NOT NULL,
  `type` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `badges`
--

INSERT INTO `badges` (`id`, `type`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 3, 'Første opskrift', 'Tilføj din første opskrift', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(2, 1, 'Vist 1000 gange', 'Få en af dine opskrifter vist 1000 gange', '2014-10-22 18:06:26', '2014-10-22 18:06:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `categories`
--

CREATE TABLE `categories` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Data dump for tabellen `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Hovedret', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(2, 'Forret', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(3, 'Dessert', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(4, 'Frokost', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(5, 'Snack', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(6, 'Morgenmad', '2014-10-22 18:06:26', '2014-10-22 18:06:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `cookbooks`
--

CREATE TABLE `cookbooks` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `cookbooks_recipes`
--

CREATE TABLE `cookbooks_recipes` (
`id` int(10) unsigned NOT NULL,
  `cookbook_id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `groups`
--

CREATE TABLE `groups` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `ingredients`
--

CREATE TABLE `ingredients` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Data dump for tabellen `ingredients`
--

INSERT INTO `ingredients` (`id`, `name`, `unit_id`, `created_at`, `updated_at`) VALUES
(1, 'Hakket oksekød 3%', 2, '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(2, 'Minimælk', 5, '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(3, 'Salt', 9, '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(4, 'Lasagneplader', 10, '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(5, 'Flåede tomater', 12, '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(6, 'Løg', 10, '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(7, 'Oregano', 2, '2014-10-22 18:06:26', '2014-10-22 18:06:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data dump for tabellen `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2012_12_06_225921_migration_cartalyst_sentry_install_users', 1),
('2012_12_06_225929_migration_cartalyst_sentry_install_groups', 1),
('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot', 1),
('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', 1),
('2014_10_20_160840_create_recipes_table', 1),
('2014_10_20_160841_create_recipes_ratings_table', 1),
('2014_10_20_160845_create_recipes_steps_table', 1),
('2014_10_20_161913_create_recipes_images_table', 1),
('2014_10_20_161915_create_recipes_ingredients_table', 1),
('2014_10_20_161918_create_recipes_comments_table', 1),
('2014_10_20_162216_create_badges_table', 1),
('2014_10_20_162233_create_users_badges_table', 1),
('2014_10_20_163035_create_ingredients_table', 1),
('2014_10_20_163051_create_units_table', 1),
('2014_10_20_164526_create_cookbooks_table', 1),
('2014_10_20_164609_create_cookbooks_recipes_table', 1),
('2014_10_20_192606_create_categories_table', 1),
('2014_10_20_192630_create_origins_table', 1),
('2014_10_20_192722_create_occasions_table', 1),
('2014_10_20_193019_create_recipes_occasions_table', 1);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `occasions`
--

CREATE TABLE `occasions` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Data dump for tabellen `occasions`
--

INSERT INTO `occasions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Jul', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(2, 'Påske', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(3, 'Pinse', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(4, 'Nytår', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(5, 'Fødselsdag', '2014-10-22 18:06:26', '2014-10-22 18:06:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `origins`
--

CREATE TABLE `origins` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

--
-- Data dump for tabellen `origins`
--

INSERT INTO `origins` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Danmark', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(2, 'Tyskland', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(3, 'Belgien', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(4, 'Bulgarien', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(5, 'Cypern', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(6, 'Estland', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(7, 'Finland', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(8, 'Frankrig', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(9, 'Grækenland', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(10, 'Irland', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(11, 'Italien', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(12, 'Kroatien', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(13, 'Letland', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(14, 'Litauen', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(15, 'Luxemborg', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(16, 'Malta', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(17, 'Holland', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(18, 'Polen', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(19, 'Portugal', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(20, 'Rumænien', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(21, 'Slovakiet', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(22, 'Slovenien', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(23, 'Spanien', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(24, 'Sverige', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(25, 'Tjekkiet', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(26, 'Ungarn', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(27, 'Østrig', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(28, 'Irak', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(29, 'Iran', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(30, 'Israel', '2014-10-22 18:06:26', '2014-10-22 18:06:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `recipes`
--

CREATE TABLE `recipes` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `introduction` text COLLATE utf8_unicode_ci NOT NULL,
  `hours` int(11) NOT NULL,
  `minutes` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `origin_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Data dump for tabellen `recipes`
--

INSERT INTO `recipes` (`id`, `name`, `introduction`, `hours`, `minutes`, `category_id`, `origin_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Lasagne', 'Denne lasagne er super hurtig at bikse sammen, og smager ganske vidunderligt', 0, 45, 1, 11, 1, '2014-10-22 18:06:26', '2014-10-22 18:06:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `recipes_comments`
--

CREATE TABLE `recipes_comments` (
`id` int(10) unsigned NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `recipes_comments`
--

INSERT INTO `recipes_comments` (`id`, `comment`, `user_id`, `recipe_id`, `created_at`, `updated_at`) VALUES
(1, 'Den er SÅ god!', 1, 1, '2014-10-22 17:06:26', '2014-10-22 18:06:26'),
(2, 'Worst fucking lasagne ever', 2, 1, '2014-10-22 18:06:26', '2014-10-22 18:06:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `recipes_images`
--

CREATE TABLE `recipes_images` (
`id` int(10) unsigned NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `recipes_images`
--

INSERT INTO `recipes_images` (`id`, `recipe_id`, `path`, `created_at`, `updated_at`) VALUES
(1, 1, '1.jpg', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(2, 1, '2.jpg', '2014-10-22 18:06:26', '2014-10-22 18:06:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `recipes_ingredients`
--

CREATE TABLE `recipes_ingredients` (
`id` int(10) unsigned NOT NULL,
  `amount` float(8,2) NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Data dump for tabellen `recipes_ingredients`
--

INSERT INTO `recipes_ingredients` (`id`, `amount`, `ingredient_id`, `recipe_id`, `unit_id`, `created_at`, `updated_at`) VALUES
(1, 500.00, 1, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1.50, 3, 1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1.00, 2, 1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 3.00, 6, 1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `recipes_occasions`
--

CREATE TABLE `recipes_occasions` (
`id` int(10) unsigned NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `occasion_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `recipes_ratings`
--

CREATE TABLE `recipes_ratings` (
`id` int(10) unsigned NOT NULL,
  `rating` int(11) NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `recipes_ratings`
--

INSERT INTO `recipes_ratings` (`id`, `rating`, `recipe_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 5, 1, 1, '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(2, 1, 1, 2, '2014-10-22 18:06:26', '2014-10-22 18:06:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `recipes_steps`
--

CREATE TABLE `recipes_steps` (
`id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `timer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recipe_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Data dump for tabellen `recipes_steps`
--

INSERT INTO `recipes_steps` (`id`, `description`, `timer`, `recipe_id`, `created_at`, `updated_at`) VALUES
(1, 'Svits løg i en gryde', NULL, 1, '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(2, 'Tilsæt derefter oksekødet til det er svitset brunt', NULL, 1, '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(3, 'Tilsæt herefter de andre ting på listen lige så stille. Den skal have 20 minutter.', NULL, 1, '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(4, 'Saml pladerne i et fad og hæld sovsen over', NULL, 1, '2014-10-22 18:06:26', '2014-10-22 18:06:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `throttle`
--

CREATE TABLE `throttle` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `units`
--

CREATE TABLE `units` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shorthand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Data dump for tabellen `units`
--

INSERT INTO `units` (`id`, `name`, `shorthand`, `created_at`, `updated_at`) VALUES
(1, 'kilogram', 'kg', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(2, 'gram', 'g', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(3, 'milligram', 'mg', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(4, 'liter', 'l', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(5, 'deciliter', 'dl', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(6, 'milliliter', 'ml', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(7, 'teske', 'tsk', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(8, 'spiseske', 'spsk', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(9, 'knivspids', 'knsp', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(10, 'styk', 'stk', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(11, 'pakke', 'pk', '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(12, 'dåse', 'ds', '2014-10-22 18:06:26', '2014-10-22 18:06:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `users`
--

CREATE TABLE `users` (
`id` int(10) unsigned NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `profile_picture`, `facebook_id`, `email`, `password`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `created_at`, `updated_at`) VALUES
(1, 'Patrick', 'Reck', '1.jpg', NULL, 'reckpatrick@gmail.com', '$2y$10$TAHve3E1zit/OxS.duFfHeVl3xXp4rsS1bGJwu220eQqdoE65UW6e', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2014-10-22 18:06:26', '2014-10-22 18:06:26'),
(2, 'Nicklas', 'Frank', '1.jpg', NULL, 'admin@cs-e.dk', '$2y$10$fqk7P.xX1UdojEJoAZ1Suut2BxWkUcaS6FqO8OVYKTjg89mxvN2XO', NULL, 0, NULL, NULL, NULL, NULL, NULL, '2014-10-22 18:06:26', '2014-10-22 18:06:26');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `users_badges`
--

CREATE TABLE `users_badges` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `badge_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `users_badges`
--

INSERT INTO `users_badges` (`id`, `user_id`, `badge_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Begrænsninger for dumpede tabeller
--

--
-- Indeks for tabel `badges`
--
ALTER TABLE `badges`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `categories`
--
ALTER TABLE `categories`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `cookbooks`
--
ALTER TABLE `cookbooks`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `cookbooks_recipes`
--
ALTER TABLE `cookbooks_recipes`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `groups_name_unique` (`name`);

--
-- Indeks for tabel `ingredients`
--
ALTER TABLE `ingredients`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `occasions`
--
ALTER TABLE `occasions`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `origins`
--
ALTER TABLE `origins`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `recipes`
--
ALTER TABLE `recipes`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `recipes_comments`
--
ALTER TABLE `recipes_comments`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `recipes_images`
--
ALTER TABLE `recipes_images`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `recipes_ingredients`
--
ALTER TABLE `recipes_ingredients`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `recipes_occasions`
--
ALTER TABLE `recipes_occasions`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `recipes_ratings`
--
ALTER TABLE `recipes_ratings`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `recipes_steps`
--
ALTER TABLE `recipes_steps`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `throttle`
--
ALTER TABLE `throttle`
 ADD PRIMARY KEY (`id`), ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indeks for tabel `units`
--
ALTER TABLE `units`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`), ADD KEY `users_activation_code_index` (`activation_code`), ADD KEY `users_reset_password_code_index` (`reset_password_code`);

--
-- Indeks for tabel `users_badges`
--
ALTER TABLE `users_badges`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `users_groups`
--
ALTER TABLE `users_groups`
 ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Brug ikke AUTO_INCREMENT for slettede tabeller
--

--
-- Tilføj AUTO_INCREMENT i tabel `badges`
--
ALTER TABLE `badges`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Tilføj AUTO_INCREMENT i tabel `categories`
--
ALTER TABLE `categories`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Tilføj AUTO_INCREMENT i tabel `cookbooks`
--
ALTER TABLE `cookbooks`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Tilføj AUTO_INCREMENT i tabel `cookbooks_recipes`
--
ALTER TABLE `cookbooks_recipes`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Tilføj AUTO_INCREMENT i tabel `groups`
--
ALTER TABLE `groups`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Tilføj AUTO_INCREMENT i tabel `ingredients`
--
ALTER TABLE `ingredients`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Tilføj AUTO_INCREMENT i tabel `occasions`
--
ALTER TABLE `occasions`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Tilføj AUTO_INCREMENT i tabel `origins`
--
ALTER TABLE `origins`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- Tilføj AUTO_INCREMENT i tabel `recipes`
--
ALTER TABLE `recipes`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Tilføj AUTO_INCREMENT i tabel `recipes_comments`
--
ALTER TABLE `recipes_comments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Tilføj AUTO_INCREMENT i tabel `recipes_images`
--
ALTER TABLE `recipes_images`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Tilføj AUTO_INCREMENT i tabel `recipes_ingredients`
--
ALTER TABLE `recipes_ingredients`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Tilføj AUTO_INCREMENT i tabel `recipes_occasions`
--
ALTER TABLE `recipes_occasions`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Tilføj AUTO_INCREMENT i tabel `recipes_ratings`
--
ALTER TABLE `recipes_ratings`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Tilføj AUTO_INCREMENT i tabel `recipes_steps`
--
ALTER TABLE `recipes_steps`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Tilføj AUTO_INCREMENT i tabel `throttle`
--
ALTER TABLE `throttle`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Tilføj AUTO_INCREMENT i tabel `units`
--
ALTER TABLE `units`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- Tilføj AUTO_INCREMENT i tabel `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Tilføj AUTO_INCREMENT i tabel `users_badges`
--
ALTER TABLE `users_badges`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;--
-- Database: `plusdage`
--

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `articles`
--

CREATE TABLE `articles` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `articles`
--

INSERT INTO `articles` (`id`, `title`, `text`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Super er kommet', 'Til byen, i København!', '', '2014-09-28 19:48:49', '2014-09-28 19:48:49'),
(2, 'Suoer', 'asdqweqweqe123123', '', '2014-09-30 13:30:50', '2014-09-30 13:30:50');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `groups`
--

CREATE TABLE `groups` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Data dump for tabellen `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2012_12_06_225921_migration_cartalyst_sentry_install_users', 1),
('2012_12_06_225929_migration_cartalyst_sentry_install_groups', 1),
('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot', 1),
('2012_12_06_225988_migration_cartalyst_sentry_install_throttle', 1);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `throttle`
--

CREATE TABLE `throttle` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Data dump for tabellen `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `ip_address`, `attempts`, `suspended`, `banned`, `last_attempt_at`, `suspended_at`, `banned_at`) VALUES
(1, 1, '::1', 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `users`
--

CREATE TABLE `users` (
`id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Data dump for tabellen `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
(1, 'admin@admin.dk', '$2y$10$GA/U2LPRV5KpyZ4NrbXt6.cNK0.NdETHJvA5scliJKmcze6Scxjfq', NULL, 1, NULL, '2014-09-28 16:45:52', '2014-09-30 11:29:13', '$2y$10$WJa2TKnYBq4gRBcD8DXgX..2Wkv3ooHyxxnBS1h0jxcWJFE7mFx9m', NULL, NULL, NULL, '2014-09-28 16:45:52', '2014-09-30 11:29:13');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Begrænsninger for dumpede tabeller
--

--
-- Indeks for tabel `articles`
--
ALTER TABLE `articles`
 ADD PRIMARY KEY (`id`);

--
-- Indeks for tabel `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `groups_name_unique` (`name`);

--
-- Indeks for tabel `throttle`
--
ALTER TABLE `throttle`
 ADD PRIMARY KEY (`id`), ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indeks for tabel `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`), ADD KEY `users_activation_code_index` (`activation_code`), ADD KEY `users_reset_password_code_index` (`reset_password_code`);

--
-- Indeks for tabel `users_groups`
--
ALTER TABLE `users_groups`
 ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Brug ikke AUTO_INCREMENT for slettede tabeller
--

--
-- Tilføj AUTO_INCREMENT i tabel `articles`
--
ALTER TABLE `articles`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Tilføj AUTO_INCREMENT i tabel `groups`
--
ALTER TABLE `groups`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Tilføj AUTO_INCREMENT i tabel `throttle`
--
ALTER TABLE `throttle`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Tilføj AUTO_INCREMENT i tabel `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
